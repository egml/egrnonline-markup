<?php
$dir = new RecursiveDirectoryIterator('src\img\svg-sprite');
$iterator = new RecursiveIteratorIterator($dir);
$icons = [];
foreach ($iterator as $path) {
	if ($path->isFile() and preg_match('/^icon-([a-zA-Z0-9-]+).svg/', $path->getFileName(), $matches)) {
		$icons[] = $matches[1];
	}
}
$fp = fopen('build/partials/icon-index.json', 'w');
fwrite($fp, json_encode($icons));
fclose($fp);
echo 'Индекс иконок обновлен.' . PHP_EOL;