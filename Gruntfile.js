var ns = 'egrn-'; // с дефисом на конце, например 'ns-'; используется в: svgstore, sass

var sassFiles = [{
	expand: true,
	cwd: '<%= src %>/css',
	src: ['**/*.scss'],
	ext: '.css',
	dest: '<%= dest %>/css',
	rename: function(dest, src) {
		var name;
		if (src == 'main.css') {
			name = ns.slice(0,-1) + '.css'; // ns без дефиса
		} else {
			name = ns + src;
		}
		return dest + '/' + name;
	},
}];

// const sass = require('sass'); // Выдает больше информации при ошибке, но работает в 4 и более раз медленнее
// const Fiber = require('fibers'); // Нужно подключать также класс Fibers для улучшения производительности Dart Sass
const sass = require('node-sass');

module.exports = function(grunt) {
	grunt.config.init({

		ns: ns,
		src: 'src',
		dest: 'build/assets',
		// dest: '../yii/www/assets',

		// /*
		// ███████  █████  ███████ ███████
		// ██      ██   ██ ██      ██
		// ███████ ███████ ███████ ███████
		//      ██ ██   ██      ██      ██
		// ███████ ██   ██ ███████ ███████
		// */
		// #sass #css

		sass: {
			options: {
				implementation: sass,
				// fiber: Fiber,
				sourceMap: false,
				outputStyle: 'expanded',
				indentType: 'tab',
				indentWidth: 1,
				importer: require('node-sass-import'),

				// // Из Node Sass Options https://github.com/sass/node-sass#options (указаны дефолтные значения и далее в комментарии все возможные):
				// precision: 10,
				// data: null,
				// importer: null,
				// functions: null,
				// includePaths: null,
				// indentedSyntax: null,
				// indentType: null,
				// indentWidth: null,
				// linefeed: null,
				// omitSourceMapUrl: null,
				// outputStyle: nested, // nested, expanded, compact, compressed
				// sourceComments: null,
				// sourceMap: null,
				// sourceMapContents: null,
				// sourceMapEmbed: null,
				// sourceMapRoot: null,
			},
			dev: {
				files: sassFiles,
			},
			prod: {
				options: {
					sourceMap: true,
					outputStyle: 'compressed',
				},
				files: sassFiles,
			},
		},
		
		// // grunt-contrib-sass:
		// sass: {
		// 	options: {
		// 		sourcemap: 'none',
		// 		trace: false,
		// 		check: false,
		// 		style: 'expanded',
		// 		precision: 5,
		// 		quiet: false,
		// 		compass: false,
		// 		debugInfo: false,
		// 		lineNumbers: false,
		// 		cacheLocation: 'node_modules/grunt-contrib-sass/.sass-cache',
		// 		noCache: false,
		// 		bundleExec: false,
		// 		update: true,
		// 	},
		// 	dev: {
		// 		files: sassFiles,
		// 	},
		// 	build: {
		// 		options: {
		// 			style: 'compressed',
		// 			update: false,
		// 		},
		// 		files: sassFiles,
		// 	},
		// },

		// /*
		// ██ ███    ███  █████   ██████  ███████ ███    ███ ██ ███    ██
		// ██ ████  ████ ██   ██ ██       ██      ████  ████ ██ ████   ██
		// ██ ██ ████ ██ ███████ ██   ███ █████   ██ ████ ██ ██ ██ ██  ██
		// ██ ██  ██  ██ ██   ██ ██    ██ ██      ██  ██  ██ ██ ██  ██ ██
		// ██ ██      ██ ██   ██  ██████  ███████ ██      ██ ██ ██   ████
		// 
		//  * https://www.npmjs.com/package/grunt-contrib-imagemin
		//  * SVGO:
		//    https://github.com/sindresorhus/grunt-svgmin#available-optionsplugins
		//    https://github.com/svg/svgo/tree/master/plugins
		// */
		// #imagemin #img
		
		imagemin: {
			build: {
				files: [{
					expand: true,
					cwd: '<%= src %>/img',
					src: ['**/*.{svg,png,jpg,gif}', '!svg-sprite/*'],
					dest: '<%= dest %>/img'
				}],
			},
			svg_sprite: {
				options: {
					svgoPlugins: [
						{ cleanupNumericValues: false },
						{ removeDimensions: true },
						{ removeUselessStrokeAndFill: false },
						{ convertPathData: false },
						// {
						// 	cleanupIDs: {
						// 		remove: false,
						// 	}
						// },
						{ mergePaths: false },
					]
				},
				files: [{
					expand: true,
					cwd: '<%= src %>/img/svg-sprite',
					src: ['*.svg'],
					dest: 'runtime/svg-sprite',
					ext: '.optimized.svg',
				}],
			}
		},

		// /*
		// ███████ ██    ██  ██████  ███████ ████████  ██████  ██████  ███████
		// ██      ██    ██ ██       ██         ██    ██    ██ ██   ██ ██
		// ███████ ██    ██ ██   ███ ███████    ██    ██    ██ ██████  █████
		//      ██  ██  ██  ██    ██      ██    ██    ██    ██ ██   ██ ██
		// ███████   ████    ██████  ███████    ██     ██████  ██   ██ ███████
		// 
		// https://www.npmjs.com/package/grunt-svgstore
		// */
		// #svgstore
		
		svgstore: {
			options: {
				prefix: '<%= ns %>svg_sprite-',
				svg: {
					id: '<%= ns %>svg_sprite',
					xmlns: 'http://www.w3.org/2000/svg',
					style: 'display:none',
				},
				cleanup: false,
				includeTitleElement: false,
			},
			build: {
				files: {
					'<%= dest %>/img/<%= ns %>svg_sprite.svg': ['runtime/svg-sprite/*'],
				},
			},
		},

		// /*
		//  ██████ ██      ███████  █████  ███    ██
		// ██      ██      ██      ██   ██ ████   ██
		// ██      ██      █████   ███████ ██ ██  ██
		// ██      ██      ██      ██   ██ ██  ██ ██
		//  ██████ ███████ ███████ ██   ██ ██   ████
		// */
		// #clean
		
		clean: {
			svg_sprite: 'runtime/svg-sprite/*',
			build: [
				'<%= dest %>/css',
				'<%= dest %>/fonts',
				'<%= dest %>/img',
				'<%= dest %>/js',
				// '!<%= dest %>/published',
			],
			css: '<%= dest %>/css',
			fonts: '<%= dest %>/fonts',
			img: '<%= dest %>/img',
			js: '<%= dest %>/js',
		},

		// /*
		//  ██████  ██████  ██████  ██    ██
		// ██      ██    ██ ██   ██  ██  ██
		// ██      ██    ██ ██████    ████
		// ██      ██    ██ ██         ██
		//  ██████  ██████  ██         ██
		// */
		// #copy
		
		copy: {
			build: {
				files: [
					{ expand: true, cwd: '<%= src %>/copy', src: ['**'], dest: '<%= dest %>' },
				]
			},
		},

		// /*
		//      ██ ███████ ██   ██ ██ ███    ██ ████████
		//      ██ ██      ██   ██ ██ ████   ██    ██
		//      ██ ███████ ███████ ██ ██ ██  ██    ██
		// ██   ██      ██ ██   ██ ██ ██  ██ ██    ██
		//  █████  ███████ ██   ██ ██ ██   ████    ██
		// */
		// #jshint
		
		jshint: {
			gruntfile: ['Gruntfile.js'],
		},

		// /*
		//  ██████  ██████  ███    ██  ██████ ██    ██ ██████  ██████  ███████ ███    ██ ████████
		// ██      ██    ██ ████   ██ ██      ██    ██ ██   ██ ██   ██ ██      ████   ██    ██
		// ██      ██    ██ ██ ██  ██ ██      ██    ██ ██████  ██████  █████   ██ ██  ██    ██
		// ██      ██    ██ ██  ██ ██ ██      ██    ██ ██   ██ ██   ██ ██      ██  ██ ██    ██
		//  ██████  ██████  ██   ████  ██████  ██████  ██   ██ ██   ██ ███████ ██   ████    ██
		// */
		// #concurrent

		concurrent: {
			options: {
				logConcurrentOutput: true,
			},
			default: [
				'watch:css',
				'watch:img',
				'watch:svg_sprite',
				'watch:copy',
			],
		},

		// /*
		// ██     ██  █████  ████████  ██████ ██   ██
		// ██     ██ ██   ██    ██    ██      ██   ██
		// ██  █  ██ ███████    ██    ██      ███████
		// ██ ███ ██ ██   ██    ██    ██      ██   ██
		//  ███ ███  ██   ██    ██     ██████ ██   ██
		// */
		// #watch

		watch: {
			options: {
				spawn: false,
			},
			css: {
				files: ['<%= src %>/css/**/*.scss'],
				tasks: ['sass:dev'],
			},
			img: {
				files: ['<%= src %>/img/**/*.{svg,png,jpg,gif}', '!<%= src %>/img/svg-sprite/*'],
				tasks: ['newer:imagemin:build'],
			},
			svg_sprite: {
				files: ['<%= src %>/img/svg-sprite/*.svg'],
				tasks: [
					'clean:svg_sprite',
					'imagemin:svg_sprite',
					'svgstore:build',
				],
			},
			copy: {
				files: ['<%= src %>/copy/**/*'],
				tasks: [
					'newer:copy',
				],
			},
		},

	});

	grunt.loadNpmTasks('grunt-notify');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-svgstore');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');

	grunt.registerTask('default', ['concurrent']);
	grunt.registerTask('css', [
		'sass:dev',
	]);
	grunt.registerTask('css:prod', [
		'sass:prod',
	]);
	grunt.registerTask('img', [
		'imagemin:build',
	]);
	grunt.registerTask('svg', [
		'clean:svg_sprite',
		'imagemin:svg_sprite',
		'svgstore:build',
	]);
	grunt.registerTask('build', [
		'clean:build',
		'css:build',
		'img',
		'svg',
		'copy',
	]);
	grunt.registerTask('build:dev', [
		'clean:build',
		'css:dev',
		'img',
		'svg',
		'copy',
	]);

};