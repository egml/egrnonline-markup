/**
 * Функционал правого блока (который сворачивается)
 * ------------------------------------------------
 * Сначала отображается содержимое страницы, а при выборе объекта на карте, содержимое блока 
 * меняется на форму выбора услуг и добавляется кнопка "Заказать"
 */
function Rblock() {
	var self = this;
	var expand = true;
	var action = $.ajax({
		url: "/aorder",
		dataType: "JSON",
		success: function(json){				 			
			console.log(json);
			self.services= json;
		},
		error:function() {}
	});
	
	this.services = [];
	this.id = false;
	this.type = false;
	this.address = false;	
	this.square = false;	
	this.region = false;	
	this.div=$('#right_block');
	this.div.body=$('#right_block .body_wrapper');
	this.div.foot=$('#right_block .foot_wrapper');
	this.modal=$('#myModal .modal-body');
  
	this.kind = {
		"26": "Электронные выписки",
		"27": "Бумажные выписки с печатью",
		"28": "Услуги",
	};

	this.kindInfo = {
		"26": "Оплачиваете сейчас на сайте и получаете в указанные сроки на электронную почту",
		"27": "Оплачиваете сейчас на сайте и получаете в указанные сроки экспресс доставкой",
		"28": "<div class='item'>1. С Вами свяжется специалист, подберет кадастрового инженера и назовет сумму</div>\
			<div class='item'>2. Примите оферту на сайте или подпишите договор в офисе или на объекте</div>\
			<div class='item'>3. Оплатите в офисе, на объекте или по выставленному на электронную почту счету через сайт</div>",
	};
	
	// обработка флажка и видимости кнопки предзаказа
	$('#right_block').on('change','input.ui-all' ,function(e){
		//console.log('chaange');
		/*$('#right_block #ul_services input:checked').each(function() {
			console.log(this);
		});*/
		console.log($('#right_block #ul_services input:checked'));
		if ($('#right_block #ul_services input:checked').length)
		{ 
			//$('#pre_order').addClass('active');
			$('#pre_order').prop('disabled', false);
			console.log('checked') ;
		} else {
			//$('#pre_order').removeClass('active');
			$('#pre_order').prop('disabled', true);
			console.log('no-checked') ;
		}
	});
	
	// обработка нажатия кнопки предзаказа
	$('#right_block').on('click', '#pre_order', function(e) {
		var selected = [];
		$('#right_block #ul_services input:checked').each(function() {
		    selected.push($(this).attr('value'));
		});
		//console.warn(selected);
		if (selected.length === 0) {
		    $('#error_message').html("Выберите услугу");
			return 'false';
		}
        $("#myModal .modal-body").html(""); // clear
		$("#myModal .modal-body").append("<div class='row'></div>");			
		$("<div class='col-md-6 lb'></div>").appendTo("#myModal .modal-body .row");
		$("<div class='col-md-6 rb'></div>").appendTo("#myModal .modal-body .row");

		$("#myModal .modal-body .lb").append(
			"<form id='orderForm'  method='get'>\
				<div id='form_service_list'>\
				</div>\
				<div class='form_order' id='form_order_id'> \
				<table class='order'>\
					<tr>\
						<td><input type='email' name='email' placeholder='Email' required></td>\
						<td><input type='tel' name='tel' placeholder='Телефон' required></td>\
						<td><input type='text' name='firstname' placeholder='Имя'></td>\
					</tr>\
					<tr>\
						<td colspan=2><textarea name='annotation' cols='30' rows='2' placeholder='Примечание'></textarea></td>\
						<td><div class='sum_info'><div class='sum_header'>К оплате</div><div class='summ'></div></div></td>\
					</tr>\
					<tr>\
						<td colspan=2>\
						<input style='position: absolute;margin-top: 23px;    width: auto;' type='checkbox' checked name='agree' value='1' required >\
						<div style='display: block;float: left;padding-left: 20px;'>Согласен с Публичной офертой и согласен на обработку моих персональных данных согласно Политике конфиденциальности</div>\
						</td>\
						<td><div class='' style='display:inline-block;'><input class='order_button' type='submit' value='ЗАКАЗАТЬ'></div></td>\
					</tr>\
				</table>\
				<input type='hidden' name='hidden_id' value='"+self.id+"' >\
				<input type='hidden' name='hidden_address' value='"+self.address+"' >\
				<input type='hidden' name='hidden_square' value='"+self.square+"' >\
				<input type='hidden' name='hidden_region' value='"+self.region+"' >\
				<div id='errorForm' class='alert alert-danger' style='display:none;'></div>\
					<div class=''>\
					</div>\
				</div>\
			</form>\
			<div class='' style='float:right;'>\
				<img src='/sites/all/themes/cdnf/img/pay.png'>\
			</div>"
		);
		
		$("#myModal .modal-body .lb").append(
			"<div id='id_info'>"+
				"<div>"+self.address+"</div>"+
				"<div>"+self.id+"</div>"+
			"</div>"
		);
		
		//self.modal.find(".rb").append("<div class=''>Преимущества</div><div class=''>Отзывы</div>");
		var features="<!--<div class='features'>\
			<ul>\
				<li>\
					<span><span class='icon'></span><span>Выписка подписана ЭЦП Росреестра</span></span>\
					<span><span class='icon'></span><span>Вернем 100%, если не предоставим выписку</span>\
				</li>\
				<li>\
					<span><span class='icon'></span><span>90% заказов менее чем за 10 минут</span></span>\
					<span><span class='icon'></span><span>Бесплатное смс о готовности заказа</span></span>\
				</li>\
				<li>\
					<span><span class='icon'></span><span>Электронаая выписка в PDF и HTML на email</span></span>\
					<span><span class='icon'></span><span>Подбор кадастрового инженера без предоплаты</span></span>\
				</li>\
			</ul>\
		</div>-->\
		<div class='features'>\
			<ul>\
				<li>\
					<div><span>Выписка подписана ЭЦП Росреестра</span></div>\
					<div><span>Вернем 100%, если не предоставим выписку</span></div>\
				</li>\
				<li>\
					<div><span>90% заказов менее чем за 10 минут</span></div>\
					<div><span>Бесплатное смс о готовности заказа</span></div>\
				</li>\
				<li>\
					<div><span>Электронаая выписка в PDF и HTML на email</span></div>\
					<div><span>Подбор кадастрового инженера без предоплаты</span></div>\
				</li>\
			</ul>\
		</div>";
		$("#myModal .modal-body .rb").append(features+"<div class='' style=' text-align: center;'><img src='/sites/all/themes/cdnf/img/reviews.png'></div><div class='buttons '><div class='other_button b_email'>Задать вопрос</div><div class='other_button b_phone'> Позвоните мне</div></div>");

	    // $("#myModal .modal-body .rb").append(form);
		self.setInfoUser();
		var servicesCheckedArray=servicesChecked();
		var summ=servicesCheckedInfo(servicesCheckedArray);
		//console.log("summ="+summ);
		//console.log( self.div.find('#ul_services'));
		self.div.find('#ul_services').clone().appendTo("#form_service_list");

		$("#form_order_id .summ").html(summ+" ₽");

	    $("#myModal").modal("show");
		self.bindSubmit();
		e.preventDefault();
	});

	// обработка экспандера
	$('#right_block').on('click', '.expander', function(e) {
		console.log('expand');
		if (self.div.hasClass( "on" )) {
			self.div.hasClass( "on" );
			self.div.removeClass("on");
			self.div.addClass( "off" );
			self.div.body.slideUp();
			self.div.foot.slideUp();
		} else {
			self.div.removeClass("off");
			self.div.addClass( "on" );
			self.div.body.slideDown();
			self.div.foot.slideDown();
		}
	});

	this.initialize = function() {
		self.div.removeClass( "off" );
		self.div.addClass( "on" );
		var page=getParameterByName('page');
		//console.warn(page);
		if (page !== null && page.length) {
			console.log('get_landing');
			return 'page';
		}
		//    self.div.body.html("<div class='title1' style='padding:10px;'>После выбора объекта вам будут доступны</div>");
		//var id_info="<ul class='features' style='padding:10px;'>"+
		//		"<li><div class='logo_icon'></div><div>Исчерпывающая информация</div></li>"+
		//		"<li><div class='logo_icon'></div><div>Предупреждения о недостающих документах</div></li>"+
		//		"<li><div class='logo_icon'></div><div>Заказ выписок и услуг</div>"+
		//		"<span>Выписки из егрн электронные, с печатью, срочные, любые</span>"+
		//		"<span>Межевание, ГПЗУ, Разрешение на строительство, Технический план и др.</span>"+
		//		"</li>"+
		//		"<li><div class='logo_icon'></div><div>Объекты на продажу и в аренду</div></li>"+
		//		"</ul>";
		// self.div.body.append(id_info);
		self.div.foot.html('');
	};

	function servicesChecked() {
		/*var selected = [];
		$('#right_block #ul_services input:checked').each(function() {
			selected.push($(this).attr('value'));
		});*/
		//console.log( 'servicesChecked');
		//console.log( self.services);
		var servicesChecked=[];
		var searchIDs = $("#right_block #ul_services input:checked").map(function(){
			return $(this).val();
		}).get(); // <----
		//console.log(searchIDs);
		$.each(searchIDs, function( index, value ) {
			// console.log( ": " + value );
			var result = $.grep( self.services, function(e){ return e.tid == value; });
			if (result.length == 1) {
				// access the foo property using result[0].foo
				servicesChecked.push(result[0]);
			}
			// console.log(result);
		});
		return (servicesChecked);
	}
   
	function servicesCheckedInfo(servicesCheckedArray) {
		var summ = 0;
		// Цикл по разделам
		$.each(self.kind, function(n,val_kind) {
			//var kind_item = ul_services.append("<span id='kind"+n+"'>"+val_kind+"</span>");		
			var result = $.grep( servicesCheckedArray, function(e){ return e.field_kind_tid == n; });
			if (result.length > 0) {		
				$("<span class='kind_title'></span>").html(val_kind).appendTo("#form_service_list");
				// цикл по заказам
				$.each(result, function(i,value) {
					if (value.field_price_value !== null) {
						$("<span class='service_item'></span>").html(value.name+" <span class='sum'>"+value.field_price_value+"₽</span>").appendTo("#form_service_list");
						summ+=parseInt(value.field_price_value);
					} else if (value.field_price_from_value !== null) {
						$("<span class='service_item'></span>").html(value.name+"от "+value.field_price_from_value+"").appendTo("#form_service_list");
					} else {
						$("<span class='service_item'></span>").html(value.name).appendTo("#form_service_list");
					}
				});
				$("<span class='kind_description'></span>").html(self.kindInfo[n]).appendTo("#form_service_list");
			}
		});
		return summ;
	}

	this.fillServices = function(id,type,address,square,region,removed=false) {
		this.id = id;
		this.type = type;
		this.address = address;
		this.square = square;
		this.region = region;
		this.div.body.html("");
		this.div.body.html("<div class='title2'>"+address+"</div>");
		if (removed) {
			this.div.body.append("<red>Запрос сведений по аннулированным объектам невозможен.</red>");  	
		} else {
			this.div.body.append("<ul id='ul_services'></ul>");  	
			var ul_services= $('ul#ul_services');
			this.createForm(ul_services);
			this.div.foot.html("");
			//this.div.foot.append("<button type='button' id='pre_order' class='btn order_button'>Заказать</button>");
			this.div.foot.append('<button type="button" id="pre_order" class="egrn-button egrn-button--submit" style="margin:.5rem 0;" disabled>Заказать</button>');
			this.div.scrollTop(0);    
			self.show(); 
		}
	};

	this.clear = function() {
		this.div.html("clear text");
		this.id = false;
		this.type = false;
	};

	this.hide  = function() {
		this.div.slideUp();
	};

	this.show  = function() {
		this.div.slideDown();
	};
  
	this.createForm = function(ul_services) {
		// чтоб блять антон засунул себе в рот этот сраный код и обосрался им потом, а потом снова бы съел

		this.div.addClass('egrn-order-form');

		//ul_services.append(
		//	'<button class="egrn-collapsible_area_trigger egrn-collapsible_area_trigger--expanded" id="egrn-order-trigger-0">' +
		//		'<span class="egrn-collapsible_area_trigger-aligner">' + 
		//			'<span class="egrn-collapsible_area_trigger-title">' +
		//				'Выписки' +
		//			'</span>' +
		//			'<span class="egrn-collapsible_area_trigger-extra"></span>' +
		//			'<span class="egrn-collapsible_area_trigger-indicator">' + 
		//				'<svg class="egrn-collapsible_area_trigger-indicator-icon">' +
		//					'<use xlink:href="#egrn-svg_sprite-icon-chevron"></use>' +
		//				'</svg>' +
		//			'</span>' +
		//		'</span>' +
		//	'</button>'
		//);

		//var servicesElectro = ul_services.append('<div class="egrn-collapsible_area egrn-collapsible_area--expanded" data-egrn-collapsible-area-trigger="#egrn-collapsible_area_trigger-0"></div>');
		//var servicesElectroContent = servicesElectro.append('<div class="egrn-collapsible_area-content"></div>');

		$.each(this.kind, function(n,val_kind) {
			//var kind_item = ul_services.append("<span id='kind"+n+"'><div class='title3'>"+val_kind+"</div></span>");
			//console.log(kind_item);
			
			//var parent = n == 28 ? ul_services : servicesElectroContent;
			
			ul_services.append(
				'<button class="egrn-collapsible_area_trigger egrn-collapsible_area_trigger--alt-font ' + (n == 26 ? 'egrn-collapsible_area_trigger--expanded' : 'egrn-collapsible_area_trigger--collapsed') + '" id="egrn-order-trigger-' + n + '">' +
					'<span class="egrn-collapsible_area_trigger-aligner">' + 
						'<span class="egrn-collapsible_area_trigger-title">' +
							val_kind +
						'</span>' +
						'<span class="egrn-collapsible_area_trigger-extra"></span>' +
						'<span class="egrn-collapsible_area_trigger-indicator">' + 
							'<svg class="egrn-collapsible_area_trigger-indicator-icon">' +
								'<use xlink:href="#egrn-svg_sprite-icon-chevron"></use>' +
							'</svg>' +
						'</span>' +
					'</span>' +
				'</button>' +
				'<div class="egrn-collapsible_area ' + (n == 26 ? 'egrn-collapsible_area--expanded' : 'egrn-collapsible_area--collapsed') + '" data-egrn-collapsible-area-trigger="#egrn-order-trigger-' + n + '">' +
					'<div class="egrn-collapsible_area-content" id="kind' + n + '"></div>' + 
				'</div>'
			);
		});

		$.each(this.services, function(i,val) {
			var li = $('<li/>').addClass('ui-menu-item').attr('role', 'menuitem').appendTo($("#kind"+val.field_kind_tid));
			var input = $('<input/>').addClass('ui-all').attr('type', 'checkbox').attr('name', 'options[]').attr('value', val.tid).attr('id', 'v'+val.tid).appendTo(li);
			aaa=$("<label class='control-label' ></label>").attr('for', 'v'+val.tid).html('<strong>' + val.name + '</strong>').appendTo(li);
			$('<span class="description">' + val.description + '</span>').appendTo(li);
			//$("<span class='description' ></span>").text(val.description).appendTo(li);
			
			//    var li = $('<li/>').addClass('ui-menu-item').attr('role', 'menuitem').appendTo($("#kind"+val.field_kind_tid));
			//var input = $('<input/>').addClass('ui-all').attr('type', 'checkbox').attr('name', 'options[]').attr('value', val.tid).attr('id', 'v'+val.tid).appendTo(li);
			//aaa=$("<label class='control-label' ></label>").attr('for', 'v'+val.tid).text(val.name).appendTo(li);
			//$("<span class='description' ></span>").text(val.description).appendTo(li);
			// --------------------------------------
			//       var aaa = $('<span/>').addClass('ui-all').text(val.name).appendTo(li);
			// var aaaTooltip = aaa.append(	'<a href="#" data-toggle="tooltip" data-placement="right" title="tooltip info"> i</a>');
		});

		EGRN.utils.applyWidget(EGRN.CollapsibleArea);
	}

	this.bindSubmit = function() {
		$("#orderForm").submit(function(e) {
			console.log($("#orderForm"));
			//console.log($("#orderForm").serialize());
			//$("#myModal .modal-body").html(""); //clear
			//$("#myModal .modal-body").append("<div id='message'>Successful</div>");
			// setTimeout(function () {  $("#myModal").modal("hide") }, 2000);

			var url = "/order_submit"; // the script where you handle the form input.
			var dataForm=$("#orderForm").serialize();
			console.log(dataForm);

			$.ajax({
			   type: "POST",
			   url: url,
			   data:  dataForm,// serializes the form's elements.
			   success: function(dat)
			   {
					pD = $.parseJSON(dat);
					console.log(pD);
					if (pD.success && pD.type=='order') { // Успешное создание заказа
						console.log('Заказ создан');
						$("#myModal .modal-body").html(""); //clear
						$("#myModal .modal-body").append("<div id='message'>Заказ успешно создан. Номер заказа "+pD.orderId+"</div>");
						setTimeout(function () { $("#myModal").modal("hide") }, 5000);
					} else if(pD.success && pD.type=='payUrl') { //редирект на оплату
						window.location.href = pD.formUrl;

					} else { //Обработка массива ошибок
						//pD.errors
						var errorStr=pD.errors.map(function(elem){
							return elem.message;
						}).join("<br>");
						console.log(errorStr);
						$('#errorForm').html('' + errorStr);
						$('#errorForm').fadeIn();
					}
				},
				error: function (jqXHR) {
					// $('#post').html(msg);
					console.log(jqXHR);
				},
			});

			console.log("submit");
			e.preventDefault(); // avoid to execute the actual submit of the form.
		});
	}

	this.setInfoUser  = function() {
		var url = "/lk_user_info"; // the script where you handle the form input.

		var ajret = $.ajax({
			type: "GET",
			async: false,
			url: url,
			// data: dataForm, // serializes the form's elements.
			success: function(dat) {
				console.log(dat);
				/*if (dat.is_logged) {
					var retstr=" <input type='email' name='email' placeholder='Email' value='"+dat.email+"' required><br>\
					<!--<input type='tel' name='tel' placeholder='Телефон'><br>\
					<input type='text' name='firstname' placeholder='Имя'><br>-->\
					<textarea name='annotation' cols='30' rows='2' placeholder='Примечание'></textarea>\
					<input type='hidden' name='hidden_id' value='"+self.id+"' ><br>\
					<div id='errorForm' class='alert alert-danger' style='display:none;'></div><br>\
					<input type='submit' value='ЗАКАЗАТЬ'>";
					
					$("#form_order_id").html(retstr);
					
				}*/
				if (dat.is_logged) {
					//set email
					$("#form_order_id input[name=email]").attr('value', dat.email);		
					//set phone
					//set name
				}
			},
			error: function (jqXHR) {
				// $('#post').html(msg);
				console.log(jqXHR);
			},
		});
		return ajret;
	};

};

var rblock = new Rblock();
rblock.initialize();

function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	    results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}




