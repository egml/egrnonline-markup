<?php
	$baseUrl = '..';
	$pageTitle = 'Панель инструментов';
?>
<?php include $baseUrl . '/partials/header.php' ?>

<!-- Highlight.js -->
<style>
	@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

<!-- Выделение кода при клике на блок с кодом -->
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var codeBlocks = document.querySelectorAll('.code-block');
		for (var i = 0; i < codeBlocks.length; i++) {
			codeBlocks[i].addEventListener('click', function() {
				var selection = window.getSelection();
				var range = document.createRange();
				range.selectNodeContents(this);
				selection.removeAllRanges();
				selection.addRange(range);
			});
		}
	});
</script>

<style>
	@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

	body {
		/* background: #f7f8f9; */
	}
</style>

<div class="content">

	<h1><?= $pageTitle ?></h1>

	<div class="egrn-toolbar">

		<div class="egrn-tool egrn-search_tool egrn-search_tool--invitation">
			<label class="egrn-tool-icon" for="egrn-search_tool-field-input-1">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-search"></use></svg>
			</label>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Поиск объекта</div></div></div>

				<div class="egrn-search_tool-field">
					<div class="egrn-search_tool-field-input">
						<input class="egrn-search_tool-field-input-control" id="egrn-search_tool-field-input-1" type="search" autofocus autocomplete="off"><!-- Этот комментарий важен. В этот контейнер может быть вставлен еще контент. И рассчитано, что пробела между полем ввода и новым контентом не должно быть, поэтому здесь комментарий обязателен.
					--></div>

					<div class="egrn-search_tool-field-submit">
						<div class="egrn-search_tool-field-submit-clip">
							<button class="egrn-search_tool-field-submit-control" type="button" disabled>Найти</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-layers2"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Настройки карты</div></div></div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-ruler"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Измерения</div></div></div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-printer"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Печать</div></div></div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon egrn-tool-icon--scale-in">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-360"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Яндекс Панорамы</div></div></div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-location"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Ваше местоположение</div></div></div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-link"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Поделиться</div></div></div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-mail2"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Обратная связь</div></div></div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-circle-question"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Справка</div></div></div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon egrn-tool-icon--special egrn-tool-icon--scale-in">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-housevalue"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Поиск недвижимости</div></div></div>
			</div>
		</div>

		<div class="egrn-tool">
			<div class="egrn-tool-icon">
				<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-cog"></use></svg>
			</div>
			<div class="egrn-tool-ui">
				<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Состав меню</div></div></div>
			</div>
		</div>

	</div>

	<div class="margin"></div>
	<div class="margin"></div>

	<div class="items">
		<div class="item">
			<strong>Панель инструментов:</strong>

			<br><br>

			<button class="egrn-button" onclick="EGRN.Toolbar.prototype.instances[1].collapse()">
				<svg class="egrn-button-icon egrn-button-icon--turn-left">
					<use xlink:href="#egrn-svg_sprite-icon-arrow"/>
				</svg>
				<span class="egrn-button-text">Свернуть</span>
			</button>

			&nbsp;

			<button class="egrn-button" onclick="EGRN.Toolbar.prototype.instances[1].expand()">
				<svg class="egrn-button-icon egrn-button-icon--turn-right">
					<use xlink:href="#egrn-svg_sprite-icon-arrow"/>
				</svg>
				<span class="egrn-button-text">Развернуть</span>
			</button>
		</div>
		<div class="item">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</div>
		<div class="item">
			<strong>Инструмент поиска:</strong><br><br>

			<button class="egrn-button" onclick="EGRN.Tool.prototype.instances[1].activate()">Активировать</button>

			&nbsp;

			<button class="egrn-button" onclick="EGRN.Tool.prototype.instances[1].deactivate()">Деактивировать</button>
		</div>
	</div>


	<div class="usage"><span>Использование:</span></div>

<?php
	$code = <<<CODE
<div class="egrn-toolbar egrn-toolbar--expanded">

	<div class="egrn-tool egrn-tool--active egrn-search_tool egrn-search_tool--invitation">
		<label class="egrn-tool-icon" for="egrn-search_tool-field-input-1">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-search"></use></svg>
		</label>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Поиск объекта</div></div></div>

			<div class="egrn-search_tool-field">
				<div class="egrn-search_tool-field-input">
					<input class="egrn-search_tool-field-input-control" id="egrn-search_tool-field-input-1" type="search" autofocus autocomplete="off"><!-- Этот комментарий важен. В этот контейнер может быть вставлен еще контент. И рассчитано, что пробела между полем ввода и новым контентом не должно быть, поэтому здесь комментарий обязателен.
				--></div>

				<div class="egrn-search_tool-field-submit">
					<div class="egrn-search_tool-field-submit-clip">
						<button class="egrn-search_tool-field-submit-control" type="button" disabled>Найти</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-layers2"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Настройки карты</div></div></div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-ruler"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Измерения</div></div></div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-printer"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Печать</div></div></div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon egrn-tool-icon--scale-in">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-360"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Яндекс Панорамы</div></div></div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-location"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Ваше местоположение</div></div></div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-link"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Поделиться</div></div></div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-mail2"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Обратная связь</div></div></div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-circle-question"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Справка</div></div></div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon egrn-tool-icon--special egrn-tool-icon--scale-in">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-housevalue"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Поиск недвижимости</div></div></div>
		</div>
	</div>

	<div class="egrn-tool">
		<div class="egrn-tool-icon">
			<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-cog"></use></svg>
		</div>
		<div class="egrn-tool-ui">
			<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Состав меню</div></div></div>
		</div>
	</div>

</div>
CODE;
	echo '<pre><code class="code-block code-block--label html">' . htmlspecialchars($code) . '</code></pre>';
?>


</div>

<script>
	window.addEventListener('load', function() {
		EGRN.utils.createWidgets(EGRN.Toolbar);
	});
</script>

<?php /* ?>
<script src="<?= $assetsUrl ?>/js/egrn-demo_name.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
<?php */ ?>
	
<?php include $baseUrl . '/partials/footer.php' ?>