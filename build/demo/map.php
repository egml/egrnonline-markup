<?php
	$baseUrl = '..';
	$pageTitle = 'Карта';
?>
<?php include $baseUrl . '/partials/header.php' ?>

<script>
	window.addEventListener('load', function() {
		EGRN.utils.createWidgets(EGRN.Map);
	});
</script>

<?php /* ?>
<?php */ ?>
<?php /* ?>
<!-- Highlight.js -->
<style>
	@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

<!-- Выделение кода при клике на блок с кодом -->
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var codeBlocks = document.querySelectorAll('.code-block');
		for (var i = 0; i < codeBlocks.length; i++) {
			codeBlocks[i].addEventListener('click', function() {
				var selection = window.getSelection();
				var range = document.createRange();
				range.selectNodeContents(this);
				selection.removeAllRanges();
				selection.addRange(range);
			});
		}
	});
</script>
<?php /**/ ?>

<style>
	@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

	body {
		/* background: #f7f8f9; */
	}
</style>

<div class="content">
	<h1><?= $pageTitle ?></h1>

	<div class="egrn-map"></div>
	
	<?php /* ?>

	<div class="usage"><span>Использование:</span></div>

	<div class="items">
		<div class="item">
			<button class="egrn-button">Обычная</button>
		</div>
		<!-- <div class="item">
			<a class="egrn-button" href="#">Обычная</a>
		</div>
		<div class="item">
			<span class="egrn-button">Обычная</span>
		</div> -->
	</div>

	<?php */ ?>

<?php /* ?>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--tight egrn-button--bigger-text">
	Узкая с увеличенным текстом
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>


<?php
	$code = <<<CODE
document.addEventListener('egrn.appready', function() {
	var area = document.querySelector('.egrn-collapsible_area');
	new EGRN.CollapsibleArea(area);
});
CODE;
	echo '<pre><code class="code-block code-block--label js">' . htmlspecialchars($code) . '</code></pre>';
?>

<?php */ ?>

</div>

<?php /* ?>
<script src="<?= $assetsUrl ?>/js/egrn-demo_name.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
<?php */ ?>
	
<?php include $baseUrl . '/partials/footer.php' ?>