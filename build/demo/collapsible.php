<?php
	$baseUrl = '..';
	$title = 'Сворачиваемые области';
	$pageTitle = $title;
?>
<?php include $baseUrl . '/partials/header.php' ?>

	<!-- Highlight.js -->
	<style>
		/* @import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css'; */
		@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
	</style>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
	<script>hljs.initHighlightingOnLoad();</script>

	<!-- Выделение кода при клике на блок с кодом -->
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			var codeBlocks = document.querySelectorAll('.code-block');
			for (var i = 0; i < codeBlocks.length; i++) {
				codeBlocks[i].addEventListener('click', function() {
					var selection = window.getSelection();
					var range = document.createRange();
					range.selectNodeContents(this);
					selection.removeAllRanges();
					selection.addRange(range);
				});
			}
		});
	</script>

	<style>
		@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

		body {
			background: #f7f8f9;
			background: #fff;
		}
		.block {
			/* display: inline-block; */
			width: 20rem;
			height: 10rem;
			border: thin dashed silver;
			margin: 2rem 0;
		}
		.block-content {
			line-height: 10rem;
			padding: 1rem;
		}
		.image {
			max-width: 20rem;
			max-height: 10rem;
			display: block;
			margin: 2rem 0;
		}

		/* .egrn-collapsible {
			outline: thin dashed dodgerblue;
		} */

		.code-block {
			border: thin solid lightgrey;
		}
	</style>

	<div class="content">

		<h1><?= $title ?></h1>

		<?php /* ?>
		<?php */ ?>
		<?php /* ?>
		<div style="width:29rem;">

			<button class="egrn-collapsible_trigger egrn-collapsible_trigger--collapsed egrn-collapsible_trigger--alt-font" id="egrn-collapsible_trigger-1">
				<span class="egrn-collapsible_trigger-aligner">
					<span class="egrn-collapsible_trigger-title">Выписки</span>
					<span class="egrn-collapsible_trigger-extra"></span>
					<span class="egrn-collapsible_trigger-indicator">
						<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
					</span>
				</span>
			</button>

			<div class="egrn-collapsible egrn-collapsible--collapsed" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-1">
				<div class="egrn-collapsible-clip egrn-clip">
					<div class="egrn-collapsible-clip-content egrn-clip-content">
						<p>
							Выписка из ЕГРН<br>
							<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
						</p>
						<p>
							Выписка из ЕГРН о переходе прав<br>
							<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
						</p>
						<p>
							Справка о кадастровой стоимости<br>
							<span class="egrn-text egrn-text--muted">49 ₽, 1-3 рабочих дня</span>
						</p>
						<p style="margin-bottom:1rem;">
							<a class="egrn-link egrn-link--pseudo" href="#">Пакет выписок из ЕГРН</a> (5 - 1000 шт.)<br>
							<span class="egrn-text egrn-text--highlighted">от 10 р. за выписку</span>
						</p>
					</div>
				</div>
			</div>

		</div>
		
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
		<?php /**/ ?>

		<?php /* ?>
		<?php /* ?>
		<?php */ ?>
		<div style="width:29rem;">
			<div style="margin:1.4rem 0;">
				<button class="egrn-collapsible_trigger egrn-collapsible_trigger--alt-font" id="egrn-collapsible_trigger-1">
					<span class="egrn-collapsible_trigger-aligner">
						<span class="egrn-collapsible_trigger-title">Выписки</span>
						<span class="egrn-collapsible_trigger-extra"></span>
						<span class="egrn-collapsible_trigger-indicator">
							<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
						</span>
					</span>
				</button>

				<div class="egrn-collapsible egrn-collapsible--optimize-animation_" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-1">
					<div class="egrn-collapsible-clip egrn-clip">
						<div class="egrn-collapsible-clip-content egrn-clip-content">
							<div style="margin:1.4rem 0;">
								<button class="egrn-collapsible_trigger egrn-collapsible_trigger--collapsed egrn-collapsible_trigger--inline" id="egrn-collapsible_trigger-2">
									<span class="egrn-collapsible_trigger-aligner">
										<span class="egrn-collapsible_trigger-title">Электронные</span>
										<span class="egrn-collapsible_trigger-extra"></span>
										<span class="egrn-collapsible_trigger-indicator">
											<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
										</span>
									</span>
								</button>
								&nbsp; &nbsp; <a class="egrn-link egrn-link--dashed" href="#">Образцы</a>
								
								<div class="egrn-collapsible egrn-collapsible--collapsed" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-2">
									<div class="egrn-collapsible-clip egrn-clip">
										<div class="egrn-collapsible-clip-content egrn-clip-content">
											<p class="egrn-paragraph--margin-15">
												Выписка из ЕГРН<br>
												<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
											</p>
											<p class="egrn-paragraph--margin-15">
												Выписка из ЕГРН о переходе прав<br>
												<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
											</p>
											<p class="egrn-paragraph--margin-15">
												Справка о кадастровой стоимости<br>
												<span class="egrn-text egrn-text--muted">49 ₽, 1-3 рабочих дня</span>
											</p>
											<p class="egrn-paragraph--margin-15" style="margin-bottom:1rem;">
												<a class="egrn-link egrn-link--pseudo" href="#">Пакет выписок из ЕГРН</a> (5 - 1000 шт.)<br>
												<span class="egrn-text egrn-text--highlighted">от 10 р. за выписку</span>
											</p>
										</div>
									</div>
								</div>
							</div>

							<div style="margin:1.4rem 0 0;">
								<button class="egrn-collapsible_trigger egrn-collapsible_trigger--collapsed egrn-collapsible_trigger--inline" id="egrn-collapsible_trigger-3">
									<span class="egrn-collapsible_trigger-aligner">
										<span class="egrn-collapsible_trigger-title">Бумажные с печатью</span>
										<span class="egrn-collapsible_trigger-extra"></span>
										<span class="egrn-collapsible_trigger-indicator">
											<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
										</span>
									</span>
								</button>
								&nbsp; &nbsp; <a class="egrn-link egrn-link--dashed" href="#">Образцы</a>
								
								<div class="egrn-collapsible egrn-collapsible--collapsed" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-3">
									<div class="egrn-collapsible-clip egrn-clip">
										<div class="egrn-collapsible-clip-content egrn-clip-content">
											<p class="egrn-paragraph--margin-15">
												Выписка из ЕГРН основные характеристики<br>
												<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
											</p>
											<p class="egrn-paragraph--margin-15">
												Выписка из ЕГРН о переходе прав<br>
												<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
											</p>
											<p class="egrn-paragraph--margin-15">
												Справка о кадастровой стоимости<br>
												<span class="egrn-text egrn-text--muted">49 ₽, 1-3 рабочих дня</span>
											</p>
											<p class="egrn-paragraph--margin-15" style="margin-bottom:.8rem;">
												<a class="egrn-link egrn-link--pseudo" href="#">Пакет выписок из ЕГРН</a> (5 - 1000 шт.)<br>
												<span class="egrn-text egrn-text--highlighted">от 10 р. за выписку</span>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div style="margin:1.4rem 0;">
				<button class="egrn-collapsible_trigger egrn-collapsible_trigger--alt-font egrn-collapsible_trigger--collapsed" id="egrn-collapsible_trigger-4">
					<span class="egrn-collapsible_trigger-aligner">
						<span class="egrn-collapsible_trigger-title">Кадастровые услуги</span>
						<span class="egrn-collapsible_trigger-extra"></span>
						<span class="egrn-collapsible_trigger-indicator">
							<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
						</span>
					</span>
				</button>

				<div class="egrn-collapsible egrn-collapsible--collapsed" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-4">
					<div class="egrn-collapsible-clip egrn-clip">
						<div class="egrn-collapsible-clip-content egrn-clip-content">
							<p class="egrn-paragraph--margin-15">
								Кадастровые услуги
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="usage"><span>Использование:</span></div>

		<button class="egrn-collapsible_trigger" id="egrn-collapsible_trigger-5">
			<span class="egrn-collapsible_trigger-aligner">
				<span class="egrn-collapsible_trigger-title">Сворачиваемая область</span>
				<span class="egrn-collapsible_trigger-extra"></span>
				<span class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</span>
			</span>
		</button>

		<div class="egrn-collapsible" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-5">
			<div class="egrn-collapsible-clip egrn-clip">
				<div class="egrn-collapsible-clip-content egrn-clip-content">
					<p class="egrn-paragraph--margin-15">
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur recusandae quaerat neque omnis quis perspiciatis porro possimus facilis earum nobis, consectetur blanditiis, repellendus mollitia doloribus laudantium consequatur dolores non quasi.
					</p>
				</div>
			</div>
		</div>
		
<?php
	$code = <<<CODE
<button class="egrn-collapsible_trigger" id="trigger1">
	<span class="egrn-collapsible_trigger-aligner">
		<span class="egrn-collapsible_trigger-title">
			Сворачиваемая область
		</span>
		<span class="egrn-collapsible_trigger-extra"></span>
		<span class="egrn-collapsible_trigger-indicator">
			<svg class="egrn-collapsible_trigger-indicator-icon">
				<use xlink:href="#egrn-svg_sprite-icon-chevron"/>
			</svg>
		</span>
	</span>
</button>

<div class="egrn-collapsible" data-egrn-collapsible-trigger="#trigger1">
	<div class="egrn-collapsible-clip egrn-clip">
		<div class="egrn-collapsible-clip-content egrn-clip-content">
			<p>
				Lorem ipsum dolor sit amet consectetur adipisicing elit.
				Pariatur recusandae quaerat neque omnis quis perspiciatis 
				porro possimus facilis earum nobis, consectetur blanditiis, 
				repellendus mollitia doloribus laudantium consequatur 
				dolores non quasi.
			</p>
		</div>
	</div>
</div>
CODE;
	echo '<pre><code class="code-block code-block--label html">' . htmlspecialchars($code) . '</code></pre>';
?>
<?php
	$code = <<<CODE
document.addEventListener('egrn.appready', function() {
	var area = document.querySelector('.egrn-collapsible');
	new EGRN.Collapsible(area);
});
CODE;
	echo '<pre><code class="code-block code-block--label js">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>Изначально свернутая:</p>

		<button class="egrn-collapsible_trigger egrn-collapsible_trigger--collapsed" id="egrn-collapsible_trigger-6">
			<span class="egrn-collapsible_trigger-aligner">
				<span class="egrn-collapsible_trigger-title">Сворачиваемая область</span>
				<span class="egrn-collapsible_trigger-extra"></span>
				<span class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</span>
			</span>
		</button>

		<div class="egrn-collapsible egrn-collapsible--collapsed" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-6">
			<div class="egrn-collapsible-clip egrn-clip">
				<div class="egrn-collapsible-clip-content egrn-clip-content">
					<p>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Pariatur recusandae quaerat neque omnis quis perspiciatis porro possimus facilis earum nobis, consectetur blanditiis, repellendus mollitia doloribus laudantium consequatur dolores non quasi.
					</p>
				</div>
			</div>
		</div>
		
<?php
	$code = <<<CODE
<button class="egrn-collapsible_trigger egrn-collapsible_trigger--collapsed" id="trigger1">
	<span class="egrn-collapsible_trigger-aligner">
		<span class="egrn-collapsible_trigger-title">
			Сворачиваемая область
		</span>
		<span class="egrn-collapsible_trigger-extra"></span>
		<span class="egrn-collapsible_trigger-indicator">
			<svg class="egrn-collapsible_trigger-indicator-icon">
				<use xlink:href="#egrn-svg_sprite-icon-chevron"/>
			</svg>
		</span>
	</span>
</button>

<div class="egrn-collapsible egrn-collapsible--collapsed" data-egrn-collapsible-trigger="#trigger1">
	<div class="egrn-collapsible-clip egrn-clip">
		<div class="egrn-collapsible-clip-content egrn-clip-content">
			<p>
				Lorem ipsum dolor sit amet consectetur adipisicing elit.
				Pariatur recusandae quaerat neque omnis quis perspiciatis 
				porro possimus facilis earum nobis, consectetur blanditiis, 
				repellendus mollitia doloribus laudantium consequatur 
				dolores non quasi.
			</p>
		</div>
	</div>
</div>
CODE;
	echo '<pre><code class="code-block code-block--label html">' . htmlspecialchars($code) . '</code></pre>';
?>
<?php
	$code = <<<CODE
var area = document.querySelector('.egrn-collapsible');
new EGRN.Collapsible(area);
CODE;
	echo '<pre><code class="code-block code-block--label js">' . htmlspecialchars($code) . '</code></pre>';
?>

		<h2>Тригеры</h2>

		<div class="egrn-collapsible_trigger egrn-collapsible_trigger--inactive">
			<div class="egrn-collapsible_trigger-aligner">
				<div class="egrn-collapsible_trigger-title">Новый заказ</div>
				<span class="egrn-collapsible_trigger-extra"></span>
				<div class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</div>
			</div>
		</div>

		<br>

		<div class="egrn-collapsible_trigger">
			<div class="egrn-collapsible_trigger-aligner">
				<div class="egrn-collapsible_trigger-title">№2877 от 11.01.18</div>
				<div class="egrn-collapsible_trigger-extra">
					(ул. Поречная 12, ул. Верхние поля 17: Бум. выписка из ЕГРН, эл. выписка из ЕГРН, межевание)
				</div>
				<div class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</div>
			</div>
		</div>

		<br>

		<div class="egrn-collapsible_trigger egrn-collapsible_trigger--alt-font">
			<div class="egrn-collapsible_trigger-aligner">
				<div class="egrn-collapsible_trigger-title">Кадастровые услуги</div>
				<span class="egrn-collapsible_trigger-extra"></span>
				<div class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</div>
			</div>
		</div>

		<br>

		<div class="egrn-collapsible_trigger egrn-collapsible_trigger--inline" style="width:28rem;">
			<div class="egrn-collapsible_trigger-aligner">
				<div class="egrn-collapsible_trigger-title">Электронные выписки</div>
				<span class="egrn-collapsible_trigger-extra"></span>
				<div class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</div>
			</div>
		</div>
		&nbsp; &nbsp; <a class="egrn-link egrn-link--dashed" href="#">Образцы</a>

		<div class="margin"></div>

		<div class="egrn-collapsible_trigger egrn-collapsible_trigger--inactive egrn-collapsible_trigger--dark">
			<div class="egrn-collapsible_trigger-aligner">
				<div class="egrn-collapsible_trigger-title">Новый заказ</div>
				<span class="egrn-collapsible_trigger-extra"></span>
				<div class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</div>
			</div>
		</div>

		<br>

		<div class="egrn-collapsible_trigger egrn-collapsible_trigger--dark">
			<div class="egrn-collapsible_trigger-aligner">
				<div class="egrn-collapsible_trigger-title">№2877 от 11.01.18</div>
				<div class="egrn-collapsible_trigger-extra">
					(ул. Поречная 12, ул. Верхние поля 17: Бум. выписка из ЕГРН, эл. выписка из ЕГРН, межевание)
				</div>
				<div class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</div>
			</div>
		</div>

		<br>

		<div class="egrn-collapsible_trigger egrn-collapsible_trigger--alt-font egrn-collapsible_trigger--dark">
			<div class="egrn-collapsible_trigger-aligner">
				<div class="egrn-collapsible_trigger-title">Кадастровые услуги</div>
				<span class="egrn-collapsible_trigger-extra"></span>
				<div class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</div>
			</div>
		</div>

		<br>

		<div class="egrn-collapsible_trigger egrn-collapsible_trigger--inline egrn-collapsible_trigger--dark" style="width:28rem;">
			<div class="egrn-collapsible_trigger-aligner">
				<div class="egrn-collapsible_trigger-title">Электронные выписки</div>
				<span class="egrn-collapsible_trigger-extra"></span>
				<div class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</div>
			</div>
		</div>
		&nbsp; &nbsp; <a class="egrn-link egrn-link--dashed" href="#">Образцы</a>

		<div class="margin"></div>

		На теге DIV:<br><br>

		<div class="egrn-collapsible_trigger">
			<div class="egrn-collapsible_trigger-aligner">
				<div class="egrn-collapsible_trigger-title">Электронные выписки</div>
				<div class="egrn-collapsible_trigger-extra">
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit illum a illo animi rem magni nesciunt, enim porro nobis maiores non quae obcaecati recusandae iusto molestiae necessitatibus eaque aliquid facilis?
				</div>
				<div class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</div>
			</div>
		</div>

		<div class="margin"></div>
		
		На теге SPAN:<br><br>

		<span class="egrn-collapsible_trigger">
			<span class="egrn-collapsible_trigger-aligner">
				<span class="egrn-collapsible_trigger-title">Электронные выписки</span>
				<span class="egrn-collapsible_trigger-extra">
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit illum a illo animi rem magni nesciunt, enim porro nobis maiores non quae obcaecati recusandae iusto molestiae necessitatibus eaque aliquid facilis?
				</span>
				<span class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</span>
			</span>
		</span>

		<div class="margin"></div>
		
		На теге BUTTON:<br><br>

		<button class="egrn-collapsible_trigger">
			<span class="egrn-collapsible_trigger-aligner">
				<span class="egrn-collapsible_trigger-title">Электронные выписки</span>
				<span class="egrn-collapsible_trigger-extra">
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit illum a illo animi rem magni nesciunt, enim porro nobis maiores non quae obcaecati recusandae iusto molestiae necessitatibus eaque aliquid facilis?
				</span>
				<span class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</span>
			</span>
		</button>

	</div>
	
	<?php /**/ ?>

	<script>
		window.addEventListener('load', function() {
			EGRN.utils.createWidgets(EGRN.Collapsible);
		});
	</script>

	<?php /* ?>
	<script src="<?= $assetsUrl ?>/js/egrn-demo_collapsible.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
	<?php */ ?>

<?php include $baseUrl . '/partials/footer.php' ?>