<?php
	$baseUrl = '..';
	$pageTitle = 'Индикатор загрузки';
?>
<?php include $baseUrl . '/partials/header.php' ?>

<?php /* ?>
<?php */ ?>
<?php /* ?>
<!-- Highlight.js -->
<style>
	@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

<!-- Выделение кода при клике на блок с кодом -->
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var codeBlocks = document.querySelectorAll('.code-block');
		for (var i = 0; i < codeBlocks.length; i++) {
			codeBlocks[i].addEventListener('click', function() {
				var selection = window.getSelection();
				var range = document.createRange();
				range.selectNodeContents(this);
				selection.removeAllRanges();
				selection.addRange(range);
			});
		}
	});
</script>
<?php /**/ ?>

<style>
	@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

	body {
		/* background: #f7f8f9; */
	}
	svg {
		width: auto;
		height: auto;
	}
	svg:not(:root) {
		overflow: visible;
	}
	#loader-aligner {
		display: flex;
		position: fixed;
		width: 100%;
		height: 100%;
		align-items: center;
		justify-content: center;
		left: 0;
		top: 0;
	}
	#egrn-loader {
		width: 3rem;
		opacity: 0;
		/* transform: translateY(-2rem); */
	}
</style>

<script src="https://unpkg.com/animejs@3.0.1/lib/anime.js"></script>

<script>
	document.addEventListener('DOMContentLoaded', function() {
		anime({
			targets: '#egrn-loader',
			easing: 'linear',
			opacity: 1,
			translateY: ['-2rem', 0],
		});

		function spin() {

			var s = [];

			s.push(function() {
				var greenStart = parseFloat(document.getElementById('egrn-loader-green').getAttribute('r'));
				var green = anime.timeline({
					easing: 'easeOutBack',
					duration: 500,
				});
				green.add({
					targets: '#egrn-loader-green',
					r: 14,
				}).add({
					targets: '#egrn-loader-green',
					r: greenStart,
				});
			});
			
			s.push(function() {
				var redStart = parseFloat(document.getElementById('egrn-loader-red').getAttribute('width'));
				var red = anime.timeline({
					easing: 'easeOutBack',
					duration: 500,
				});
				red.add({
					targets: '#egrn-loader-red',
					// width: 18,
					width: 17.3,
				}).add({
					targets: '#egrn-loader-red',
					width: redStart,
				});
			});

			s.push(function() {
				var blueStart = parseFloat(document.getElementById('egrn-loader-blue').getAttribute('x'));
				var blue = anime.timeline({
					easing: 'easeOutBack',
					duration: 500,
				});
				blue.add({
					targets: '#egrn-loader-blue',
					// x: -8,
					x: -5.5,
				}).add({
					targets: '#egrn-loader-blue',
					x: blueStart,
				});
			});

			s.push(function() {
				var yellowStart = parseFloat(document.getElementById('egrn-loader-yellow').getAttribute('r'));
				var yellow = anime.timeline({
					easing: 'easeOutBack',
					duration: 500,
				});
				yellow.add({
					targets: '#egrn-loader-yellow',
					// r: 12.5,
					r: 11.5,
				}).add({
					targets: '#egrn-loader-yellow',
					r: yellowStart,
				});
			});

			var offset = 0;
			s.forEach(function(part, i) {
				if (i == 0) {
					part();
				} else {
					window.setTimeout(part, offset += 65);
				}
			});

		}
		
		// spin();
		var interval = window.setInterval(spin, 2000);

		document.documentElement.addEventListener('click', function() {
			window.clearInterval(interval);
		});
	});
</script>

<div class="content">
	<h1 style="text-align:center"><?= $pageTitle ?></h1>
	
	<div id="loader-aligner">

		<?php include 'assets/egrn-loader.svg' ?>

	</div>

	<?php /* ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/svg.js/3.0.11/svg.js"></script>
	<script>
		var draw = SVG('svg8');
		draw.rect(100, 100).move(100, 50).fill('#f06');
	</script>
	<?php */ ?>
	
	<?php /* ?>

	<div class="usage"><span>Использование:</span></div>

	<div class="items">
		<div class="item">
			<button class="egrn-button">Обычная</button>
		</div>
		<!-- <div class="item">
			<a class="egrn-button" href="#">Обычная</a>
		</div>
		<div class="item">
			<span class="egrn-button">Обычная</span>
		</div> -->
	</div>

	<?php */ ?>

<?php /* ?>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--tight egrn-button--bigger-text">
	Узкая с увеличенным текстом
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>


<?php
	$code = <<<CODE
document.addEventListener('egrn.appready', function() {
	var area = document.querySelector('.egrn-collapsible_area');
	new EGRN.CollapsibleArea(area);
});
CODE;
	echo '<pre><code class="code-block code-block--label js">' . htmlspecialchars($code) . '</code></pre>';
?>

<?php */ ?>

</div>

<?php /* ?>
<script src="<?= $assetsUrl ?>/js/egrn-demo_name.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
<?php */ ?>
	
<?php include $baseUrl . '/partials/footer.php' ?>