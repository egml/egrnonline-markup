<?php
	$baseUrl = '..';
	$pageTitle = 'Правый блок';
?>
<?php include $baseUrl . '/partials/header.php' ?>

<div class="egrn-box" style="width:45rem; margin:5rem auto;">
	<div class="egrn-section">

		<h1>Выписка из ЕГРН</h1>
		<div class="egrn-h1"><span class="egrn-heading-smaller_line">Для заказа воспользуйтесь поиском</span></div>
		<p>Для заказа выписки из ФГИС ЕГРН Росреестра&nbsp;введите кадастровый номер или адрес объекта, либо найдите его на публичной кадастровой карте<br>&nbsp;</p>
		<table width="279" height="25" cellspacing="0" cellpadding="0" border="0">
			<tbody>
				<tr>
					<td>
						<h3>Электронная выписка из ЕГРН</h3>
					</td>
					<td><a class="egrn-link egrn-link--pseudo" href="#">Образцы</a></td>
				</tr>
			</tbody>
		</table>
		<p><strong class="egrn-text-emphasis--no-bold">Выписка из ЕГРН об основных характеристиках объекта недвижимости</strong><br><span class="egrn-text-muted">49 ₽, готовность: 5-30 мин</span></p>
		<p><strong class="egrn-text-emphasis--no-bold">Выписка из ЕГРН о переходе прав</strong><br><span class="egrn-text-muted">49 ₽, готовность: 5-30 мин</span></p>
		<p><strong class="egrn-text-emphasis--no-bold">Справка о кадастровой стоимости</strong><br><span class="egrn-text-muted">49 ₽, готовность: 5-30 мин</span><br>&nbsp;</p>
		<table width="286" height="42" cellspacing="0" cellpadding="0" border="0" align="left">
			<tbody>
				<tr>
					<td style="white-space: nowrap; width: 0px;">
						<h3>Выписка из ЕГРН с печатью&nbsp;&nbsp;&nbsp;</h3>
					</td>
					<td><a class="egrn-link egrn-link--pseudo egrn-link--middle" href="#">Образцы</a></td>
				</tr>
				<tr>
					<td colspan="2">
						<div class="egrn-h3"><span class="egrn-heading-smaller_line">Доставка экспресс-почтой по всей РФ</span></div>
					</td>
				</tr>
			</tbody>
		</table>
		<p><br><br><br><strong class="egrn-text-emphasis--no-bold">Выписка из ЕГРН об основных характеристиках объекта недвижимости с печатью</strong><br><span class="egrn-text-muted">Цена: 1900 ₽, готовность: 5 дней</span></p>
		<p><strong class="egrn-text-emphasis--no-bold">Выписка из ЕГРН о переходе прав с печатью</strong><br><span class="egrn-text-muted">Цена: 1900 ₽, готовность: 5 дней</span></p>
		<p><strong class="egrn-text-emphasis--no-bold">Справка о кадастровой стоимости с печатью</strong><br><span class="egrn-text-muted">Цена: 1900 ₽, готовность: 5 дней</span></p>
		<p><strong class="egrn-text-emphasis--no-bold">Срочная выписка из ЕГРН с печатью</strong><br><span class="egrn-text-muted">Цена: 2400 ₽, готовность: 1 день</span><br>&nbsp;</p>
		<h3>Полезная информация</h3>
		<p>С 1 января 2017&nbsp;ФГБУ «ФКП Росреестр»&nbsp;ведёт учёт объектов недвижимости в объединённой системе государственного кадастрового учёта&nbsp;и&nbsp;регистрации прав на недвижимое имущество - «ЕГРН» Единый государственный реестр недвижимости.</p>
		<p>Ранее&nbsp;в ведомстве возможно было заказать и получить выписку из ЕГРП, кадастровый&nbsp;паспорт на объект недвижимости или кадастровую выписку&nbsp;на земельный участок или здание.</p>
		<p><strong>Получить выписку&nbsp;из ЕГРН можно в двух форматах:</strong></p>
		<ul class="egrn-list">
			<li>Электронная&nbsp;онлайн выписка из ЕГРН заверенная ЭЦП Росреестра</li>
			<li>Бумажная выписка из ЕГРН с печатью</li>
		</ul>
		<p><strong>Выписка из ЕГРН бывает трех видов:</strong></p>
		<ul>
			<li>Выписка из ЕГРН об основных характеристиках объекта&nbsp;недвижимости</li>
			<li>Выписка из ЕГРН о переходе прав</li>
			<li>Справка о кадастровой стоимости</li>
		</ul>
		<p><strong>Заказать выписку из ЕГРН возможно исключительно на объект недвижимости, который уже стоит на кадастровом учёте и ему присвоен кадастровый номер.</strong></p>
		<p>Выписка из ЕГРН на квартиру;<br>Выписка из ЕГРН на нежилое помещение;<br>Выписка из ЕГРН на здание;<br>Выписка из ЕГРН на сооружение (инженерные сети, очистные сооружения, дороги);<br>Выписка из ЕГРН на земельный участок.</p>

	</div>
</div>
	
<?php include $baseUrl . '/partials/footer.php' ?>