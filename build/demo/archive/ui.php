<?php 
	$baseUrl = '..';
	$pageTitle = 'UI';
?>
<?php include $baseUrl . '/partials/header.php' ?>
		<link rel="stylesheet" href="../css/egrn-new.css?<?php include '../partials/cache-buster.txt' ?>">
		<style>
			body {
				background:#fff;
			}

			/* Это только для демонстрации, чтобы лучше смотрелось */
			.egrn-btn {
				margin: .8rem 0;
			}

			.demo-row {
				padding:20px 10px 0 10px;
				max-width:1200px;
				margin:0 auto;
				text-align:center;
			}

			.demo-title {
				font-size:28px;
				line-height:28px;
				font-family:'bebasneue-forced';
				font-weight:700;
				padding:0 5px 10px 5px;
			}

			.demo-subtitle {
				font-size:16px;
				line-height:16px;
				padding:0 5px 10px 5px;
			}

			.demo-item {
				display:inline-block;
				vertical-align:top;
				margin:0 5px 10px 5px;
				max-width:300px;
				text-align:left;
			}

			.demo-item--250 {
				max-width:none;
				width:250px;
			}

			.demo-item--300 {
				max-width:none;
				width:300px;
			}

			.demo-item--540 {
				max-width:none;
				width:540px;
			}
		</style>
		<div class="demo-row">
			<div class="demo-title">Кнопки</div>
			<div class="demo-subtitle">Обычный размер</div>
			<div class="demo-item">
				<button class="egrn-btn egrn-btn--bebas egrn-btn--green" type="button">
					<span class="egrn-btn__text egrn-btn__text--bebas">
						Active
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn egrn-btn--bebas egrn-btn--green" type="button" disabled>
					<span class="egrn-btn__text egrn-btn__text--bebas">
						Disabled
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn" type="button">
					<span class="egrn-btn__text">
						Населенные пункты, районы
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn" type="button" disabled>
					<span class="egrn-btn__text">
						Населенные пункты, районы
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn" type="button">
					<svg class="egrn-btn__icon"><use xlink:href="#egrn-svg_sprite-icon-mail"></use></svg>
					<span class="egrn-btn__text">
						Задать вопрос
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn" type="button" disabled>
					<svg class="egrn-btn__icon"><use xlink:href="#egrn-svg_sprite-icon-mail"></use></svg>
					<span class="egrn-btn__text">
						Задать вопрос
					</span>
				</button>
			</div>
			<div class="demo-subtitle">Уменьшенный</div>
			<div class="demo-item">
				<button class="egrn-btn egrn-btn--bebas egrn-btn--green egrn-btn--small" type="button">
					<span class="egrn-btn__text egrn-btn__text--bebas">
						Active
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn egrn-btn--bebas egrn-btn--green egrn-btn--small" type="button" disabled>
					<span class="egrn-btn__text egrn-btn__text--bebas">
						Disabled
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn egrn-btn--small" type="button">
					<span class="egrn-btn__text">
						Населенные пункты, районы
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn egrn-btn--small" type="button" disabled>
					<span class="egrn-btn__text">
						Населенные пункты, районы
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn egrn-btn--small" type="button">
					<svg class="egrn-btn__icon"><use xlink:href="#egrn-svg_sprite-icon-mail"></use></svg>
					<span class="egrn-btn__text">
						Задать вопрос
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn egrn-btn--small" type="button" disabled>
					<svg class="egrn-btn__icon"><use xlink:href="#egrn-svg_sprite-icon-mail"></use></svg>
					<span class="egrn-btn__text">
						Задать вопрос
					</span>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-btn egrn-btn--green egrn-btn--small" type="button">
					<span class="egrn-btn__text">
						Найти
					</span>
				</button>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				<button class="egrn-icon-btn" type="button">
					<svg class="egrn-icon-btn__icon egrn-icon-btn__icon--light"><use xlink:href="#egrn-svg_sprite-icon-mail"></use></svg>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-icon-btn" type="button">
					<svg class="egrn-icon-btn__icon egrn-icon-btn__icon--dark"><use xlink:href="#egrn-svg_sprite-icon-mail"></use></svg>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-icon-btn" type="button">
					<svg class="egrn-icon-btn__icon egrn-icon-btn__icon--dark"><use xlink:href="#egrn-svg_sprite-icon-plus2"></use></svg>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-icon-btn" type="button">
					<svg class="egrn-icon-btn__icon egrn-icon-btn__icon--zoom-out egrn-icon-btn__icon--red"><use xlink:href="#egrn-svg_sprite-icon-attention"></use></svg>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-icon-btn" type="button">
					<svg class="egrn-icon-btn__icon egrn-icon-btn__icon--zoom-out egrn-icon-btn__icon--green"><use xlink:href="#egrn-svg_sprite-icon-layers"></use></svg>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-icon-btn egrn-icon-btn--border egrn-icon-btn--border-light" type="button">
					<svg class="egrn-icon-btn__icon egrn-icon-btn__icon--zoom-out egrn-icon-btn__icon--light"><use xlink:href="#egrn-svg_sprite-icon-layers"></use></svg>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-icon-btn egrn-icon-btn--border egrn-icon-btn--border-dark" type="button">
					<svg class="egrn-icon-btn__icon egrn-icon-btn__icon--zoom-out egrn-icon-btn__icon--dark"><use xlink:href="#egrn-svg_sprite-icon-layers"></use></svg>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-icon-btn egrn-icon-btn--border egrn-icon-btn--border-red" type="button">
					<svg class="egrn-icon-btn__icon egrn-icon-btn__icon--zoom-out egrn-icon-btn__icon--red"><use xlink:href="#egrn-svg_sprite-icon-layers"></use></svg>
				</button>
			</div>
			<div class="demo-item">
				<button class="egrn-icon-btn egrn-icon-btn--border egrn-icon-btn--border-green" type="button">
					<svg class="egrn-icon-btn__icon egrn-icon-btn__icon--zoom-out egrn-icon-btn__icon--green"><use xlink:href="#egrn-svg_sprite-icon-layers"></use></svg>
				</button>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-title">Чекбоксы, радио</div>
			<div class="demo-item">
				<label class="egrn-checkbox">
					<input class="egrn-checkbox-input" type="checkbox" />
					<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
					<span class="egrn-checkbox-text">
						Чекбокс
					</span>
				</label>
			</div>
			<div class="demo-item demo-item--250">
				<label class="egrn-checkbox">
					<input class="egrn-checkbox-input" type="checkbox" checked />
					<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
					<span class="egrn-checkbox-text">
						Чекбокс c&nbsp;длинным текстом чекбокс c&nbsp;длинным текстом
					</span>
				</label>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				<label class="egrn-radio">
					<input class="egrn-radio-input" type="radio" name="radio1" />
					<span class="egrn-radio-check"></span>
					<span class="egrn-radio-text">
						Радио
					</span>
				</label>
			</div>
			<div class="demo-item demo-item--250">
				<label class="egrn-radio">
					<input class="egrn-radio-input" type="radio" name="radio1" checked />
					<span class="egrn-radio-check"></span>
					<span class="egrn-radio-text">
						Радио c&nbsp;длинным текстом радио c&nbsp;длинным текстом
					</span>
				</label>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-title">Селекты</div>
			<div class="demo-subtitle">Обычный размер</div>
			<div class="demo-item demo-item--250">
				<div class="egrn-select">
					<select class="egrn-select-default" id="select1">
						<option class="egrn-select-option" data-egrn-select-option="Участок">Участок</option>
						<option class="egrn-select-option" data-egrn-select-option="Дом">Дом</option>
						<option class="egrn-select-option" data-egrn-select-option="Квартира">Квартира</option>
					</select>
					<label class="egrn-select-trigger" for="select1">
						<span class="egrn-select-trigger-inner">
							<span class="egrn-select-trigger-text"></span>
						</span>
						<svg class="egrn-select-trigger-arrow"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</label>
				</div>
			</div>
			<div class="demo-item demo-item--250">
				<div class="egrn-select egrn-select--sortable">
					<select class="egrn-select-default" id="select2">
						<option class="egrn-select-option" data-egrn-select-sort="inc" data-egrn-select-option="По названию">По названию ↑</option>
						<option class="egrn-select-option" data-egrn-select-sort="dec" data-egrn-select-option="По названию">По названию ↓</option>
						<option class="egrn-select-option" data-egrn-select-sort="inc" data-egrn-select-option="По цене">По цене ↑</option>
						<option class="egrn-select-option" data-egrn-select-sort="dec" data-egrn-select-option="По цене">По цене ↓</option>
						<option class="egrn-select-option" data-egrn-select-sort="inc" data-egrn-select-option="По дате">По дате ↑</option>
						<option class="egrn-select-option" data-egrn-select-sort="dec" data-egrn-select-option="По дате">По дате ↓</option>
					</select>
					<label class="egrn-select-trigger" for="select1">
						<span class="egrn-select-trigger-inner">
							<span class="egrn-select-trigger-text"></span>
						</span>
						<svg class="egrn-select-trigger-arrow"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</label>
				</div>
			</div>
			<div class="demo-subtitle">Уменьшенный</div>
			<div class="demo-item demo-item--250">
				<div class="egrn-select">
					<select class="egrn-select-default" id="select3">
						<option class="egrn-select-option" data-egrn-select-option="Участок">Участок</option>
						<option class="egrn-select-option" data-egrn-select-option="Дом">Дом</option>
						<option class="egrn-select-option" data-egrn-select-option="Квартира">Квартира</option>
						<option class="egrn-select-option" data-egrn-select-option="Поле с длинным текстом с длинным текстом с длинным текстом">Поле с длинным текстом с длинным текстом с длинным текстом</option>
					</select>
					<label class="egrn-select-trigger egrn-select-trigger--small" for="select3">
						<span class="egrn-select-trigger-inner">
							<span class="egrn-select-trigger-text"></span>
						</span>
						<svg class="egrn-select-trigger-arrow"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</label>
				</div>
			</div>
			<div class="demo-item demo-item--250">
				<div class="egrn-select egrn-select--sortable">
					<select class="egrn-select-default" id="select4">
						<option class="egrn-select-option" data-egrn-select-sort="inc" data-egrn-select-option="По названию">По названию ↑</option>
						<option class="egrn-select-option" data-egrn-select-sort="dec" data-egrn-select-option="По названию">По названию ↓</option>
						<option class="egrn-select-option" data-egrn-select-sort="inc" data-egrn-select-option="По цене">По цене ↑</option>
						<option class="egrn-select-option" data-egrn-select-sort="dec" data-egrn-select-option="По цене">По цене ↓</option>
						<option class="egrn-select-option" data-egrn-select-sort="inc" data-egrn-select-option="По дате">По дате ↑</option>
						<option class="egrn-select-option" data-egrn-select-sort="dec" data-egrn-select-option="По дате">По дате ↓</option>
					</select>
					<label class="egrn-select-trigger egrn-select-trigger--small" for="select4">
						<span class="egrn-select-trigger-inner">
							<span class="egrn-select-trigger-text"></span>
						</span>
						<svg class="egrn-select-trigger-arrow"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</label>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-title">Поля ввода</div>
			<div class="demo-subtitle">Обычный размер</div>
			<div class="demo-item">
				<input class="egrn-input" type="text" placeholder="Однострочное поле" />
			</div>
			<div class="demo-item">
				<textarea class="egrn-input egrn-input--textarea" placeholder="Многострочное поле"></textarea>
			</div>
			<div class="demo-subtitle">Уменьшенный</div>
			<div class="demo-item">
				<input class="egrn-input egrn-input--small" type="text" placeholder="Однострочное поле" />
			</div>
			<div class="demo-item">
				<textarea class="egrn-input egrn-input--textarea egrn-input--small" placeholder="Многострочное поле"></textarea>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-title">Тултипы</div>
			<div class="demo-subtitle">По клику</div>
			<div class="demo-item">
				<a class="egrn-link--pseudo egrn-drop-trigger" data-egrn-drop-id="tooltip1" data-egrn-drop-position="left middle" data-egrn-drop-theme="egrn-drop-theme-tooltips" href="javascript:void(0);">
					left middle
				</a>
			</div>
			<div class="demo-item">
				<a class="egrn-link--pseudo egrn-drop-trigger" data-egrn-drop-id="tooltip1" data-egrn-drop-position="top center" data-egrn-drop-theme="egrn-drop-theme-tooltips" href="javascript:void(0);">
					top center
				</a>
			</div>
			<div class="demo-item">
				<a class="egrn-link--pseudo egrn-drop-trigger" data-egrn-drop-id="tooltip1" data-egrn-drop-position="bottom center" data-egrn-drop-theme="egrn-drop-theme-tooltips" href="javascript:void(0);">
					bottom center
				</a>
			</div>
			<div class="demo-item">
				<a class="egrn-link--pseudo egrn-drop-trigger" data-egrn-drop-id="tooltip1" data-egrn-drop-position="right middle" data-egrn-drop-theme="egrn-drop-theme-tooltips" href="javascript:void(0);">
					right middle
				</a>
			</div>
			<div class="demo-subtitle">По ховеру</div>
			<div class="demo-item">
				<a class="egrn-link--pseudo egrn-drop-trigger" data-egrn-drop-id="tooltip1" data-egrn-drop-position="left middle" data-egrn-drop-theme="egrn-drop-theme-tooltips" data-egrn-drop-open-on="hover" href="javascript:void(0);">
					left middle
				</a>
			</div>
			<div class="demo-item">
				<a class="egrn-link--pseudo egrn-drop-trigger" data-egrn-drop-id="tooltip1" data-egrn-drop-position="top center" data-egrn-drop-theme="egrn-drop-theme-tooltips" data-egrn-drop-open-on="hover" href="javascript:void(0);">
					top center
				</a>
			</div>
			<div class="demo-item">
				<a class="egrn-link--pseudo egrn-drop-trigger" data-egrn-drop-id="tooltip1" data-egrn-drop-position="bottom center" data-egrn-drop-theme="egrn-drop-theme-tooltips" data-egrn-drop-open-on="hover" href="javascript:void(0);">
					bottom center
				</a>
			</div>
			<div class="demo-item">
				<a class="egrn-link--pseudo egrn-drop-trigger" data-egrn-drop-id="tooltip1" data-egrn-drop-position="right middle" data-egrn-drop-theme="egrn-drop-theme-tooltips" data-egrn-drop-open-on="hover" href="javascript:void(0);">
					right middle
				</a>
			</div>
			<div class="demo-subtitle">Всегда открыт</div>
			<div class="demo-item">
				<a class="egrn-link--pseudo egrn-drop-trigger" data-egrn-drop-id="tooltip1" data-egrn-drop-position="bottom center" data-egrn-drop-theme="egrn-drop-theme-tooltips" data-egrn-drop-open-on="always" href="javascript:void(0);">
					bottom center
				</a>
			</div>
			<br /><br /><br /><br /><br /><br /><br /><br />
			<div class="egrn-drop-hidden egrn-drop-popup" data-egrn-drop-id="tooltip1">
				<div class="egrn-tooltip">
					<div class="egrn-tooltip-text">
						Межевание&nbsp;- очень важная услуга, просто потому что вот.
					</div>
					<ul class="egrn-tooltip-links">
						<li class="egrn-tooltip-links-item">
							<a class="egrn-tooltip-link egrn-tooltip-link--underline" href="javascript:void(0);">Образец</a>
						</li>
						<li class="egrn-tooltip-links-item">
							<a class="egrn-tooltip-link" href="javascript:void(0);">Подробнее</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-title">Сворачиваемые области</div>
			<div class="demo-subtitle">Темные триггеры</div>
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--dark" data-toggle="collapse" data-target="#collapse1" role="button" aria-expanded="true" aria-controls="collapse1">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--head egrn-collapse-trigger-text__span--white">
								ВЫПИСКИ
							</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--white"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse" id="collapse1">
						<div class="egrn-collapse-content-inner">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--dark" data-toggle="collapse" data-target="#collapse2" role="button" aria-expanded="true" aria-controls="collapse2">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--white">№2728 от 22.11.17</span>
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--small egrn-collapse-trigger-text__span--grey">(ул. Поречная 12: Бум. выписка ЕГРН)</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--white"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse" id="collapse2">
						<div class="egrn-collapse-content-inner">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--dark" data-toggle="collapse" data-target="#collapse3" role="button" aria-expanded="true" aria-controls="collapse3">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--white">№2728 от 22.11.17</span>
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--small egrn-collapse-trigger-text__span--grey">(ул. Поречная 12: Бум. выписка ЕГРН, ул. Поречная 12: Бум. выписка ЕГРН)</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--white"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse" id="collapse3">
						<div class="egrn-collapse-content-inner">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-subtitle">Светлые триггеры</div>
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--light" data-toggle="collapse" data-target="#collapse4" role="button" aria-expanded="true" aria-controls="collapse4">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--head egrn-collapse-trigger-text__span--dark">
								ВЫПИСКИ
							</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--dark"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse" id="collapse4">
						<div class="egrn-collapse-content-inner">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--light" data-toggle="collapse" data-target="#collapse5" role="button" aria-expanded="true" aria-controls="collapse5">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--dark">№2728 от 22.11.17</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--dark"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse" id="collapse5">
						<div class="egrn-collapse-content-inner">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--light" data-toggle="collapse" data-target="#collapse6" role="button" aria-expanded="true" aria-controls="collapse6">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--dark">№2728 от 22.11.17</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--dark"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse" id="collapse6">
						<div class="egrn-collapse-content-inner">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-subtitle">Заблокированные</div>
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--dark egrn-collapse-trigger--inactive" data-toggle="collapse" data-target="#collapse7" role="button" aria-expanded="true" aria-controls="collapse7">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--white">№2728 от 22.11.17</span>
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--small egrn-collapse-trigger-text__span--grey">(ул. Поречная 12: Бум. выписка ЕГРН)</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--white"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse" id="collapse7">
						<div class="egrn-collapse-content-inner">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--light egrn-collapse-trigger--inactive" data-toggle="collapse" data-target="#collapse8" role="button" aria-expanded="true" aria-controls="collapse8">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--dark">№2728 от 22.11.17</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--dark"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse" id="collapse8">
						<div class="egrn-collapse-content-inner">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-subtitle">Раскрытые по дефолту</div>
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--dark" data-toggle="collapse" data-target="#collapse9" role="button" aria-expanded="true" aria-controls="collapse9">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--white">№2728 от 22.11.17</span>
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--small egrn-collapse-trigger-text__span--grey">(ул. Поречная 12: Бум. выписка ЕГРН)</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--white"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse show" id="collapse9">
						<div class="egrn-collapse-content-inner">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-subtitle">Увеличенные отступы внутри раскрываемого контента</div>
			<div class="demo-item demo-item--540">
				<div class="egrn-collapse">
					<div class="egrn-collapse-trigger egrn-collapse-trigger--dark" data-toggle="collapse" data-target="#collapse10" role="button" aria-expanded="true" aria-controls="collapse10">
						<div class="egrn-collapse-trigger-text">
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--white">№2728 от 22.11.17</span>
							<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--small egrn-collapse-trigger-text__span--grey">(ул. Поречная 12: Бум. выписка ЕГРН)</span>
						</div>
						<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--white"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
					</div>
					<div class="egrn-collapse-content collapse" id="collapse10">
						<div class="egrn-collapse-content-inner egrn-collapse-content-inner--pad-30">
							<div style="border:#d2d7dc solid 1px;padding:10px;">
								Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-title">Сворачиваемые блоки</div>
			<div class="demo-subtitle">Обычные размере заголовков</div>
			<div class="demo-item demo-item--300">
				<div class="egrn-collapsable">
					<div class="egrn-collapsable-trigger" data-toggle="collapse" data-target="#collapsable1" role="button" aria-expanded="false" aria-controls="collapsable1">
						<div class="egrn-collapsable-trigger-text">
							Поиск недвижимости
						</div>
						<div class="egrn-collapsable-trigger-arrow">
							<svg class="egrn-collapsable-trigger-icon"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapsable"></use></svg>
						</div>
					</div>
					<div class="egrn-collapsable-content collapse" id="collapsable1">
						<div class="egrn-collapsable-content-inner">
							<div class="egrn-collapsable-content-title">
								обл. Московская, р-н Талдомский, СНТ "Дружба" южнее д. Пановка, уч 743
							</div>
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-subtitle">Уменьшенные размере заголовков</div>
			<div class="demo-item demo-item--300">
				<div class="egrn-collapsable egrn-collapsable--size2">
					<div class="egrn-collapsable-trigger" data-toggle="collapse" data-target="#collapsable2" role="button" aria-expanded="false" aria-controls="collapsable2">
						<div class="egrn-collapsable-trigger-text egrn-collapsable-trigger-text--size2">
							Поиск недвижимости
						</div>
						<div class="egrn-collapsable-trigger-arrow">
							<svg class="egrn-collapsable-trigger-icon"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapsable"></use></svg>
						</div>
					</div>
					<div class="egrn-collapsable-content collapse" id="collapsable2">
						<div class="egrn-collapsable-content-inner">
							<div class="egrn-collapsable-content-title">
								обл. Московская, р-н Талдомский, СНТ "Дружба" южнее д. Пановка, уч 743
							</div>
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item demo-item--300">
				<div class="egrn-collapsable egrn-collapsable--size3">
					<div class="egrn-collapsable-trigger" data-toggle="collapse" data-target="#collapsable3" role="button" aria-expanded="false" aria-controls="collapsable3">
						<div class="egrn-collapsable-trigger-text egrn-collapsable-trigger-text--size3">
							Поиск недвижимости
						</div>
						<div class="egrn-collapsable-trigger-arrow">
							<svg class="egrn-collapsable-trigger-icon"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapsable"></use></svg>
						</div>
					</div>
					<div class="egrn-collapsable-content collapse" id="collapsable3">
						<div class="egrn-collapsable-content-inner">
							<div class="egrn-collapsable-content-title">
								обл. Московская, р-н Талдомский, СНТ "Дружба" южнее д. Пановка, уч 743
							</div>
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-subtitle">Уменьшенные "поля" слева и справа</div>
			<div class="demo-item demo-item--300">
				<div class="egrn-collapsable">
					<div class="egrn-collapsable-trigger egrn-collapsable-trigger--pad2" data-toggle="collapse" data-target="#collapsable4" role="button" aria-expanded="false" aria-controls="collapsable4">
						<div class="egrn-collapsable-trigger-text">
							Поиск недвижимости
						</div>
						<div class="egrn-collapsable-trigger-arrow">
							<svg class="egrn-collapsable-trigger-icon"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapsable"></use></svg>
						</div>
					</div>
					<div class="egrn-collapsable-content collapse" id="collapsable4">
						<div class="egrn-collapsable-content-inner egrn-collapsable-content-inner--pad2">
							<div class="egrn-collapsable-content-title">
								обл. Московская, р-н Талдомский, СНТ "Дружба" южнее д. Пановка, уч 743
							</div>
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-subtitle">Блок без заголовка для триггера</div>
			<div class="demo-item demo-item--300">
				<div class="egrn-collapsable">
					<div class="egrn-collapsable-trigger" data-toggle="collapse" data-target="#collapsable5" role="button" aria-expanded="false" aria-controls="collapsable5">
						<div class="egrn-collapsable-trigger-text"></div>
						<div class="egrn-collapsable-trigger-arrow">
							<svg class="egrn-collapsable-trigger-icon"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapsable"></use></svg>
						</div>
					</div>
					<div class="egrn-collapsable-content collapse" id="collapsable5">
						<div class="egrn-collapsable-content-inner">
							<div class="egrn-collapsable-content-title">
								обл. Московская, р-н Талдомский, СНТ "Дружба" южнее д. Пановка, уч 743
							</div>
							Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>



		<!-- jQuery -->
		<script src="../js/jquery-3.3.1.min.js"></script>

		<!-- Сворачиваемые блоки/области на bootstrap -->
		<script src="../js/util.js"></script>
		<script src="../js/collapse.js"></script>

		<!-- Кастомные селекты -->
		<!-- <script src="../js/customselect.js"></script> -->

		<!-- Тултипы/попапы на tether -->
		<script src="../js/tether.js"></script>
		<script src="../js/drop.js"></script>

		<!-- Скрипты -->
		<script src="../js/scripts.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
<?php include $baseUrl . '/partials/footer.php' ?>