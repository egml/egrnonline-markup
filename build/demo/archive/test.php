<?php
	$baseUrl = '..';
	$pageTitle = 'Тестовая страница';
	// $template = 'simple';
?>
<?php include $baseUrl . '/partials/header.php' ?>

	<style>
		@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

		.wrapper {
			border: thin solid darkgrey;
		}
		.wrapper::after, .wrapper::before {
			content: '';
			display: table;
		}
	</style>

	<div class="content">
		<h1>Тестовая страница</h1>
		
		<div class="items">

			<?php

				$sizes = [ 'size-1', 'size-2', 'size-3' ];
				$dirs = [ 'row', 'column' ];
				$types = [ 'margin', 'padding' ];
				$combines = [ 'combine', 'combine-alt' ];

				$combinations = [];

				
				function cbn(&$cbn, $set1, $set2) {
					$cbn = [];
					foreach ($set1 as $item1) {
						foreach ($set2 as $item2) {
							$cbn[] = [ $item1, $item2 ];
						}
					}
					return $cbn;
				}

				cbn($combinations, $dirs, $types);


				// _cbn($combinations, [ $sizes, $dirs ]);

				// function _cbn(&$cbn, $set) {
				// 	foreach ($set as $item) {
				// 		$cbn[] = $item;
				// 	}
				// }

				// shuffle([[1, 2, 3], [a, b]]);



				// for ($i=0; $i < count($sizes); $i++) { 
				// 	$combinations[$i] = [ $sizes[$i] ];

				// 	for ($j=0; $j < count($dirs); $j++) { 
				// 		$combinations[$i][$j] = [ $sizes[$i], $dirs[$j] ];

				// 		for ($k=0; $k < count($types); $k++) { 
				// 			$combinations[$i][$j][$k] = [ $sizes[$i], $dirs[$j], $types[$k] ];
				// 		}
				// 	}

				// 	// for ($j=0; $j < count($combines); $j++) { 
				// 	// 	$combinations[$i][$j] = [ $sizes[$i], $dirs[$j] ];
				// 	// 	$combinations[] = $size . ' ' . $combine;
				// 	// }
				// }

				echo '<pre>';
				print_r($combinations);
				echo '</pre>';
				exit;

				// for ($i = 0; $i < count($mods); $i++) { 
					
				// }
				// function asd($a) {
				// 	foreach ($mods as $mod) {
				// 		echo $mod . '_' . 
				// 	}
				// }

				// foreach ($mods as $mod) {
				// 	foreach ($mods as $mod) {
				// 		echo $mod . '_' . 
				// 	}
				// }
			?>


			<div class="item" style="width:30%; white-space:normal;">
				<p><strong>.egrn-section<br> .egrn-section--padding</strong></p>
				<div class="wrapper">
					<div class="egrn-section egrn-section--padding">
						Lorem, ipsum dolor sit amet consectetur adipisicing elit.
					</div>
				</div>
			</div>
		</div>


		<?php /* ?>
		<style>
			#div {
				width: 10rem;
				height: 10rem;
				background: red;
			}
		</style>

		<div id="div"></div>

		<script>
			var div = document.getElementById('div');
			var styles = window.getComputedStyle(div);
			document.write(styles.getPropertyValue('width'));
		</script>
		<?php */ ?>
		
	</div>

<?php include $baseUrl . '/partials/footer.php' ?>