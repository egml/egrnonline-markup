<?php
	$baseUrl = '..';
	$pageTitle = 'Таблицы';
?>
<?php include $baseUrl . '/partials/header.php' ?>

<?php /* ?>
<?php */ ?>
<?php /* ?>
<!-- Highlight.js -->
<style>
	@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

<!-- Выделение кода при клике на блок с кодом -->
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var codeBlocks = document.querySelectorAll('.code-block');
		for (var i = 0; i < codeBlocks.length; i++) {
			codeBlocks[i].addEventListener('click', function() {
				var selection = window.getSelection();
				var range = document.createRange();
				range.selectNodeContents(this);
				selection.removeAllRanges();
				selection.addRange(range);
			});
		}
	});
</script>
<?php /**/ ?>

<style>
	@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

	body {
		/* background: #f7f8f9; */
		background: #fff;
	}
</style>

<div class="content">
	<h1><?= $pageTitle ?></h1>

	<table class="egrn-table egrn-table--first-col-bold egrn-table--margin-30" cellspacing="0">
		<tr>
			<td>Тип:</td>
			<td>Земельный участок</td>
		</tr>
		<tr>
			<td>Межевание:</td>
			<td>Погрешность определения границ</td>
		</tr>
		<tr>
			<td>Кад. квартал:</td>
			<td>77:06:0001002:119</td>
		</tr>
		<tr>
			<td>Статус:</td>
			<td>Ранее учтенный</td>
		</tr>
	</table>

	<table class="egrn-table egrn-table--first-col-muted egrn-table--bigger-text egrn-table--margin-30" cellspacing="0">
		<tr>
			<td>Тип:</td>
			<td>Земельный участок</td>
		</tr>
		<tr>
			<td>Межевание:</td>
			<td>Погрешность определения границ</td>
		</tr>
		<tr>
			<td>Кад. квартал:</td>
			<td>77:06:0001002:119</td>
		</tr>
		<tr>
			<td>Статус:</td>
			<td>Ранее учтенный</td>
		</tr>
	</table>

	<div class="egrn-table egrn-table--first-col-bold">
		<div class="egrn-table-row">
			<div class="egrn-table-cell">Тип:</div>
			<div class="egrn-table-cell">Земельный участок</div>
		</div>
		<div class="egrn-table-row">
			<div class="egrn-table-cell">Межевание:</div>
			<div class="egrn-table-cell">Погрешность определения границ</div>
		</div>
		<div class="egrn-table-row">
			<div class="egrn-table-cell">Кад. квартал:</div>
			<div class="egrn-table-cell">77:06:0001002:119</div>
		</div>
		<div class="egrn-table-row">
			<div class="egrn-table-cell">Статус:</div>
			<div class="egrn-table-cell">Ранее учтенный</div>
		</div>
	</div>

	<div class="egrn-table egrn-table--bigger-text egrn-table--first-col-muted egrn-table--margin-30">
		<div class="egrn-table-row">
			<div class="egrn-table-cell">Тип:</div>
			<div class="egrn-table-cell">Земельный участок</div>
		</div>
		<div class="egrn-table-row">
			<div class="egrn-table-cell">Межевание:</div>
			<div class="egrn-table-cell">Погрешность определения границ</div>
		</div>
		<div class="egrn-table-row">
			<div class="egrn-table-cell">Кад. квартал:</div>
			<div class="egrn-table-cell">77:06:0001002:119</div>
		</div>
		<div class="egrn-table-row">
			<div class="egrn-table-cell">Статус:</div>
			<div class="egrn-table-cell">Ранее учтенный</div>
		</div>
	</div>

	<?php /* ?>

	<div class="usage"><span>Использование:</span></div>

	<div class="items">
		<div class="item">
			<button class="egrn-button">Обычная</button>
		</div>
		<!-- <div class="item">
			<a class="egrn-button" href="#">Обычная</a>
		</div>
		<div class="item">
			<span class="egrn-button">Обычная</span>
		</div> -->
	</div>

	<?php */ ?>

<?php /* ?>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--tight egrn-button--bigger-text">
	Узкая с увеличенным текстом
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>


<?php
	$code = <<<CODE
document.addEventListener('egrn.appready', function() {
	var area = document.querySelector('.egrn-collapsible_area');
	new EGRN.CollapsibleArea(area);
});
CODE;
	echo '<pre><code class="code-block code-block--label js">' . htmlspecialchars($code) . '</code></pre>';
?>

<?php */ ?>

</div>

<?php /* ?>
<script src="<?= $assetsUrl ?>/js/egrn-demo_name.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
<?php */ ?>
	
<?php include $baseUrl . '/partials/footer.php' ?>