<?php
	$baseUrl = '../..';
	$pageTitle = 'Поля ввода';
?>
<?php include $baseUrl . '/partials/header.php' ?>

	<?php /* ?>
	<!-- Highlight.js -->
	<style>
		@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
	</style>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
	<script>hljs.initHighlightingOnLoad();</script>

	<!-- Выделение кода при клике на блок с кодом -->
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			var codeBlocks = document.querySelectorAll('.code-block');
			for (var i = 0; i < codeBlocks.length; i++) {
				codeBlocks[i].addEventListener('click', function() {
					var selection = window.getSelection();
					var range = document.createRange();
					range.selectNodeContents(this);
					selection.removeAllRanges();
					selection.addRange(range);
				});
			}
		});
	</script>
	<?php */ ?>

	<style>
		@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

		body {
			background: #f7f8f9;
		}
	</style>

	<div class="content">

		<h1>Поля ввода</h1>

		<div class="items">
			<div class="item">
				<input class="egrn-text_input" type="text" placeholder="Номер карты">
			</div>
			<div class="item">
				<div class="egrn-text_input" placeholder="Номер карты">Стиль поля на теге DIV</div>
			</div>
			<div class="item">
				<input type="text" placeholder="Поле без стилей">
			</div>
		</div>

		<div class="margin"></div>
		Комбинированное поле:
		<div class="margin"></div>

		<div class="items">
			<div class="item">
				<div class="egrn-combined_text_input">
					<label class="egrn-combined_text_input-aligner" for="input1">
						<div class="egrn-combined_text_input-aligner-control">
							<input class="egrn-combined_text_input-control" type="text" placeholder="Номер карты" id="input1">
						</div>
						<div class="egrn-combined_text_input-aligner-suffix">
							<svg class="egrn-combined_text_input-appendix-icon">
								<use xlink:href="#egrn-svg_sprite-icon-circle-question"/>
							</svg>
						</div>
					</label>
				</div>
			</div>

			<div class="item">
				<div class="egrn-combined_text_input">
					<label class="egrn-combined_text_input-aligner" for="input2">
						<div class="egrn-combined_text_input-aligner-control">
							<input class="egrn-combined_text_input-control" type="text" placeholder="Площадь" id="input2">
						</div>
						<div class="egrn-combined_text_input-aligner-suffix">
							<span class="egrn-combined_text_input-appendix-text">
								м<sup>2</sup>
							</span>
						</div>
					</label>
				</div>
			</div>
		</div>

		<div class="margin"></div>
		Узкое:
		<div class="margin"></div>

		<div class="items">
			<div class="item">
				<input class="egrn-text_input egrn-text_input--tight" type="text" placeholder="Номер карты">
			</div>
			<div class="item">
				<div class="egrn-text_input egrn-text_input--tight" placeholder="Номер карты">Стиль поля на теге DIV</div>
			</div>
		</div>

		<div class="margin"></div>

		<div class="items">
			<div class="item">
				<div class="egrn-combined_text_input egrn-combined_text_input--tight">
					<label class="egrn-combined_text_input-aligner" for="input1">
						<div class="egrn-combined_text_input-aligner-control">
							<input class="egrn-combined_text_input-control" type="text" placeholder="Номер карты" id="input1">
						</div>
						<div class="egrn-combined_text_input-aligner-suffix">
							<svg class="egrn-combined_text_input-appendix-icon">
								<use xlink:href="#egrn-svg_sprite-icon-circle-question"/>
							</svg>
						</div>
					</label>
				</div>
			</div>

			<div class="item">
				<div class="egrn-combined_text_input egrn-combined_text_input--tight">
					<label class="egrn-combined_text_input-aligner" for="input2">
						<div class="egrn-combined_text_input-aligner-control">
							<input class="egrn-combined_text_input-control" type="text" placeholder="Площадь" id="input2">
						</div>
						<div class="egrn-combined_text_input-aligner-suffix">
							<span class="egrn-combined_text_input-appendix-text">
								м<sup>2</sup>
							</span>
						</div>
					</label>
				</div>
			</div>
		</div>

		<div class="margin"></div>
		С увеличенным текстом:
		<div class="margin"></div>

		<div class="items">
			<div class="item">
				<input class="egrn-text_input egrn-text_input--tight egrn-text_input--bigger-text" type="text" placeholder="Номер карты">
			</div>
		</div>

		<div class="margin"></div>

		<?php /* ?>
		<div class="items">
			<div class="item">

				<div class="egrn-dropdown_select">
					<button class="egrn-dropdown_select-select" type="button">
						<span class="egrn-dropdown_select-select-aligner">
							<span class="egrn-dropdown_select-select-aligner-choice">
								<span class="egrn-dropdown_select-select-choice egrn-dropdown_select-select-choice--placeholder">Сортировка</span>
							</span>
							<span class="egrn-dropdown_select-select-aligner-indicator">
								<svg class="egrn-dropdown_select-select-indicator-icon">
									<use xlink:href="#egrn-svg_sprite-icon-chevron"/>
								</svg>
							</span>
						</span>
					</button>
					<div class="egrn-dropdown_select-options">
						<div class="egrn-collapsible-content">
							<div class="egrn-dropdown_select-options-menu">
								<button class="egrn-dropdown_select-options-menu-item" type="button">
									<div class="egrn-dropdown_select-options-menu-item-name">По возрастанию цены</div>
								</button>
								<button class="egrn-dropdown_select-options-menu-item" type="button">
									<div class="egrn-dropdown_select-options-menu-item-name">По убыванию цены</div>
								</button>
							</div>
						</div>
					</div>
					<input class="egrn-dropdown_select-control" type="hidden">
				</div>

			</div>
		</div>

		<div class="margin"></div>
		<?php */ ?>

		<?php /* ?>
		<h2>Светлая</h2>

		<p>
			<button class="egrn-button">Обычная</button type="button">
		</p>

		<div class="items">
			<div class="item">
				<button class="egrn-button">На теге BUTTON</button type="button">
			</div>
			<div class="item">
				<a class="egrn-button" href="#">На теге A</a>
			</div>
			<div class="item">
				<span class="egrn-button">На теге SPAN</span>
			</div>
			<div class="item">
				<div class="egrn-button">На теге DIV</div>
			</div>
		</div>

		<div class="usage">Использование:</div>

		<div class="items">
			<div class="item">
				<button class="egrn-button">Обычная</button type="button">
			</div>
		</div>

<?php
	$code = <<<CODE
<button class="egrn-button">Обычная</button type="button">
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>
		<?php */ ?>

	</div>

	<?php /* ?>
	<script src="<?= $assetsUrl ?>/js/egrn-demo_input.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
	<?php */ ?>

<?php include $baseUrl . '/partials/footer.php' ?>