<?php 
	$baseUrl = '../..';
	$pageTitle = 'Прокручиваемые области';
?>
<?php include $baseUrl . '/partials/header.php' ?>

	<!-- Highlight.js -->
	<style>
		/* @import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css'; */
		@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
	</style>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
	<script>hljs.initHighlightingOnLoad();</script>

	<!-- Выделение кода при клике на блок с кодом -->
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			var codeBlocks = document.querySelectorAll('.code-block');
			for (var i = 0; i < codeBlocks.length; i++) {
				codeBlocks[i].addEventListener('click', function() {
					var selection = window.getSelection();
					var range = document.createRange();
					range.selectNodeContents(this);
					selection.removeAllRanges();
					selection.addRange(range);
				});
			}
		});
	</script>

	<style>
		@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

		.egrn-scrollable {
			height: 20rem;
			/* outline: .1rem solid grey;
			outline-offset: .2rem; */
		}
		.img {
			/* display: block; */
			/* width: 100%; */
			/* width: 80rem;
			height: 80rem; */
			/* object-fit: fill; */
			/* transform: scaleX(2.5); */
			/* transform-origin: top left; */
		}
		.circle {
			background: skyblue;
			border-radius: 50%;
			width: 80rem;
			height: 80rem;
		}
		.bluerect {
			/* 
				Если ширина блока 100%, тогда работает verticalGutter. И только тогда.
			*/
			background: skyblue;
			border: thick dashed dodgerblue;
			border-radius: 2rem;
			<?php /* ?>
			width: 80rem;
			<?php */ ?>
			/* width: 100%; */
			height: 30rem;
			box-sizing: border-box;
		}
	</style>

	<div class="content">

		<h1>Прокручиваемые области</h1>

		<h2>По умолчанию</h2>

		<div id="scroll-1" class="egrn-scrollable">

			<?php /* ?>
			<div style="width:300px; height:300px; background:green;"></div>
			<img class="img" src="test.svg">
			<img class="img" src="test.jpg">
			<div class="circle"></div>
			<div class="bluerect"></div>
			<?php */ ?>

			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>

			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>

			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>
			
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>

			<?php /* ?>

			<?php */ ?>

		</div>

		<div class="margin"></div>
		<div class="margin"></div>
		<div class="margin"></div>

<?php
	$code = <<<CODE
<div class="egrn-scrollable">
	<p>
		Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
		Pariatur quasi dolores dignissimos atque, perspiciatis, 
		voluptate maiores quos est quisquam, numquam libero 
		doloremque tempore ex delectus eveniet voluptatem maxime? 
		Doloribus, atque.
	</p>
	...
</div>
CODE;
	echo '<pre><code class="code-block code-block--label html">' . htmlspecialchars($code) . '</code></pre>';
?>
<?php
	$code = <<<CODE
document.addEventListener('egrn.appready', function() {
	var scrollable = document.querySelector('.egrn-scrollable');
	new EGRN.Scrollable(scrollable);
});
CODE;
	echo '<pre><code class="code-block code-block--label js">' . htmlspecialchars($code) . '</code></pre>';
?>

		<h2>С тенюшкой</h2>

		<div id="scroll-2" class="egrn-scrollable egrn-scrollable--shadows">
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>

			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>

			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>
			
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>
		</div>

		<div class="margin"></div>
		<div class="margin"></div>
		<div class="margin"></div>

<?php
	$code = <<<CODE
<div class="egrn-scrollable">
	<p>
		Lorem ipsum, dolor sit amet consectetur adipisicing elit. 
		Pariatur quasi dolores dignissimos atque, perspiciatis, 
		voluptate maiores quos est quisquam, numquam libero 
		doloremque tempore ex delectus eveniet voluptatem maxime? 
		Doloribus, atque.
	</p>
	...
</div>
CODE;
	echo '<pre><code class="code-block code-block--label html">' . htmlspecialchars($code) . '</code></pre>';
?>
<?php
	$code = <<<CODE
document.addEventListener('egrn.appready', function() {
	var scrollable = document.querySelector('.egrn-scrollable');
	new EGRN.Scrollable(scrollable, { shadows: true });
});
CODE;
	echo '<pre><code class="code-block code-block--label js">' . htmlspecialchars($code) . '</code></pre>';
?>

	</div>

	<?php /* ?>
	<script src="<?= $assetsUrl ?>/js/egrn-demo_scrollbar.js?<?php include '../partials/cache-buster.txt' ?>"></script>
	<?php */ ?>

<?php include $baseUrl . '/partials/footer.php' ?>