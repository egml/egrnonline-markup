<?php
	$baseUrl = '../..';
	$pageTitle = 'Меню';
?>
<?php include $baseUrl . '/partials/header.php' ?>

	<?php /* ?>
	<!-- Highlight.js -->
	<style>
		@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
	</style>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
	<script>hljs.initHighlightingOnLoad();</script>

	<!-- Выделение кода при клике на блок с кодом -->
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			var codeBlocks = document.querySelectorAll('.code-block');
			for (var i = 0; i < codeBlocks.length; i++) {
				codeBlocks[i].addEventListener('click', function() {
					var selection = window.getSelection();
					var range = document.createRange();
					range.selectNodeContents(this);
					selection.removeAllRanges();
					selection.addRange(range);
				});
			}
		});
	</script>
	<?php */ ?>

	<style>
		@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

		body {
			background: #f7f8f9;
		}
	</style>

	<div class="content">

		<h1>Меню</h1>

		<div class="items">
			<div class="item">

				<div class="egrn-menu egrn-menu--popover">
					<button class="egrn-menu-item">
						<div class="egrn-menu-item-prefix">
							<svg class="egrn-menu-item-prefix-icon">
								<use xlink:href="#egrn-svg_sprite-icon-circle-plus">
							</svg>
						</div>
						<div class="egrn-menu-item-name">Пункт меню</div>
					</button>
					<button class="egrn-menu-item">
						<div class="egrn-menu-item-prefix">
							<svg class="egrn-menu-item-prefix-icon">
								<use xlink:href="#egrn-svg_sprite-icon-star">
							</svg>
						</div>
						<div class="egrn-menu-item-name">Пункт меню</div>
					</button>
					<button class="egrn-menu-item">
						<div class="egrn-menu-item-prefix"></div>
						<div class="egrn-menu-item-name">Пункт меню</div>
					</button>
					<button class="egrn-menu-item">
						<div class="egrn-menu-item-prefix">
							<svg class="egrn-menu-item-prefix-icon">
								<use xlink:href="#egrn-svg_sprite-icon-profile">
							</svg>
						</div>
						<div class="egrn-menu-item-name">Пункт меню</div>
					</button>
				</div>

				<?php /* ?>
				<div class="egrn-menu">
					<div class="egrn-menu-itemgroup">
						<div class="egrn-menu-item">
							<div class="egrn-menu-item-prefix">
								<svg class="egrn-menu-item-prefix-icon">
									<use xlink:href="#egrn-svg_sprite-icon-circle-plus">
								</svg>
							</div>
							<div class="egrn-menu-item-name">Пункт меню</div>
						</div>
						<div class="egrn-menu-item">
							<div class="egrn-menu-item-prefix">
								<svg class="egrn-menu-item-prefix-icon">
									<use xlink:href="#egrn-svg_sprite-icon-star">
								</svg>
							</div>
							<div class="egrn-menu-item-name">Пункт меню</div>
						</div>
					</div>
					<div class="egrn-menu-itemgroup">
						<div class="egrn-menu-item">
							<div class="egrn-menu-item-prefix">
								<svg class="egrn-menu-item-prefix-icon">
									<use xlink:href="#egrn-svg_sprite-icon-profile">
								</svg>
							</div>
							<div class="egrn-menu-item-name">Пункт меню</div>
						</div>
						<div class="egrn-menu-item">
							<div class="egrn-menu-item-prefix">
								<svg class="egrn-menu-item-prefix-icon">
									<use xlink:href="#egrn-svg_sprite-icon-wallet">
								</svg>
							</div>
							<div class="egrn-menu-item-name">Пункт меню</div>
						</div>
					</div>
					<hr class="egrn-menu-separator">
				</div>
				<?php */ ?>

			</div>
		</div>













		<?php /* ?>
		<h2>Светлая</h2>

		<p>
			<button class="egrn-button">Обычная</button>
		</p>

		<div class="items">
			<div class="item">
				<button class="egrn-button">На теге BUTTON</button>
			</div>
			<div class="item">
				<a class="egrn-button" href="#">На теге A</a>
			</div>
			<div class="item">
				<span class="egrn-button">На теге SPAN</span>
			</div>
			<div class="item">
				<div class="egrn-button">На теге DIV</div>
			</div>
		</div>

		<div class="usage">Использование:</div>

		<div class="items">
			<div class="item">
				<button class="egrn-button">Обычная</button>
			</div>
		</div>

<?php
	$code = <<<CODE
<button class="egrn-button">Обычная</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>
		<?php */ ?>

	</div>

	<script src="<?= $assetsUrl ?>/js/egrn-demo_menu.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>

<?php include $baseUrl . '/partials/footer.php' ?>