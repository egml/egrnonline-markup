<?php
	$baseUrl = '..';
	$pageTitle = 'Ярлыки';
?>
<?php include $baseUrl . '/partials/header.php' ?>

	<!-- Highlight.js -->
	<style>
		/* @import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css'; */
		@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
	</style>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
	<script>hljs.initHighlightingOnLoad();</script>

	<!-- Выделение кода при клике на блок с кодом -->
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			var codeBlocks = document.querySelectorAll('.code-block');
			for (var i = 0; i < codeBlocks.length; i++) {
				codeBlocks[i].addEventListener('click', function() {
					var selection = window.getSelection();
					var range = document.createRange();
					range.selectNodeContents(this);
					selection.removeAllRanges();
					selection.addRange(range);
				});
			}
		});
	</script>

	<style>
		@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

		body {
			background: #f7f8f9;
		}
		.block {
			display: inline-block;
			width: 20rem;
			height: 10rem;
			border: thin dashed silver;
		}
		.block-content {
			line-height: 10rem;
			padding: 1rem;
		}
	</style>

	<div class="content">

		<h1>Ярлыки</h1>

		<div class="block egrn-tag-parent">
			<span class="block-content">На блочном элементе</span>
			<span class="egrn-tag">5</span>
		</div>

		<span class="gap"></span>

		<div class="block egrn-tag-parent">
			<span class="block-content">На блочном элементе</span>
			<span class="egrn-tag egrn-tag--red">Текст</span>
		</div>

		<br>
		<br>
		<br>

		На кнопке: &nbsp; &nbsp;
		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button">Партнерская программа</button>
			<span class="egrn-tag">5</span>
		</span>


		<br>
		<br>
		<br>

		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
			<span class="egrn-tag egrn-tag--red egrn-tag--top-left">0</span>
		</span>

		&nbsp; &nbsp; 

		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
			<span class="egrn-tag egrn-tag--red egrn-tag--bottom-left">0</span>
		</span>

		&nbsp; &nbsp; 

		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
			<span class="egrn-tag egrn-tag--red egrn-tag--top-right">0</span>
		</span>

		&nbsp; &nbsp; 

		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
			<span class="egrn-tag egrn-tag--red egrn-tag--bottom-right">0</span>
		</span>

		<div class="usage"><span>Использование:</span></div>

		<div class="block egrn-tag-parent">
			<span class="block-content">На блочном элементе</span>
			<span class="egrn-tag">5</span>
		</div>

<?php
	$code = <<<CODE
<div class="egrn-tag-parent">
	На блочном элементе
	<span class="egrn-tag">5</span>
</div>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<div class="block egrn-tag-parent">
			<span class="block-content">На блочном элементе</span>
			<span class="egrn-tag egrn-tag--red">Текст</span>
		</div>

<?php
	$code = <<<CODE
<div class="egrn-tag-parent">
	На блочном элементе
	<span class="egrn-tag egrn-tag--red">Текст</span>
</div>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button">Партнерская программа</button>
			<span class="egrn-tag">5</span>
		</span>

<?php
	$code = <<<CODE
<span class="egrn-tag-parent egrn-tag-parent--inline">
	<button class="egrn-button">Партнерская программа</button>
	<span class="egrn-tag">5</span>
</span>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button egrn-button--icon">
				<svg class="egrn-button-icon">
					<use xlink:href="#egrn-svg_sprite-icon-cart"/>
				</svg>
			</button>
			<span class="egrn-tag egrn-tag--red egrn-tag--top-left">0</span>
		</span>

<?php
	$code = <<<CODE
<span class="egrn-tag-parent egrn-tag-parent--inline">
	<button class="egrn-button egrn-button--icon">
		<svg class="egrn-button-icon">
			<use xlink:href="#egrn-svg_sprite-icon-cart"/>
		</svg>
	</button>
	<span class="egrn-tag egrn-tag--red egrn-tag--top-left">0</span>
</span>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
			<span class="egrn-tag egrn-tag--red egrn-tag--bottom-left">0</span>
		</span>

<?php
	$code = <<<CODE
<span class="egrn-tag-parent egrn-tag-parent--inline">
	<button class="egrn-button egrn-button--icon">
		<svg class="egrn-button-icon">
			<use xlink:href="#egrn-svg_sprite-icon-cart"/>
		</svg>
	</button>
	<span class="egrn-tag egrn-tag--red egrn-tag--bottom-left">0</span>
</span>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
			<span class="egrn-tag egrn-tag--red egrn-tag--top-right">0</span>
		</span>

<?php
	$code = <<<CODE
<span class="egrn-tag-parent egrn-tag-parent--inline">
	<button class="egrn-button egrn-button--icon">
		<svg class="egrn-button-icon">
			<use xlink:href="#egrn-svg_sprite-icon-cart"/>
		</svg>
	</button>
	<span class="egrn-tag egrn-tag--red egrn-tag--top-right">0</span>
</span>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<span class="egrn-tag-parent egrn-tag-parent--inline">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
			<span class="egrn-tag egrn-tag--red egrn-tag--bottom-right">0</span>
		</span>

<?php
	$code = <<<CODE
<span class="egrn-tag-parent egrn-tag-parent--inline">
	<button class="egrn-button egrn-button--icon">
		<svg class="egrn-button-icon">
			<use xlink:href="#egrn-svg_sprite-icon-cart"/>
		</svg>
	</button>
	<span class="egrn-tag egrn-tag--red egrn-tag--bottom-right">0</span>
</span>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>


		<h2>С использованием псевдо-элементов</h2>
		
		<div class="block egrn-tag-parent egrn-tag-parent--pseudo egrn-tag--content-1"><span class="block-content">На блочном элементе</span></div>

		<br>
		<br>
		<br>

		На теге BUTTON (с доп. разметкой): &nbsp; &nbsp;
		<span class="egrn-tag-parent egrn-tag-parent--inline egrn-tag-parent--pseudo egrn-tag--content-5">
			<button class="egrn-button">Партнерская программа</button>
		</span>

		<br>
		<br>

		На теге BUTTON (без доп. разметки<!--, с косяком при фокусе-->): &nbsp; &nbsp;
		<button class="egrn-button egrn-tag-parent egrn-tag-parent--pseudo egrn-tag--content-5">Партнерская программа</button>

		<br>
		<br>
		<br>

		<span class="egrn-tag-parent egrn-tag-parent--inline egrn-tag-parent--pseudo egrn-tag--red egrn-tag--content-0 egrn-tag--top-left">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
		</span>

		&nbsp; &nbsp; 

		<span class="egrn-tag-parent egrn-tag-parent--inline egrn-tag-parent--pseudo egrn-tag--red egrn-tag--content-0 egrn-tag--bottom-left">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
		</span>

		&nbsp; &nbsp; 

		<span class="egrn-tag-parent egrn-tag-parent--inline egrn-tag-parent--pseudo egrn-tag--red egrn-tag--content-0 egrn-tag--top-right">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
		</span>

		&nbsp; &nbsp; 

		<span class="egrn-tag-parent egrn-tag-parent--inline egrn-tag-parent--pseudo egrn-tag--red egrn-tag--content-0 egrn-tag--bottom-right">
			<button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cart"/></svg></button>
		</span>

		<div class="usage"><span>Использование:</span></div>

		<div class="block egrn-tag-parent egrn-tag-parent--pseudo egrn-tag--content-1">
			<span class="block-content">На блочном элементе</span>
		</div>

<?php
	$code = <<<CODE
<div class="egrn-tag-parent egrn-tag-parent--pseudo egrn-tag--content-1">
	На блочном элементе
</div>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<div class="margin"></div>
		<div class="margin"></div>

	</div>

<?php include $baseUrl . '/partials/footer.php' ?>