<?php
	$baseUrl = '..';
	$pageTitle = 'Компонент для скрытия элементов';
?>
<?php include $baseUrl . '/partials/header.php' ?>

	<style>
		@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

		body {
			background: #f7f8f9;
		}
		.block {
			display: inline-block;
			width: 20rem;
			height: 10rem;
			border: thin dashed silver;
		}
		.block-content {
			line-height: 10rem;
			padding: 1rem;
		}
		.wrapper {
			border: thin solid black;
		}
	</style>

	<div class="content">

		<h1><?= $pageTitle ?></h1>

		<div class="margin"></div>

		<div class="columns">
			<div class="column">

				<button class="button" id="button">Кнопка</button>
				&nbsp;
				<!-- <button class="button" id="button-2">Инициализировать заново</button> -->

				<br><br>

				<div class="egrn-clip" id="clip">
					<div class="egrn-clip-content">
						<div class="wrapper">
							<div class="block"></div>
							<p>
								Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem quos at pariatur velit consectetur repudiandae delectus molestias facere harum assumenda, laborum consequatur voluptate cumque eum, cum iste error doloremque excepturi.
							</p>
							<p class="egrn-text-center">
								<button class="egrn-button egrn-button--submit">Кнопка</button>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<?php /* ?>
		<div class="margin"></div>

		<h2>С анимацией, без прерываний</h2>

		<button class="button" id="button-3">Кнопка</button>

		<br><br>

		<div class="egrn-clip egrn-clip--animated egrn-clip--expanded" id="clip-3">
			<div class="egrn-clip-content">
				<div class="wrapper">
					<div class="block"></div>
					<p>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem quos at pariatur velit consectetur repudiandae delectus molestias facere harum assumenda, laborum consequatur voluptate cumque eum, cum iste error doloremque excepturi.
					</p>
					<p class="egrn-text-center">
						<button class="egrn-button egrn-button--submit">Кнопка</button>
					</p>
				</div>
			</div>
		</div>
		<?php */ ?>

	</div>

	<?php /* ?>
	<script src="<?= $assetsUrl ?>/js/egrn-demo_clip.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
	<?php */ ?>
	<script>
		// #CBFIX Старые андроиды (Android Browser 4.4.4 (Webkit 537.36) и почему-то iOS Safari 10.3 (8.4, 9.3, 11.3 - ок)), вероятно из-за подгрузки шрифтов, выдают некорректные размеры элементов, если выполнять код до загрузки всех ресурсов. Поэтому используется событие load на window.
		window.addEventListener('load', function() {
		// document.addEventListener('egrn.svg_sprite.load', function() {

			var debug = false;
			// var debug = true;

			function init() {
				clip.dom.self.addEventListener(clip.collapsedEventName, function(event) {
					console.log('Событие: ' + clip.collapsedEventName);
					checkButtonTitle();
				});
				clip.dom.self.addEventListener(clip.expandedEventName, function(event) {
					console.log('Событие: ' + clip.expandedEventName);
					checkButtonTitle();
				});
				checkButtonTitle();
			}

			function checkButtonTitle() {
				if (clip.collapsed) {
					button.textContent = 'Развернуть';
				} else {
					button.textContent = 'Свернуть';
				}
			}

			var button = document.getElementById('button');
			// var button2 = document.getElementById('button-2');

			clip = new EGRN.Clip(document.getElementById('clip'), {
				debug: debug,
				// interruptable: false,
				// animate: false,
			});

			init();

			button.onclick = function() {
				clip.toggle();
			};

			// button2.onclick = function() {
			// 	console.log('Клик по второй кнопке');
			// 	clip.init(document.getElementById('clip'));
			// 	init();
			// };

		});
	</script>

<?php include $baseUrl . '/partials/footer.php' ?>