<?php
	$baseUrl = '..';
	$pageTitle = 'Кнопки';
?>
<?php include $baseUrl . '/partials/header.php' ?>

	<!-- Highlight.js -->
	<style>
		/* @import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/default.min.css'; */
		@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
	</style>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
	<script>hljs.initHighlightingOnLoad();</script>

	<!-- Выделение кода при клике на блок с кодом -->
	<script>
		document.addEventListener('DOMContentLoaded', function() {
			var codeBlocks = document.querySelectorAll('.code-block');
			for (var i = 0; i < codeBlocks.length; i++) {
				codeBlocks[i].addEventListener('click', function() {
					var selection = window.getSelection();
					var range = document.createRange();
					range.selectNodeContents(this);
					selection.removeAllRanges();
					selection.addRange(range);
				});
			}
		});
	</script>

	<style>
		@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

		body {
			background: #f7f8f9;
		}
	</style>

	<div class="content">

		<h1>Кнопки</h1>

		<h2>Светлая</h2>

		<p>
			<button class="egrn-button">Обычная</button>
		</p>

		<div class="items">
			<div class="item">
				<button class="egrn-button">На теге BUTTON</button>
			</div>
			<div class="item">
				<a class="egrn-button" href="#">На теге A</a>
			</div>
			<div class="item">
				<span class="egrn-button">На теге SPAN</span>
			</div>
			<div class="item">
				<div class="egrn-button">На теге DIV</div>
			</div>
		</div>

		<div class="margin"></div>

		<div class="items">
			<div class="item">
				<button class="egrn-button egrn-button--tight">Узкая</button>
			</div>
			<div class="item">
				<button class="egrn-button egrn-button--tight egrn-button--bigger-text">Узкая с увеличенным текстом</button>
			</div>
		</div>

		<div class="margin"></div>

		<div class="items">
			<div class="item">
				<button class="egrn-button egrn-button--dense">Компактная</button>
			</div>
			<div class="item">
				<button class="egrn-button egrn-button--dense egrn-button--bigger-text">Компактная с увеличенным текстом</button>
			</div>
		</div>

		<div class="margin"></div>

		<div class="items">
			<div class="item">
				<button class="egrn-button">
					<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-mail"/></svg>
					<span class="egrn-button-text">С иконкой</span>
				</button>
			</div>
			<div class="item">
				<button class="egrn-button">
					<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cog"/></svg>
					<span class="egrn-button-text">С иконкой</span>
				</button>
			</div>
			<div class="item">
				<button class="egrn-button">
					<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-search"/></svg>
					<span class="egrn-button-text">С иконкой</span>
				</button>
			</div>
			<div class="item">
				<button class="egrn-button">
					<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-phone"/></svg>
					<span class="egrn-button-text">С иконкой</span>
				</button>
			</div>
		</div>

		<p>
			<button class="egrn-button egrn-button--muted-icon">
				<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-mail"/></svg>
				<span class="egrn-button-text">С приглушенной иконкой</span>
			</button>
		</p>

		<p>
			<button class="egrn-button egrn-button--link">
				<!-- <svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-mail"/></svg> -->
				<span class="egrn-button-text"><svg class="egrn-button-text-icon egrn-button-text-icon--turn-90-cw"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg><span>В виде ссылки</span></span>
			</button>
		</p>

		<p>
			<button class="egrn-button egrn-button--block">
				Блочная на теге BUTTON
			</button>
			<br>
			<a class="egrn-button egrn-button--block" href="#">
				Блочная на теге A
			</a>
			<br>
			<button class="egrn-button egrn-button--block egrn-button--tight">
				Блочная узкая
			</button>
		</p>

		<p>
			<button class="egrn-button egrn-button--passive" tabindex="-1">Пассивная</button>
		</p>

		<div class="items">
			<div class="item">
				<button class="egrn-button" disabled>Отключенная</button>
			</div>
			<div class="item">
				<button class="egrn-button egrn-button--disabled" tabindex="-1">Отключенная (с помощью класса)</button>
			</div>
		</div>

		<p>
			Особый стиль (совмещает узкую, с увеличенным текстом и с приглушенной иконкой):
		</p>

		<div class="items">
			<div class="item">
				<button class="egrn-button egrn-button--special">
					<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-profile"/></svg>
					<span class="egrn-button-text">Дмитрий Сафронов</span>
				</button>
			</div>

			<div class="item">
				<button class="egrn-button egrn-button--special">
					<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-circle-plus"/></svg>
					<span class="egrn-button-text">Пополнить</span>
				</button>
			</div>
		</div>

		<p>
			Особый стиль 2 (особый стиль + компактная):

			<br><br>

			<button class="egrn-button egrn-button--special-2">
				<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-circle-plus"/></svg>
				<span class="egrn-button-text">Списком</span>
			</button>
		</p>

		<p>
			Иконка в тексте кнопки

			<br><br>

			<button class="egrn-button egrn-button--muted-icon egrn-button--tight">
				<span class="egrn-button-text">
					<svg class="egrn-button-text-icon egrn-button-text-icon--no-margin">
						<use xlink:href="#egrn-svg_sprite-icon-plus-slim"/>
					</svg>
					Объявление / Загрузить с ЦИАН
				</span>
			</button>
		</p>



		<div class="usage"><span>Использование:</span></div>

		<div class="items">
			<div class="item">
				<button class="egrn-button">Обычная</button>
			</div>
			<!-- <div class="item">
				<a class="egrn-button" href="#">Обычная</a>
			</div>
			<div class="item">
				<span class="egrn-button">Обычная</span>
			</div> -->
		</div>

<?php
	$code = <<<CODE
<button class="egrn-button">Обычная</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

<?php
	$code = <<<CODE
<a class="egrn-button" href="#">Обычная</a>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

<?php
	$code = <<<CODE
<span class="egrn-button">Обычная</span>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>
		
		<p>
			<button class="egrn-button egrn-button--tight">Узкая</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--tight">Узкая</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--tight egrn-button--bigger-text">Узкая с увеличенным текстом</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--tight egrn-button--bigger-text">
	Узкая с увеличенным текстом
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--dense">Компактная</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--dense">Компактная</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--dense egrn-button--bigger-text">Компактная с увеличенным текстом</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--dense egrn-button--bigger-text">
	Компактная с увеличенным текстом
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button">
				<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-mail"/></svg>
				<span class="egrn-button-text">С иконкой</span>
			</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button">
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-mail"/>
	</svg>
	<span class="egrn-button-text">С иконкой</span>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--muted-icon">
				<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-mail"/></svg>
				<span class="egrn-button-text">С приглушенной иконкой</span>
			</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--muted-icon">
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-mail"/>
	</svg>
	<span class="egrn-button-text">С приглушенной иконкой</span>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--block">Блочная</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--block">Блочная</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--block egrn-button--tight">Блочная узкая</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--block egrn-button--tight">
	Блочная узкая
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button" disabled>Отключенная</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button" disabled>Отключенная</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--special">
				<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-profile"/></svg>
				<span class="egrn-button-text">Особый стиль</span>
			</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--special">
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-profile"/>
	</svg>
	<span class="egrn-button-text">Особый стиль</span>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--special-2">
				<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-circle-plus"/></svg>
				<span class="egrn-button-text">Особый стиль 2</span>
			</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--special-2">
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-circle-plus"/>
	</svg>
	<span class="egrn-button-text">Особый стиль 2</span>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--muted-icon egrn-button--tight">
				<span class="egrn-button-text">
					<svg class="egrn-button-text-icon egrn-button-text-icon--no-margin">
						<use xlink:href="#egrn-svg_sprite-icon-plus-slim"/>
					</svg>
					Объявление / Загрузить с ЦИАН
				</span>
			</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--muted-icon egrn-button--tight">
	<span class="egrn-button-text">
		<svg class="egrn-button-text-icon egrn-button-text-icon--no-margin">
			<use xlink:href="#egrn-svg_sprite-icon-plus-slim"/>
		</svg>
		Объявление / Загрузить с ЦИАН
	</span>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

<?php
/*
  ██████  ██████  ███████ ███████ ███    ██
 ██       ██   ██ ██      ██      ████   ██
 ██   ███ ██████  █████   █████   ██ ██  ██
 ██    ██ ██   ██ ██      ██      ██  ██ ██
  ██████  ██   ██ ███████ ███████ ██   ████
*/
?>

		<h2>Зеленая</h2>

		<p>
			<button class="egrn-button egrn-button--green">Обычная</button>
		</p>

		<div class="items">
			<div class="item">
				<button class="egrn-button egrn-button--green">На теге BUTTON</button>
			</div>
			<div class="item">
				<a class="egrn-button egrn-button--green" href="#">На теге A</a>
			</div>
			<div class="item">
				<span class="egrn-button egrn-button--green">На теге SPAN</span>
			</div>
			<div class="item">
				<div class="egrn-button egrn-button--green">На теге DIV</div>
			</div>
		</div>

		<div class="margin"></div>
		
		<div class="items">
			<div class="item">
				<button class="egrn-button egrn-button--green egrn-button--tight">Узкая</button>
			</div>
			<div class="item">
				<button class="egrn-button egrn-button--green egrn-button--dense">Компактная</button>
			</div>
		</div>

		<p>
			<button class="egrn-button egrn-button--green egrn-button--block">
				Блочная на теге BUTTON
			</button>
			<br>
			<a class="egrn-button egrn-button--green egrn-button--block" href="#">
				Блочная на теге A
			</a>
			<br>
			<button class="egrn-button egrn-button--green egrn-button--block egrn-button--tight">
				Блочная узкая
			</button>
		</p>

		<div class="items">
			<div class="item">
				<span class="egrn-button egrn-button--gradient">С градиентом</span>
			</div>
			<div class="item">
				<button class="egrn-button egrn-button--green egrn-button--alt-font">Другим шрифтом</button>
			</div>
			<div class="item">
				<a class="egrn-button egrn-button--submit" href="#">Для заказа</a>
			</div>
			<div class="item">
				<button class="egrn-button egrn-button--green" disabled>Отключенная</button>
			</div>
			<div class="item">
				<button class="egrn-button egrn-button--submit" disabled>Отключенная</button>
			</div>
		</div>

		<div class="margin"></div>

		<div class="items">
			<div class="item">
				<button class="egrn-button egrn-button--green">
					<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-circle-plus"/></svg>
					<div class="egrn-button-text">С иконкой</div>
				</button>
			</div>
		</div>

		<div class="margin"></div>

		<div class="items">
			<div class="item">
				<button class="egrn-button egrn-button--green egrn-button--special">
					<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-search"/></svg>
					<div class="egrn-button-text">С иконкой (особый стиль)</div>
				</button>
			</div>
			<div class="item">
				<button class="egrn-button egrn-button--green egrn-button--special-2">
					<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-wallet"/></svg>
					<div class="egrn-button-text">С иконкой (особый стиль 2)</div>
				</button>
			</div>
		</div>

		<div class="usage"><span>Использование:</span></div>

		<p>
			<button class="egrn-button egrn-button--green">Обычная</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--green">Обычная</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>
		
		<p>
			<button class="egrn-button egrn-button--green egrn-button--tight">Узкая</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--green egrn-button--tight">Узкая</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--green egrn-button--dense">Компактная</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--green egrn-button--dense">Компактная</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--green egrn-button--block">Блочная</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--green egrn-button--block">Блочная</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--green egrn-button--block egrn-button--tight">Блочная узкая</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--green egrn-button--block egrn-button--tight">
	Блочная узкая
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--green egrn-button--gradient">С градиентом</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--green egrn-button--gradient">
	С градиентом
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--green egrn-button--alt-font">Другим шрифтом</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--green egrn-button--alt-font">
	Другим шрифтом
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--submit">Для заказа</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--submit">Для заказа</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			<button class="egrn-button egrn-button--submit" disabled>Отключенная</button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--submit" disabled>
	Отключенная
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

<?php
/*
 ██  ██████  ██████  ███    ██
 ██ ██      ██    ██ ████   ██
 ██ ██      ██    ██ ██ ██  ██
 ██ ██      ██    ██ ██  ██ ██
 ██  ██████  ██████  ██   ████
*/
?>

		<h2>Кнопка-иконка</h2>

		<div class="items">
			<div class="item">
				На теге BUTTON: &nbsp; <button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-layers2"/></svg></button>
			</div>
			<div class="item">
				На теге A: &nbsp; <a class="egrn-button egrn-button--icon" href="#"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-layers2"/></svg></a>
			</div>
		</div>

		<p>
			Размеры: &nbsp;
			
			<button class="egrn-button egrn-button--icon">
				<svg class="egrn-button-icon">
					<use xlink:href="#egrn-svg_sprite-icon-mail" />
				</svg>
			</button>

			&nbsp; &nbsp;

			<button class="egrn-button egrn-button--icon egrn-button--small"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-mail"/></svg></button>

			&nbsp; &nbsp;

			<button class="egrn-button egrn-button--icon egrn-button--mini"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-mail"/></svg></button>
		</p>

		<p>
			Без подложки: &nbsp;

			<button class="egrn-button egrn-button--icon egrn-button--no-substrate"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-profile"/></svg></button>
			
			&nbsp; &nbsp;

			<button class="egrn-button egrn-button--icon egrn-button--no-substrate egrn-button--small"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-profile"/></svg></button>

			&nbsp; &nbsp;

			<button class="egrn-button egrn-button--icon egrn-button--no-substrate egrn-button--mini"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-profile"/></svg></button>
		</p>

		<div class="items">
			<div class="item">
				Приглушенная: &nbsp; <button class="egrn-button egrn-button--icon egrn-button--muted"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-biglist"/></svg></button>
			</div>
			<div class="item">
				Отключенная: &nbsp; <button class="egrn-button egrn-button--icon" disabled><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-wallet"/></svg></button>
			</div>
		</div>

		<div class="margin"></div>

		Все варианты иконок: <br><br>

		<div class="items items--denser">
			<?php
				$icons = [];
				if (DEV) {
					$dir = new RecursiveDirectoryIterator('..\..\src\img\svg-sprite');
					$iterator = new RecursiveIteratorIterator($dir);
					foreach ($iterator as $path) {
						if ($path->isFile() and preg_match('/^icon-([a-zA-Z0-9-]+).svg/', $path->getFileName(), $matches)) {
							$icons[] = $matches[1];
						}
					}
				} else {
					$icons = json_decode(file_get_contents($baseUrl . '/partials/icon-index.json'));
				}
				foreach ($icons as $i => $icon) {
					echo '<div class="item"><button class="egrn-button egrn-button--icon" title="' . $icon . '"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-' . $icon . '"/></svg></button></div>';
				}
				
			?>
		</div>

		<div class="usage"><span>Использование:</span></div>

		<p>
			Обычная: &nbsp; <button class="egrn-button egrn-button--icon"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-layers2"/></svg></button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--icon">
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-layers2"/>
	</svg>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			Меньше: &nbsp; <button class="egrn-button egrn-button--icon egrn-button--small"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-layers2"/></svg></button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--icon egrn-button--small">
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-layers2"/>
	</svg>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			Мини: &nbsp; <button class="egrn-button egrn-button--icon egrn-button--mini"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-layers2"/></svg></button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--icon egrn-button--mini">
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-layers2"/>
	</svg>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			Без подложки: &nbsp; <button class="egrn-button egrn-button--icon egrn-button--no-substrate"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-profile"/></svg></button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--icon egrn-button--no-substrate">
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-profile"/>
	</svg>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			Приглушенная: &nbsp; <button class="egrn-button egrn-button--icon egrn-button--muted"><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-biglist"/></svg></button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--icon egrn-button--muted">
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-biglist"/>
	</svg>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<p>
			Отключенная: &nbsp; <button class="egrn-button egrn-button--icon" disabled><svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-wallet"/></svg></button>
		</p>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--icon" disabled>
	<svg class="egrn-button-icon">
		<use xlink:href="#egrn-svg_sprite-icon-wallet"/>
	</svg>
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>

		<h2>Кнопка без стилей</h2>

		<button>Кнопка без стилей</button>

	</div>

<?php include $baseUrl . '/partials/footer.php' ?>