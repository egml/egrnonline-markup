<?php
	$baseUrl = '..';
	$pageTitle = 'Попапы';
?>
<?php include $baseUrl . '/partials/header.php' ?>

<?php /* ?>
<?php */ ?>
<?php /* ?>
<!-- Highlight.js -->
<style>
	@import 'https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/github.min.css';
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

<!-- Выделение кода при клике на блок с кодом -->
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var codeBlocks = document.querySelectorAll('.code-block');
		for (var i = 0; i < codeBlocks.length; i++) {
			codeBlocks[i].addEventListener('click', function() {
				var selection = window.getSelection();
				var range = document.createRange();
				range.selectNodeContents(this);
				selection.removeAllRanges();
				selection.addRange(range);
			});
		}
	});
</script>
<?php /**/ ?>

<style>
	@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';

	body {
		/* background: #f7f8f9; */
	}

	svg:not(:root) {
		overflow: visible;
	}
	svg {
		width: 20rem;
		height: 10rem;
	}
	.popup {
		width: 20rem;
		position: relative;
		box-sizing: border-box;
		padding: 1rem 1.5rem 1.3rem 3rem;
	}
	.popup > .substrate {
		display: flex;
		align-items: center;
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		opacity: .5;
	}
	.popup > .substrate::before {
		content: '';
		border: 1.5rem solid transparent;
		border-left: 0;
		/* border-right-color: rgba(0, 0, 0, .5); */
		border-right-color: #000;
		width: 0;
		height: 0;
		margin: 1rem 0;
	}
	.popup > .substrate::after {
		content: '';
		/* color: #fff; */
		/* background: rgba(0, 0, 0, .5); */
		background: #000;
		/* border-radius: .5rem; */
		height: 100%;
		/* padding: 1rem 1.5rem 1.3rem; */
		box-sizing: border-box;
		flex: 1 0 0;
		box-shadow: 0 0 .5rem #000;
	}
	.popup-content {
		color: #fff;
		position: relative;
	}

	
	.popup-alt {
		width: 20rem;
		height: 10rem;
		position: relative;
		color: #fff;
		background: rgba(0, 0, 0, .5);
		border-radius: .5rem;
		padding: 1rem 1.5rem;
		box-sizing: border-box;
	}
	.popup-alt::before {
		content: '';
		position: absolute;
		left: -1.5rem;
		top: calc(50% - 1.5rem);
		border: 1.5rem solid transparent;
		border-left: 0;
		border-right-color: rgba(0, 0, 0, .5);
		width: 0;
		height: 0;
	}
</style>

<div class="content">
	<h1><?= $pageTitle ?></h1>

	<div class="popup">
		<div class="substrate"></div>
		<div class="popup-content">Lorem ipsum dolor sit amet consectetur, adipisicing elit</div>
	</div>

	<div class="margin"></div>

	<?php /* ?>

	<div class="usage"><span>Использование:</span></div>

	<div class="items">
		<div class="item">
			<button class="egrn-button">Обычная</button>
		</div>
		<!-- <div class="item">
			<a class="egrn-button" href="#">Обычная</a>
		</div>
		<div class="item">
			<span class="egrn-button">Обычная</span>
		</div> -->
	</div>

	<?php */ ?>

<?php /* ?>

<?php
	$code = <<<CODE
<button class="egrn-button egrn-button--tight egrn-button--bigger-text">
	Узкая с увеличенным текстом
</button>
CODE;
	echo '<pre><code class="code-block html">' . htmlspecialchars($code) . '</code></pre>';
?>


<?php
	$code = <<<CODE
document.addEventListener('egrn.appready', function() {
	var area = document.querySelector('.egrn-collapsible_area');
	new EGRN.CollapsibleArea(area);
});
CODE;
	echo '<pre><code class="code-block code-block--label js">' . htmlspecialchars($code) . '</code></pre>';
?>

<?php */ ?>

</div>

<?php /* ?>
<script src="<?= $assetsUrl ?>/js/egrn-demo_name.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
<?php */ ?>
	
<?php include $baseUrl . '/partials/footer.php' ?>