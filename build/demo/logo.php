<?php
	$baseUrl = '..';
	$pageTitle = 'Логотип';
	// $template = 'simple';
?>
<?php include $baseUrl . '/partials/header.php' ?>

	<style>
		@import '<?= $baseUrl ?>/demo/assets/demo.css?<?php include $baseUrl . '/partials/cache-buster.txt' ?>';
	</style>

	<?php /* ?>
	<style>
		.a {
			font-size: 1rem;
			width: 200px;
			height: 50px;
		}
		.a.a--border {
			border: 2px solid green;
		}
		.a.a--blue {
			background: dodgerblue;
		}
		.a.a--orange, .a {
			background: orange;
		}
		.a.a--small {
			width: 180px;
			height: 30px;
		}
	</style>
	<?php */ ?>

	<?php /* ?>
	<script src="<?= $assetsUrl ?>/js/egrn.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
	<?php */ ?>	

	<div class="content">
		<h1>Логотип</h1>

		<p style="text-align:center;">
			<a class="egrn-logo" href="#" title="">
				<svg class="egrn-logo-icon"><use xlink:href="#egrn-svg_sprite-logo-icon-color"></use></svg>
				<svg class="egrn-logo-text"><use xlink:href="#egrn-svg_sprite-logo-text"></use></svg>
			</a>
		</p>

		<p style="text-align:center;">
			<button class="egrn-logo egrn-logo--only-icon">
				<svg class="egrn-logo-icon"><use xlink:href="#egrn-svg_sprite-logo-icon-color"></use></svg>
				<svg class="egrn-logo-text"><use xlink:href="#egrn-svg_sprite-logo-text"></use></svg>
			</button>
		</p>

		<p style="text-align:center;">
			<button class="egrn-logo egrn-logo--vertical">
				<svg class="egrn-logo-icon"><use xlink:href="#egrn-svg_sprite-logo-icon-color"></use></svg>
				<svg class="egrn-logo-text"><use xlink:href="#egrn-svg_sprite-logo-text"></use></svg>
			</button>
		</p>

		<p style="text-align:center;">
			<button class="egrn-logo egrn-logo--vertical egrn-logo--only-icon">
				<svg class="egrn-logo-icon"><use xlink:href="#egrn-svg_sprite-logo-icon-color"></use></svg>
				<svg class="egrn-logo-text"><use xlink:href="#egrn-svg_sprite-logo-text"></use></svg>
			</button>
		</p>
		
		<!-- <p class="a a--small">
			.a.a--small
		</p>

		<p class="a a--border a--bg">
			.a.a--border.a--bg
		</p>

		<p class="a a--border a--small">
			.a.a--border.a--small
		</p> -->
		
		<?php /* ?>
		<div class="egrn-scrollable" style="height:20rem;">
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>

			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>

			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>

			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Pariatur quasi dolores dignissimos atque, perspiciatis, voluptate maiores quos est quisquam, numquam libero doloremque tempore ex delectus eveniet voluptatem maxime? Doloribus, atque.</p>
		</div>

		<script>
			document.addEventListener('egrn.appready', function() {
				new EGRN.Scrollable(document.querySelector('.egrn-scrollable'), {shadows: true});
			});
		</script>

		<div class="margin"></div>
		<?php */ ?>

		<?php /* ?>
		<?php */ ?>
		<?php /* ?> Сворачиваемые области
		<button class="egrn-collapsible_area_trigger" id="trigger1">
			<span class="egrn-collapsible_area_trigger-aligner">
				<span class="egrn-collapsible_area_trigger-title">
					Сворачиваемая область
				</span>
				<span class="egrn-collapsible_area_trigger-extra"></span>
				<span class="egrn-collapsible_area_trigger-indicator">
					<svg class="egrn-collapsible_area_trigger-indicator-icon">
						<use xlink:href="#egrn-svg_sprite-icon-chevron"/>
					</svg>
				</span>
			</span>
		</button>

		<div class="egrn-collapsible_area" data-egrn-collapsible-area-trigger="#trigger1">
			<div class="egrn-collapsible_area-content">
				<p>
					Lorem ipsum dolor sit amet consectetur adipisicing elit.
					Pariatur recusandae quaerat neque omnis quis perspiciatis 
					porro possimus facilis earum nobis, consectetur blanditiis, 
					repellendus mollitia doloribus laudantium consequatur 
					dolores non quasi.
				</p>
			</div>
		</div>

		<script>
			document.addEventListener('egrn.appready', function() {
				var area = document.querySelector('.egrn-collapsible_area');
				new EGRN.CollapsibleArea(area);
			});
		</script>
		<?php /**/ ?>





		<?php /* ?>

		<h2>Заголовок</h2>
		<p>
			Lorem ipsum dolor sit amet consectetur adipisicing elit. Necessitatibus vero molestias nihil odit qui ipsam facilis nam officiis ad inventore reprehenderit, dolores eos saepe.

			<button class="egrn-button">
				<svg class="egrn-button-icon">
					<use xlink:href="#egrn-svg_sprite-icon-mail"/>
				</svg>
				<span class="egrn-button-text">С иконкой</span>
			</button>

			Quam nihil ex ratione <span class="egrn-text-alt-font">cum modi</span>!
		</p>
		<?php */ ?>
		<?php
			/*
			*/
			/*
			for ($i=0; $i < 2000; $i++) { 
				echo '<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Numquam fugiat quia dolore, pariatur dolorum obcaecati beatae explicabo necessitatibus alias, eaque minima molestias nostrum enim nesciunt illo, voluptates aliquid est voluptatem.</p>';
			}
			/**/
		?>
	</div>

<?php include $baseUrl . '/partials/footer.php' ?>