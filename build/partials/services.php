<div class="egrn-block-section egrn-block-section--margin">
	<div class="egrn-object-title">
		<h2 class="egrn-object-kadnum">77:09:0004020:1114</h2>
		<div class="egrn-object-address">
			Москва г, ул Ямского Поля 5-я, д 19-21
		</div>
	</div>
	<p class="egrn-paragraph egrn-paragraph--margin-20">
		<a class="egrn-link" href="#">Другая собственность на объекте (4)</a>
	</p>
</div>
<div class="egrn-block-section egrn-block-section--padding-for-submit">
	<div style="margin:0 0 1.4rem;">
		<button class="egrn-collapsible_trigger egrn-collapsible_trigger--alt-font" id="egrn-collapsible_trigger-1">
			<span class="egrn-collapsible_trigger-aligner">
				<span class="egrn-collapsible_trigger-title">Выписки</span>
				<span class="egrn-collapsible_trigger-extra"></span>
				<span class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</span>
			</span>
		</button>
	
		<div class="egrn-collapsible egrn-collapsible--optimize-animation_" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-1">
			<div class="egrn-collapsible-clip egrn-clip">
				<div class="egrn-collapsible-clip-content egrn-clip-content">
					<div style="margin:1.4rem 0;">
						<button class="egrn-collapsible_trigger egrn-collapsible_trigger--inline" id="egrn-collapsible_trigger-2">
							<span class="egrn-collapsible_trigger-aligner">
								<span class="egrn-collapsible_trigger-title">Электронные</span>
								<span class="egrn-collapsible_trigger-extra"></span>
								<span class="egrn-collapsible_trigger-indicator">
									<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
								</span>
							</span>
						</button>
						&nbsp; &nbsp; <a class="egrn-link egrn-link--dashed" href="#">Образцы</a>
						 
						<div class="egrn-collapsible" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-2">
							<div class="egrn-collapsible-clip egrn-clip">
								<div class="egrn-collapsible-clip-content egrn-clip-content">
									<p class="egrn-paragraph--margin-15">
										Выписка из ЕГРН<br>
										<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
									</p>
									<p class="egrn-paragraph--margin-15">
										Выписка из ЕГРН о переходе прав<br>
										<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
									</p>
									<p class="egrn-paragraph--margin-15">
										Справка о кадастровой стоимости<br>
										<span class="egrn-text egrn-text--muted">49 ₽, 1-3 рабочих дня</span>
									</p>
									<p class="egrn-paragraph--margin-15" style="margin-bottom:1rem;">
										<a class="egrn-link egrn-link--pseudo" href="#">Пакет выписок из ЕГРН</a> (5 - 1000 шт.)<br>
										<span class="egrn-text egrn-text--highlighted">от 10 р. за выписку</span>
									</p>
								</div>
							</div>
						</div>
					</div>
	
					<div style="margin:1.4rem 0 0;">
						<button class="egrn-collapsible_trigger egrn-collapsible_trigger--collapsed egrn-collapsible_trigger--inline" id="egrn-collapsible_trigger-3">
							<span class="egrn-collapsible_trigger-aligner">
								<span class="egrn-collapsible_trigger-title">Бумажные с печатью</span>
								<span class="egrn-collapsible_trigger-extra"></span>
								<span class="egrn-collapsible_trigger-indicator">
									<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
								</span>
							</span>
						</button>
						&nbsp; &nbsp; <a class="egrn-link egrn-link--dashed" href="#">Образцы</a>
						 
						<div class="egrn-collapsible egrn-collapsible--collapsed" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-3">
							<div class="egrn-collapsible-clip egrn-clip">
								<div class="egrn-collapsible-clip-content egrn-clip-content">
									<p class="egrn-paragraph--margin-15">
										Выписка из ЕГРН основные характеристики<br>
										<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
									</p>
									<p class="egrn-paragraph--margin-15">
										Выписка из ЕГРН о переходе прав<br>
										<span class="egrn-text egrn-text--muted">49 ₽, 5-30 мин</span>
									</p>
									<p class="egrn-paragraph--margin-15">
										Справка о кадастровой стоимости<br>
										<span class="egrn-text egrn-text--muted">49 ₽, 1-3 рабочих дня</span>
									</p>
									<p class="egrn-paragraph--margin-15" style="margin-bottom:.8rem;">
										<a class="egrn-link egrn-link--pseudo" href="#">Пакет выписок из ЕГРН</a> (5 - 1000 шт.)<br>
										<span class="egrn-text egrn-text--highlighted">от 10 р. за выписку</span>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div style="margin:1.4rem 0;">
		<button class="egrn-collapsible_trigger egrn-collapsible_trigger--alt-font egrn-collapsible_trigger--collapsed" id="egrn-collapsible_trigger-4">
			<span class="egrn-collapsible_trigger-aligner">
				<span class="egrn-collapsible_trigger-title">Кадастровые услуги</span>
				<span class="egrn-collapsible_trigger-extra"></span>
				<span class="egrn-collapsible_trigger-indicator">
					<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
				</span>
			</span>
		</button>
	
		<div class="egrn-collapsible egrn-collapsible--collapsed" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-4">
			<div class="egrn-collapsible-clip egrn-clip">
				<div class="egrn-collapsible-clip-content egrn-clip-content">
					<p class="egrn-paragraph--margin-15">
						Кадастровые услуги
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="egrn-block-submit">
	<button class="egrn-button egrn-button--submit">Заказать выбранное (1)</button>
</div>
<script>
	window.addEventListener('load', function() {
		EGRN.utils.createWidgets(EGRN.Collapsible, document.querySelector('.egrn-block'));
	});
</script>
