<?php 
	defined('DEV') or define('DEV',
		(
			strpos($_SERVER['SERVER_ADDR'], '::1') !== false ||
			strpos($_SERVER['SERVER_ADDR'], '127.0.0.1') !== false ||
			strpos($_SERVER['SERVER_ADDR'], '192.168.1.34') !== false ||
			strpos($_SERVER['SERVER_ADDR'], '192.168.1.35') !== false ||
			strpos($_SERVER['SERVER_NAME'], 'localhost') !== false ||
			strpos($_SERVER['HTTP_HOST'], 'localhost') !== false
		) &&
		strpos($_SERVER['REMOTE_ADDR'], '192.168.1.33') === false
	);

	if (!isset($baseUrl)) {
		$baseUrl = '.';
	}
	$requestUri = str_replace('\\', '/', dirname($_SERVER['REQUEST_URI']));
	if (strlen($requestUri) > 1) {
		if (substr($requestUri, -1) != '/') {
			$requestUri .= '/';
		}
	} elseif ($requestUri != '/') {
		$requestUri .= '/';
	} else {
		$requestUri = '/';
	}
	$assetsUrl = $requestUri . $baseUrl . '/assets';

	if (!isset($pageTitle)) {
		$pageTitle = 'Главная страница';
	}
	if (!isset($headTitle)) {
		$headTitle = $pageTitle . ' - Верстка ЕГРН Онлайн';
	}

	if (!isset($template)) {
		$template = 'default'; // default, simple
	}
?>
<!DOCTYPE html>
<html>
	<?php switch ($template): case 'simple': ?>

		<head>
			<meta charset="utf-8">
			<title><?= $headTitle ?></title>
			<link rel="stylesheet" href="<?= $assetsUrl ?>/css/normalize.css">
			<link rel="stylesheet" href="<?= $assetsUrl ?>/css/egrn.css?<?php include 'cache-buster.txt' ?>">
		</head>
		<body>

	<?php break; case 'default': default: ?>

		<head>
			<meta charset="utf-8">
			<title><?= $headTitle ?></title>
			<?php /* ?>
			<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
			<?php */ ?>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<meta name="format-detection" content="telephone=no">
			<link rel="icon" href="<?= $assetsUrl ?>/img/egrn-favicon.png?<?php include 'cache-buster.txt' ?>">
			<link rel="stylesheet" href="<?= $assetsUrl ?>/css/egrn.css?<?php include 'cache-buster.txt' ?>">
			<script src="<?= $assetsUrl ?>/js/egrn.js?<?php include 'cache-buster.txt' ?>"></script>
			<script>
				EGRN.options.assetsUrl = '<?= $assetsUrl ?>';
				EGRN.options.cacheBuster = '<?php include 'cache-buster.txt' ?>';
			</script>
		</head>
		<body>
			<div class="egrn-app egrn-app--not-loaded">
				<script>EGRN.importSvgSprite()</script>
			
	<?php endswitch ?>