		<?php /* ?>
		<?php */ ?>
		<?php /* ?>
		<script>
			document.addEventListener('DOMContentLoaded', function() {
				var el = document.createElement('div');
				el.style.cssText = 
					'border-width: 0 thin thin 0;' +
					'background-color: dodgerblue;' +
					'color: #fff;' +
					'font-family: sans-serif;' +
					'font-size: 1em;' +
					'padding: .3em .5em;' +
					'opacity: .75;' +
					'position: fixed;' +
					'top: 0;' +
					'left: 0;' +
					'z-index: 32767;';
				el.innerHTML = '<b>' + window.innerWidth + '×' + window.innerHeight + '</b><span style="color:lightskyblue"> – ' + window.navigator.userAgent + '</span>';
				el.addEventListener('click', function() {
					el.parentNode.removeChild(el);
				});
				document.body.appendChild(el);
			});
		</script>
		<?php /**/ ?>

		<?php /* ?>
		<?php */ ?>
		<?php switch ($template): case 'simple': ?>
		<?php break; case 'default': default: ?>
			</div>
		<?php endswitch ?>
	</body>
</html>