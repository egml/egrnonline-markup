<?php
	if (!isset($topbarClasses)) {
		$topbarClasses = [];
	}
	if (empty($topbarClasses)) {
		$topbarClasses = '';
	} else {
		$topbarClasses = ' ' . implode(' ', $topbarClasses);
	}
?>
<div class="egrn-topbar<?= $topbarClasses ?>">
	<div class="egrn-topbar-logo">
		<a class="egrn-logo" href="<?= $baseUrl ?>/frontpage.php" title="">
			<svg class="egrn-logo-icon"><use xlink:href="#egrn-svg_sprite-logo-icon-color"></use></svg><svg class="egrn-logo-text"><use xlink:href="#egrn-svg_sprite-logo-text"></use></svg>
		</a>
	</div>
	<div class="egrn-topbar-left">

		<div class="egrn-topbar-menu">
			<a class="egrn-topbar-menu-link" href="#">Публичная карта</a>
			<a class="egrn-topbar-menu-link" href="#">Специалисты</a>
			<a class="egrn-topbar-menu-link" href="#">Услуги</a>
			<a class="egrn-topbar-menu-link" href="#">Статус заявки</a>
		</div>

	</div>
	<div class="egrn-topbar-right">

		<div class="egrn-topbar-phones">
			<span class="egrn-topbar-phone">8 (800) 100-34-34</span>
			<span class="egrn-topbar-phone">+7 (495) 917-38-25</span>
		</div>

		<div class="egrn-topbar-icons">
			<a class="egrn-topbar-icon egrn-topbar-icon-menu" href="#">
				<svg class="egrn-icon">
					<use xlink:href="#egrn-svg_sprite-icon-menu"></use>
				</svg>
			</a>
			<a class="egrn-topbar-icon egrn-topbar-icon-star" href="#">
				<svg class="egrn-icon">
					<use xlink:href="#egrn-svg_sprite-icon-star"></use>
				</svg>
			</a>
			<a class="egrn-topbar-icon egrn-topbar-icon--muted egrn-topbar-icon-cart" href="#">
				<svg class="egrn-icon egrn-icon--scale-110">
					<use xlink:href="#egrn-svg_sprite-icon-cart"></use>
				</svg>
			</a>
			<a class="egrn-topbar-icon egrn-topbar-icon-messages" href="#">
				<svg class="egrn-icon">
					<use xlink:href="#egrn-svg_sprite-icon-mail"></use>
				</svg>
			</a>
			<a class="egrn-topbar-icon egrn-topbar-icon-profile" href="<?= $baseUrl ?>/lk.php">
				<svg class="egrn-icon egrn-icon--scale-110_">
					<use xlink:href="#egrn-svg_sprite-icon-profile"></use>
				</svg>
			</a>
		</div>

	</div>
</div>