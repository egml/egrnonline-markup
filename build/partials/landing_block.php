<h1 class="egrn-h1 egrn-block-title">
	Выписка из ЕГРН
	<span class="egrn-heading-smaller_line">для заказа выберите объект на карте</span>
</h1>

<div class="egrn-block-sections">
	<div class="egrn-block-section">
		<div class="egrn-h3">Электронные выписки <a class="egrn-link egrn-link--pseudo egrn-heading-link" href="#">Образцы</a></div>
		<p>
			Выписка из ЕГРН основные характеристики<br>
			<span class="egrn-text-muted">49 ₽, 5-30 мин</span>
		</p>
		<p>
			Выписка из ЕГРН о переходе прав<br>
			<span class="egrn-text-muted">49 ₽, 5-30 мин</span>
		</p>
		<p>
			Справка о кадастровой стоимости<br>
			<span class="egrn-text-muted">49 ₽, 1-3 рабочих дня</span>
		</p>
		<p>
			<a class="egrn-link egrn-link--pseudo" href="#">Пакет выписок из ЕГРН</a> (5 - 1000 шт.)<br>
			<span class="egrn-text-highlighted">от 10 р. за выписку</span>
		</p>
	</div>
	<div class="egrn-block-section">
		<div class="egrn-h3">Бумажные выписки с печатью <a class="egrn-link egrn-link--pseudo egrn-heading-link" href="#">Образцы</a></div>
		<p>
			Срочная выписка из ЕГРН с печатью<br>
			<span class="egrn-text-muted">4690 ₽, 1-2 раб. дня, доставка по Москве</span>
		</p>
		<p>
			Выписка из ЕГРН с печатью за 4 дня<br>
			<span class="egrn-text-muted">3290 ₽, 4 раб. дня, доставка по Москве</span>
		</p>
	</div>
	<div class="egrn-block-section">
		<div class="egrn-h3">Полезная информация</div>
		<p>Купить ЕГРН можно, обратившись в ЕГРН Онлайн – заявитель не всегда точно составляет грамотные запросы, а без точности в заявлении могут отказать. Кроме того, если раньше для того, чтобы заказать ЕГРН, необходимо было ехать в две инстанции – ЕГРП или кадастровый орган, то теперь можно узнать цену.</p>
		<p>Но если нужно продлить этот абзац, а продлит его, несомненно, теперь нужно, то я
		без проблем это, собственно, и сделаю.</p>
	</div>
</div>