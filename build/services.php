<?php
	$baseUrl = '.';
?>
<?php include $baseUrl . '/partials/header.php' ?>

<div class="egrn-stage egrn-stage--object-selected">
	<div class="egrn-stage-map" id="egrn-stage-map"></div>

	<div class="egrn-stage-top">
		<?php include $baseUrl . '/partials/topbar.php' ?>
	</div>

	<div class="egrn-stage-center">
		<div class="egrn-tools">
			<div class="egrn-toolbar egrn-toolbar--expanded">
				<div id="egrn-search-tool" class="egrn-tool egrn-search_tool egrn-search_tool--invitation">
					<label class="egrn-tool-icon" for="egrn-search_tool-field-input-1">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-search"></use></svg>
					</label>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Поиск объекта</div></div></div>

						<div class="egrn-search_tool-field">
							<div class="egrn-search_tool-field-input">
								<input class="egrn-search_tool-field-input-control" id="egrn-search_tool-field-input-1" type="search" placeholder="Адрес или кадастровый номер" autofocus autocomplete="off"><!-- Этот комментарий важен. В этот контейнер может быть вставлен еще контент. И рассчитано, что пробела между полем ввода и новым контентом не должно быть, поэтому здесь комментарий обязателен.
							--></div>

							<div class="egrn-search_tool-field-submit">
								<div class="egrn-search_tool-field-submit-clip">
									<button class="egrn-search_tool-field-submit-control" type="button" disabled>Найти</button>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="egrn-layers-tool" class="egrn-tool">
					<div class="egrn-tool-icon">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-layers2"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Настройки карты</div></div></div>
					</div>
				</div>

				<div id="egrn-ruler-tool" class="egrn-tool">
					<div class="egrn-tool-icon">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-ruler"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Измерения</div></div></div>
					</div>
				</div>

				<div id="egrn-print-tool" class="egrn-tool">
					<div class="egrn-tool-icon">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-printer"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Печать</div></div></div>
					</div>
				</div>

				<div id="egrn-panorama-tool" class="egrn-tool">
					<div class="egrn-tool-icon egrn-tool-icon--scale-in">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-360"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Яндекс Панорамы</div></div></div>
					</div>
				</div>

				<div id="egrn-location-tool" class="egrn-tool">
					<div class="egrn-tool-icon">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-location"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Ваше местоположение</div></div></div>
					</div>
				</div>

				<div id="egrn-share-tool" class="egrn-tool">
					<div class="egrn-tool-icon">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-link"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Поделиться</div></div></div>
					</div>
				</div>

				<div id="egrn-contact-tool" class="egrn-tool">
					<div class="egrn-tool-icon">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-mail2"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Обратная связь</div></div></div>
					</div>
				</div>

				<div id="egrn-help-tool" class="egrn-tool">
					<div class="egrn-tool-icon">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-circle-question"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Справка</div></div></div>
					</div>
				</div>

				<div id="egrn-estate-tool" class="egrn-tool">
					<div class="egrn-tool-icon egrn-tool-icon--special egrn-tool-icon--scale-in">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-housevalue"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Поиск недвижимости</div></div></div>
					</div>
				</div>

				<div id="egrn-settings-tool" class="egrn-tool">
					<div class="egrn-tool-icon">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-cog"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Состав меню</div></div></div>
					</div>
				</div>
			</div>
			<div class="egrn-toolbar">
				<div id="egrn-zoom-in-tool" class="egrn-tool">
					<div class="egrn-tool-icon egrn-tool-icon--scale-out-special">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-plus"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Увеличить</div></div></div>
					</div>
				</div>

				<div id="egrn-zoom-out-tool" class="egrn-tool">
					<div class="egrn-tool-icon egrn-tool-icon--scale-out-special">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-minus"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Уменьшить</div></div></div>
					</div>
				</div>

				<div id="egrn-marker-tool" class="egrn-tool">
					<div class="egrn-tool-icon egrn-tool-icon--scale-in">
						<svg class="egrn-tool-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-hide-pins"></use></svg>
					</div>
					<div class="egrn-tool-ui">
						<div class="egrn-tool-title"><div class="egrn-tool-title-aligner"><div class="egrn-tool-title-text">Убрать метки</div></div></div>
					</div>
				</div>
			</div>
		</div>

		<div class="egrn-block">
			<button class="egrn-block-close egrn-button egrn-button--icon egrn-button--no-substrate egrn-button--muted">
				<svg class="egrn-button-icon">
					<use xlink:href="#egrn-svg_sprite-icon-cross"/>
				</svg>
			</button>
			<div class="egrn-block-content">
				<?php include $baseUrl . '/partials/services.php' ?>
			</div>
		</div>
	</div>

	<div class="egrn-bottombar">
		<div class="egrn-bottombar-group egrn-bottombar-group--dark-special">
			<a class="egrn-bottombar-link" href="#">Не нашли?</a>
		</div>
		<div class="egrn-bottombar-group">
			<a class="egrn-bottombar-link" href="#">Регионы</a>
			<a class="egrn-bottombar-link" href="#">Оферта</a>
			<a class="egrn-bottombar-link" href="#">Контакты</a>
		</div>
	</div>

</div>

<script>
	window.addEventListener('load', function() {
		EGRN.stage.initialize(); // Это должно быть раньше виджета тулбара, иначе не сработает показ оверлея
		EGRN.utils.createWidgets(EGRN.Toolbar);
		// EGRN.Tool.prototype.instances[1].activate();
		EGRN.Tool.prototype.instances[12].action = function() {
			EGRN.stage.map.leafletMap.zoomIn();
			this.deactivate();
		};
		EGRN.Tool.prototype.instances[13].action = function() {
			EGRN.stage.map.leafletMap.zoomOut();
			this.deactivate();
		};
	});
</script>

<?php include $baseUrl . '/partials/footer.php' ?>