<?php
	$baseUrl = '.';
	$pageTitle = 'Личный кабинет';
?>
<?php include $baseUrl . '/partials/header.php' ?>

<div class="egrn-page">
	<?php include $baseUrl . '/partials/topbar.php' ?>

	<div class="egrn-page-content">
		<h1 class="egrn-page-title">Личный кабинет</h1>

		<div class="egrn-lk">
			<div class="egrn-lk-left">
				<div class="egrn-section egrn-section--combine-alt egrn-section--dashed-bottom">
					<div class="egrn-lk-first-line">
						<div class="egrn-lk-first-line-user">
							<span class="egrn-button egrn-button--special egrn-button--block egrn-button--text-ellipsis egrn-button--passive" title="Дмитрий Сафронов" tabindex="-1">
								<svg class="egrn-button-icon">
									<use xlink:href="#egrn-svg_sprite-icon-profile"/>
								</svg>
								<span class="egrn-button-text">Дмитрий Сафронов</span>
							</span>
						</div>

						<div class="egrn-lk-first-line-other">
							<div class="egrn-row-item">
								<button class="egrn-button egrn-button--special">Счета</button>
							</div>

							<div class="egrn-row-item">
								<button class="egrn-button egrn-button--special">Акты</button>
							</div>

							<div class="egrn-row-item">
								<button class="egrn-button egrn-button--icon egrn-button--no-substrate egrn-button--muted egrn-button--tight egrn-button--compensate-padding">
									<svg class="egrn-button-icon">
										<use xlink:href="#egrn-svg_sprite-icon-logout"/>
									</svg>
								</button>
							</div>
						</div>
					</div>
				</div>

				<div class="egrn-section egrn-section--combine-alt egrn-section--dashed-bottom">
					<div class="egrn-items egrn-items--center">
						<div class="egrn-items-item egrn-items-item--double-margin">
							<span class="egrn-text egrn-text--bold egrn-text--size-relative-24">12 250</span> <span class="egrn-text egrn-text--size-relative-15">₽</span>
						</div>

						<div class="egrn-items">
							<div class="egrn-items-item">
								<button class="egrn-button egrn-button--special">
									<svg class="egrn-button-icon">
										<use xlink:href="#egrn-svg_sprite-icon-circle-plus"/>
									</svg>
									<span class="egrn-button-text">Пополнить</span>
								</button>
							</div>

							<div class="egrn-items-item">
								<button class="egrn-button egrn-button--special">Вывести</button>
							</div>
						</div>
					</div>
				</div>

				<div class="egrn-section egrn-section--combine-alt egrn-section--dashed-bottom">
					<div class="egrn-text egrn-text--center">
						<span class="egrn-text egrn-text--size-relative-15">Бесплатных электронных выписок ЕГРН:</span> <span class="egrn-text egrn-text--bold egrn-text--size-relative-24">5</span>
					</div>

					<p class="egrn-text egrn-text--center egrn-text--size-relative-15">
						Акции на покупку пакета электронных выписок ЕГРН <br> для кадастровых инженеров (цена для всех - 49 р.):
					</p>

					<div class="egrn-helper-width-60 egrn-helper-block-center">
						<div class="egrn-items egrn-items--vertical egrn-items--stretch">
							<div class="egrn-items-item">
								<button class="egrn-button egrn-button--special egrn-button--block">
									500 выписок по 6 р. (3000 р.)
								</button>
							</div>

							<div class="egrn-items-item">
								<button class="egrn-button egrn-button--special egrn-button--block">
									100 выписок по 20 р. (2000 р.)
								</button>
							</div>
						</div>
					</div>
				</div>
				
				<div class="egrn-section egrn-section--combine-alt egrn-section--dashed-bottom">
					<div class="egrn-text egrn-text--center">
						<span class="egrn-tag-parent egrn-tag-parent--inline">
							<button class="egrn-button egrn-button--special">Партнерская программа (3000 р. за клиента)</button>
							<span class="egrn-tag">1</span>
						</span>
					</div>
				</div>

				<div class="egrn-section egrn-section--combine-alt">
					<div class="egrn-text egrn-text--center egrn-items egrn-items--vertical">
						<div class="egrn-items-item egrn-text egrn-text--size-relative-15">
							У Вас <span class="egrn-text egrn-text--bold egrn-text--size-relative-24">0</span> объявлений по недвижимости
						</div>

						<div class="egrn-items-item">
							<button class="egrn-button egrn-button--muted-icon egrn-button--tight">
								<span class="egrn-button-text">
									<svg class="egrn-button-text-icon egrn-button-text-icon--no-margin">
										<use xlink:href="#egrn-svg_sprite-icon-plus-slim"/>
									</svg>
									Объявление / Загрузить с ЦИАН
								</span>
							</button>
						</div>
					</div>
				</div>
			</div>

			<div class="egrn-lk-right">
				<div class="egrn-lk-right-content">
					<h2 class="egrn-lk-right-heading">Ваши заказы</h2>
					<button class="egrn-collapsible_trigger egrn-collapsible_trigger--dark" id="egrn-collapsible_trigger-1">
						<span class="egrn-collapsible_trigger-aligner">
							<span class="egrn-collapsible_trigger-title">Новый заказ</span>
							<span class="egrn-collapsible_trigger-extra">(б-р Цветной, вл 7, стр 11: Эл. выписка ЕГРН, Межевание)</span>
							<span class="egrn-collapsible_trigger-indicator">
								<svg class="egrn-collapsible_trigger-indicator-icon"><use xlink:href="#egrn-svg_sprite-icon-chevron"/></svg>
							</span>
						</span>
					</button>

					<div class="egrn-collapsible" data-egrn-collapsible-trigger="#egrn-collapsible_trigger-1">
						<div class="egrn-collapsible-clip egrn-clip">
							<div class="egrn-collapsible-clip-content egrn-clip-content">
							
								<div class="egrn-row egrn-row--item-margin-15 egrn-row--paragraph">
									<div class="egrn-row egrn-row-item egrn-row-item--fill">
										<div class="egrn-row-item egrn-row-item--fill egrn-row-item--margin-10">
											<input class="egrn-text_input egrn-text_input--tight egrn-text_input--bigger-text egrn-text_input--fill" type="text" placeholder="Адрес или кадастровый номер к добавлению">
										</div>
										<div class="egrn-row-item">
											<button class="egrn-button egrn-button--green egrn-button--tight egrn-button--bigger-text" type="submit" disabled>Найти</button>
										</div>
									</div>
									
									<?php /* ?>
									<div class="egrn-search_field">
										<label class="egrn-search_field-icon" for="egrn-search_field-input-1">
											<svg class="egrn-search_field-icon-graphics"><use xlink:href="#egrn-svg_sprite-icon-search"></use></svg>
										</label>

										<div class="egrn-search_field-input">
											<input class="egrn-search_field-input-control" id="egrn-search_field-input-1" type="search" autofocuss placeholder="Адрес или кадастровый номер к добавлению">
										</div>

										<div class="egrn-search_field-submit">
											<button class="egrn-search_field-submit-control" type="submit" disabled>Найти</button>
										</div>
									</div>
									<?php */ ?>	

									<div class="egrn-row-item">
										<button class="egrn-button egrn-button--special-2">
											<svg class="egrn-button-icon">
												<use xlink:href="#egrn-svg_sprite-icon-circle-plus"/>
											</svg>
											<span class="egrn-button-text">Списком</span>
										</button>
									</div>
								</div>

								<div class="egrn-order-object egrn-order-object--blue">
									<div class="egrn-order-object-header">
										<div class="egrn-order-object-header-line egrn-row egrn-row--top">
											<div class="egrn-order-object-address egrn-row-item egrn-row-item--margin-15">г.Москва, б-р Цветной, вл 7, стр 11</div>
											<div class="egrn-row-item egrn-text egrn-text--no-wrap">
												<a class="egrn-link" style="position:relative; top:.2rem;" href="#"><svg class="egrn-link-icon"><use xlink:href="#egrn-svg_sprite-icon-pin"></use></svg>На карте</a>
											</div>
										</div>
										<?php /* ?>
										<div class="egrn-order-object-header-line">
											<span class="egrn-order-object-address">г.Москва, б-р Цветной, вл 7, стр 11</span> &nbsp; <a class="egrn-link" href="#"><svg class="egrn-link-icon"><use xlink:href="#egrn-svg_sprite-icon-pin"/></svg>На карте</a>
										</div>
										<?php */ ?>
										<div class="egrn-order-object-header-line">
											<div class="egrn-row">
												<div class="egrn-order-object-details egrn-row-item egrn-row-item--fill">
													<span class="egrn-order-object-details-item">77:01:0003044:3833</span>
													<span class="egrn-order-object-details-item">107 м<sup>2</sup></span>
												</div>
												<div class="egrn-row-item">
													<a class="egrn-link egrn-link--pseudo" href="#">Инфо</a>
												</div>
											</div>
										</div>
									</div>
									<div class="egrn-order-object-content">
										<button class="egrn-button egrn-button--special-2">
											<svg class="egrn-button-icon">
												<use xlink:href="#egrn-svg_sprite-icon-circle-plus"/>
											</svg>
											<span class="egrn-button-text">Добавить услугу</span>
										</button>
										<div class="egrn-order-section">
											<h2 class="egrn-order-section-heading">Выписки</h2>

											<div class="egrn-order-positions">
												<div class="egrn-order-position">
													<div class="egrn-row-item egrn-row-item--fill">
														<div class="egrn-row egrn-row--bottom egrn-row--edge egrn-row--margin-5">
															<div class="egrn-row-item">
																<strong class="egrn-text egrn-text--size-relative-15">Электронная выписка из ЕГРН</strong>
															</div>
															<div class="egrn-row-item egrn-row-item--dashed-filler"></div>
															<div class="egrn-row-item egrn-text egrn-text--no-wrap">
																<strong class="egrn-text egrn-text--size-relative-15">49 &#8381;</strong>
															</div>
														</div>

														<div class="egrn-row egrn-row--top egrn-row--item-margin-15">
															<div class="egrn-row-item egrn-row-item--fill egrn-row-item--margin-20">
																<span class="egrn-text egrn-text--muted">ФИО собственника квартиры, кадастровая стоимость, наличие/отсутствие обременений</span>
															</div>
															<div class="egrn-row-item">
																<span class="egrn-text egrn-text--no-wrap">5-30 мин</span>
															</div>
															<div class="egrn-row-item">
																<a class="egrn-link egrn-link--pseudo" href="#">Образец</a>
															</div>
														</div>

														<p class="egrn-order-status egrn-order-status--green egrn-order-status--margin">
															Оплачиваете сейчас на сайте и получаете в указанные сроки на электронную почту (бумажные - экспресс-доставкой)
														</p>
													</div>

													<div class="egrn-row-item">
														<button class="egrn-order-position-remove">
															<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cross"/></svg>
														</button>
													</div>
												</div>
												
												<div class="egrn-order-position">
													<div class="egrn-row-item egrn-row-item--fill">
														<div class="egrn-row egrn-row--bottom egrn-row--edge egrn-row--margin-5">
															<div class="egrn-row-item">
																<strong class="egrn-text egrn-text--size-relative-15">Электронная выписка из ЕГРН</strong>
															</div>
															<div class="egrn-row-item egrn-row-item--dashed-filler"></div>
															<div class="egrn-row-item egrn-text egrn-text--no-wrap">
																<strong class="egrn-text egrn-text--size-relative-15">49 &#8381;</strong>
															</div>
														</div>

														<div class="egrn-order-status egrn-order-status--green">
															Оплачено. Выписка отправлена на почту. <a class="egrn-link egrn-link--pseudo" href="#">Скачать</a>
														</div>
													</div>
													
													<div class="egrn-row-item">
														<button class="egrn-order-position-remove">
															<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cross"/></svg>
														</button>
													</div>
												</div>

												<div class="egrn-order-position">
													<div class="egrn-row-item egrn-row-item--fill">
														<div class="egrn-row egrn-row--bottom egrn-row--edge egrn-row--margin-5">
															<div class="egrn-row-item">
																<strong class="egrn-text egrn-text--size-relative-15">Электронная выписка из ЕГРН</strong>
															</div>
															<div class="egrn-row-item egrn-row-item--dashed-filler"></div>
															<div class="egrn-row-item egrn-text egrn-text--no-wrap">
																<strong class="egrn-text egrn-text--size-relative-15">49 &#8381;</strong>
															</div>
														</div>

														<div class="egrn-order-status egrn-order-status--green">
															Оплачено. Выписка отправлена на почту. <a class="egrn-link egrn-link--pseudo" href="#">Скачать</a>
														</div>
													</div>

													<div class="egrn-row-item">
														<button class="egrn-order-position-remove">
															<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cross"/></svg>
														</button>
													</div>
												</div>
											</div>
										</div>

										<div class="egrn-order-section">
											<h2 class="egrn-order-section-heading">Услуги</h2>

											<div class="egrn-order-positions">
												<div class="egrn-order-position">
													<div class="egrn-row-item egrn-row-item--fill">
														<div class="egrn-row egrn-row--bottom egrn-row--edge egrn-row--margin-5">
															<div class="egrn-row-item">
																<strong class="egrn-text egrn-text--size-relative-15">Межевание земельного участка</strong>
															</div>
															<div class="egrn-row-item egrn-row-item--dashed-filler"></div>
															<div class="egrn-row-item egrn-text egrn-text--no-wrap">
																<strong class="egrn-text egrn-text--size-relative-15">от 14 900 &#8381;</strong>
															</div>
														</div>

														<div class="egrn-row egrn-row--top egrn-row--item-margin-10">
															<div class="egrn-row-item egrn-row-item--fill">
																<span class="egrn-text egrn-text--muted"></span>
															</div>
															<div class="egrn-row-item">
																<span class="egrn-text egrn-text--no-wrap">~1,5 мес</span>
															</div>
															<div class="egrn-row-item">
																<a class="egrn-link egrn-link--pseudo" href="#">Образец</a>
															</div>
														</div>

														<div class="egrn-order-status egrn-order-status--green egrn-order-status--margin">
															<ol class="egrn-list egrn-list--ordered">
																<li>
																	С Вами свяжется специалист, подберет кадастрового инженера и назовет сумму
																</li>
																<li>
																	Примете <a class="egrn-link egrn-link--pseudo" href="#">оферту</a> на сайте или подпишете договор в офисе или на объекте
																</li>
																<li>
																	Оплатите в офисе, на объекте или по выставленному на электронную почту счету через сайт
																</li>
															</ol>
														</div>
													</div>

													<!-- <div class="egrn-row-item">
														<button class="egrn-order-position-remove">
															<svg class="egrn-button-icon"><use xlink:href="#egrn-svg_sprite-icon-cross"/></svg>
														</button>
													</div> -->
												</div>
											</div>
										</div>

									</div>
								</div>

							</div>
						</div>
					</div>

					<!-- <img style="width:100%; display:block; margin-top:15rem" src="demo/lk-part.png"> -->
				</div>
			</div>
		</div>

	</div>
</div>

<script>EGRN.utils.createWidgets(EGRN.Collapsible)</script>
<?php /* ?>
<script src="<?= $assetsUrl ?>/js/egrn-frontpage.js?<?php include $baseUrl . '/partials/cache-buster.txt' ?>"></script>
<?php */ ?>

<?php include $baseUrl . '/partials/footer.php' ?>