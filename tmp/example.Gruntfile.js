module.exports = function(grunt) {
	grunt.config.init({

		/** -------------------------------------------
		 *  SASS
		 * -------------------------------------------- */
		sass: {
			options: {
				// Requires Sass 3.4.0, which can be installed with gem install sass
				// auto - relative paths where possible, file URIs elsewhere
				// file - always absolute file URIs
				// inline - include the source text in the sourcemap
				// none - no sourcemaps
				sourcemap: 'none', // auto (default), file, inline, none
				// Show a full traceback on error.
				trace: false,
				// Force Unix newlines in written files.
				//unixNewlines: false, // false on Windows, otherwise true
				// Just check the Sass syntax, does not evaluate and write the output.
				check: false,
				// Output style. Can be nested, compact, compressed, expanded.
				style: 'expanded',
				// How many digits of precision to use when outputting decimal numbers.
				precision: 5,
				// Silence warnings and status messages during compilation.
				quiet: false,
				// Make Compass imports available and load project configuration (config.rb located close to the Gruntfile.js).
				compass: false,
				// Emit extra information in the generated CSS that can be used by the FireSass Firebug plugin.
				debugInfo: false,
				// Emit comments in the generated CSS indicating the corresponding source line.
				lineNumbers: false,
				// Add a (or multiple) Sass import path.
				//loadPath: ''|[]
				//loadPath: ['src/css/fonts'],
				// Require a (or multiple) Ruby library before running Sass.
				//require: ''|[]
				// The path to put cached Sass files.
				cacheLocation: 'node_modules/grunt-contrib-sass/.sass-cache',
				// Don't cache to sassc files.
				noCache: false,
				// Run sass with bundle exec: bundle exec sass.
				bundleExec: false,
				// Only compile changed files.
				update: true,
			},
			build: {
				options: {
					style: 'compressed',
					update: false,
				},
				files: [{
					expand: true,
					cwd: 'src/css',
					src: ['**/*.scss'],
					ext: '.css',
					dest: 'build/css',
				}],
			},
			dev: {
				files: [{
					expand: true,
					cwd: 'src/css',
					src: ['**/*.scss'],
					ext: '.css',
					dest: 'dev/css',
				}],
			},
		},

		/** -------------------------------------------
		 *  IMAGEMIN
		 * -------------------------------------------- */
		imagemin: {
			build: {
				files: [{
					expand: true,
					cwd: 'src/img',
					src: ['**/*.{svg,png,jpg,gif}', '!svg_sprite/*'],
					dest: 'build/img'
				}],
			},
			dev: {
				files: [{
					expand: true,
					cwd: 'src/img',
					src: ['**/*.{svg,png,jpg,gif}', '!svg_sprite/*'],
					dest: 'dev/img'
				}],
			},
			svg_sprite: {
				options: {
					svgoPlugins: [
						// { mergePaths: false },
						{ cleanupNumericValues: false },
						{ removeDimensions: true }, // remove width/height attributes if viewBox is present (disabled by default)
						// { cleanupIDs: false },
						{ removeUselessStrokeAndFill: false },
						// { removeViewBox: true },
						// { removeMetadata: true },
						// { cleanupAttrs: true },
						{ convertPathData: false },
						/*
						{ addClassesToSVGElement: false },
						{ cleanupAttrs: true },
						{ cleanupEnableBackground: true },
						{ cleanupIDs: true },
						{ cleanupNumericValues: true },
						{ collapseGroups: true },
						{ convertColors: true },
						{ convertPathData: true },
						{ convertShapeToPath: true },
						{ convertStyleToAttrs: true },
						{ convertTransform: true },
						{ mergePaths: true },
						{ moveElemsAttrsToGroup: true },
						{ moveGroupAttrsToElems: true },
						{ removeComments: true },
						{ removeAttrs: false }, // по RegEx
						{ removeDimensions: false }, // remove width/height attributes if viewBox is present (disabled by default)
						{ removeDesc: true },
						{ removeDoctype: true },
						{ removeEditorsNSData: true },
						{ removeEmptyAttrs: true },
						{ removeEmptyContainers: true },
						{ removeEmptyText: true },
						{ removeHiddenElems: true },
						{ removeMetadata: true },
						{ removeNonInheritableGroupAttrs: true },
						{ removeRasterImages: true },
						{ removeTitle: true },
						{ removeUnknownsAndDefaults: true },
						{ removeUnusedNS: true },
						{ removeUselessStrokeAndFill: true },
						{ removeViewBox: false },
						{ removeXMLProcInst: true },
						{ removeStyleElement: false },
						{ sortAttrs: true },
						{ transformsWithOnePath: true },
						*/
					]
				},
				files: [{
					expand: true,
					cwd: 'src/img/svg_sprite',
					src: ['*.svg'],
					dest: 'runtime/svg_sprite',
					ext: '.optimized.svg',
				}],
			}
		},

		/** -------------------------------------------
		 *  SVGSTORE
		 * -------------------------------------------- */
		svgstore: {
			options: {
				prefix : 'mv-svg_sprite-',
				/*
					options.cleanup (since 0.2.6)
					Type: boolean or Array Default value: false
					This option can be used to clean up inline definitions that may jeopardise later CSS-based styles.
					When set to true clean up all inline style attributes.
					Apart from true / false, the value of this property can be an array of inline attributes ( like fill, stroke, ...) you want to remove from the elements in the SVG.
				*/
				cleanup: false,
				/*
					Include a <title> element for screen readers. Uses the filename if no <title> was found on the source SVG. Set to false only if you are providing your own accessible name in your code.
				*/
				includeTitleElement: false,
			},
			build: {
				files: {
					'build/img/sprite.svg': ['runtime/svg_sprite/*'],
				},
			},
			dev: {
				files: {
					'dev/img/sprite.svg': ['runtime/svg_sprite/*'],
				},
			},
		},

		/** -------------------------------------------
		 *  WATCH
		 * -------------------------------------------- */
		watch: {
			options: {
				spawn: false,
			},
			js_main: {
				files: ['src/js/**/*.js', '!src/js/browser-supported/**/*'],
				tasks: ['browserify:dev'],
			},
			js_browser_supported: {
				files: ['src/js/browser-supported/**/*.js'],
				tasks: ['browserify:browser_supported'],
			},
			css: {
				files: ['src/css/**/*.scss'],
				tasks: ['sass:dev'],
			},
			img: {
				files: ['src/img/**/*.{svg,png,jpg,gif}', '!src/img/svg_sprite/*'],
				tasks: ['newer:imagemin:dev'],
			},
			svg_sprite: {
				files: ['src/img/svg_sprite/*.svg'],
				tasks: [
					'clean:svg_sprite',
					'imagemin:svg_sprite',
					'svgstore:dev',
					'processhtml:dev',
				],
			},
			gruntfile: {
				files: ['Gruntfile.js'],
				tasks: ['jshint:gruntfile'],
			},
			html: {
				files: ['src/html/*.html', '!src/html/_*.html'],
				tasks: ['newer:processhtml:dev'],
			},
			html_partials: {
				files: ['src/html/_*.html'],
				tasks: ['processhtml:dev'],
			},
		},

		/** -------------------------------------------
		 *  CONCURRENT
		 * -------------------------------------------- */
		concurrent: {
			options: {
				logConcurrentOutput: true,
			},
			default: [
				'watch:js_main',
				'watch:js_browser_supported',
				'watch:css',
				'watch:img',
				'watch:svg_sprite',
				'watch:html',
				'watch:html_partials',
				// 'watch:gruntfile',
			],
		},

		/** -------------------------------------------
		 *  PROCESSHTML
		 * -------------------------------------------- */
		processhtml: {
			options: {
				recursive: true,
				data: {
					cacheBrake: grunt.template.today('yyyymmddHHMMss'),
				}
			},
			build: {
				files: [{
					expand: true,
					cwd: 'src/html',
					src: ['*.html', '!_*.html'],
					dest: 'build',
					ext: '.html',
				}]
			},
			dev: {
				files: [{
					expand: true,
					cwd: 'src/html',
					src: ['*.html', '!_*.html'],
					dest: 'dev',
				}]
			}
		},

		/** -------------------------------------------
		 *  COPY
		 * -------------------------------------------- */
		copy: {
			build: {
				files: [
					// CSS
					/*
					{ src: 'bower_components/normalize-css/normalize.css', dest: 'build/css/normalize.css' },
					{ src: 'src/css/helpers.css', dest: 'build/css/helpers.css' },
					*/
					// JS
					/*
					{ src: 'bower_components/modernizr/modernizr.js', dest: 'build/js/modernizr.js' },
					{ src: 'bower_components/jquery/dist/jquery.js', dest: 'build/js/jquery.js' },
					{ src: 'bower_components/jquery.transit/jquery.transit.js', dest: 'build/js/jquery.transit.js' },
					{ src: 'bower_components/spin.js/spin.js', dest: 'build/js/spin.js' },
					{ src: 'bower_components/mustache.js/mustache.js', dest: 'build/js/mustache.js' },
					{
						expand: true,
						cwd: 'src/js',
						src: ['*.js'],
						dest: 'build/js',
					},
					*/
					// Шрифты
					{
						expand: true,
						cwd: 'src/fonts/elektra',
						src: ['*.{eot,svg,ttf,woff,woff2}'],
						dest: 'build/fonts',
					},
					// Разное
				]
			},
			dev: {
				files: [
					// Шрифты
					{
						expand: true,
						cwd: 'src/fonts/elektra',
						src: ['*.{eot,svg,ttf,woff,woff2}'],
						dest: 'dev/fonts',
					},
					// Разное
				],
			},
		},

		/** -------------------------------------------
		 *  JSHINT
		 * -------------------------------------------- */
		jshint: {
			gruntfile: ['Gruntfile.js'],
		},

		/** -------------------------------------------
		 *  CLEAN
		 * -------------------------------------------- */
		clean: {
			build: 'build',
			dev: 'dev',
			svg_sprite: 'runtime/svg_sprite/*',
		},

		/** -------------------------------------------
		 *  COMPRESS
		 * -------------------------------------------- */
		compress: {
			backup: {
				options: {
					archive: 'backup/' + grunt.template.today('yyyymmddHHMMss') + '.zip',
				},
				files: [
					{ src: ['.imdone/**'], dest: '' },
					{ src: ['src/**'], dest: '' },
					{ src: [
						'.gitignore',
						'Gruntfile.js',
						'bower.json',
						'package.json',
						'README.md',
					], dest: ''},
				],
			},
			backup_src: {
				options: {
					archive: 'backup/src-' + grunt.template.today('yyyymmddHHMMss') + '.zip',
				},
				files: [
					{ src: ['src/**'], dest: '' },
				],
			}
		},

		/** -------------------------------------------
		 *  BROWSERIFY
		 * -------------------------------------------- */
		browserify: {
			options: {
				browserifyOptions: {
					noParse: [
						'./node_modules/jquery/dist/jquery.js',
						'./bower_components/jquery1/dist/jquery.js',
						'jquery1',
						'jquery',
						'spin.js',
						'browsernizr',
						'mustache',
					],
				}
			},
			build: {
				files: {
					'build/js/mastervideo.js': 'src/js/mastervideo.js',
					'build/js/browser-supported.js': 'src/js/browser-supported/browser-supported.js'
				},
				options: {
					browserifyOptions: {
						noParse: [
							'./node_modules/jquery/dist/jquery.js',
							'./bower_components/jquery1/dist/jquery.js',
							'jquery1',
							'jquery',
							'spin.js',
							'browsernizr',
							'mustache',
						],
						debug: true,
					},
					plugin: [
						[
							'minifyify',
							{
								map: false,
								// map: './bundle.js.map',
								// output: 'bundle.js.map',
							},
						],
					],
				},
			},
			dev: {
				files: {
					'dev/js/mastervideo.js': 'src/js/mastervideo.js',
				},
			},
			browser_supported: {
				files: {
					'dev/js/browser-supported.js': 'src/js/browser-supported/browser-supported.js'
				},
			},
		},
	});

	grunt.loadNpmTasks('grunt-notify');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-svgstore');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-concurrent');
	grunt.loadNpmTasks('grunt-processhtml');
	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.loadNpmTasks('grunt-browserify');

	grunt.registerTask('default', ['concurrent']);
	grunt.registerTask('build', [
		'clean:build',
		'copy:build',
		'sass:build',
		'imagemin:build',
		'clean:svg_sprite',
		'imagemin:svg_sprite',
		'svgstore:build',
		'processhtml:build',
		'browserify:build',
	]);
	grunt.registerTask('dev', [
		'clean:dev',
		'copy:dev',
		'sass:dev',
		'imagemin:dev',
		'clean:svg_sprite',
		'imagemin:svg_sprite',
		'svgstore:dev',
		'processhtml:dev',
		'browserify:dev',
		'browserify:browser_supported',
	]);
	grunt.registerTask('svg', [
		'clean:svg_sprite',
		'imagemin:svg_sprite',
		'svgstore:dev',
		'processhtml:dev',
	]);
	grunt.registerTask('backup', [ 'compress:backup' ]);
	grunt.registerTask('backup_src', [ 'compress:backup_src' ]);

};