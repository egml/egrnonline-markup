html {
  font-size: .625em;
}

body {
  font-size: 1.4rem;
  font-family: "PT Sans", sans-serif;
  line-height: 1.3;
  color: #54565b;
}

/*
 ██      ██ ███    ██ ██   ██
 ██      ██ ████   ██ ██  ██
 ██      ██ ██ ██  ██ █████
 ██      ██ ██  ██ ██ ██  ██
 ███████ ██ ██   ████ ██   ██
*/
.egrn-link, a {
  color: #30789e;
  text-decoration: none;
  transition: none;
}
.egrn-link:hover, a:hover, .egrn-link:focus, a:focus {
  text-decoration: underline;
}
.egrn-link:focus, a:focus {
  outline: 0;
}
.egrn-link.egrn-link--pseudo, a.egrn-link--pseudo {
  text-decoration: none;
  border-bottom: dashed thin #30789e;
}
.egrn-link.egrn-link--pseudo:hover, a.egrn-link--pseudo:hover, .egrn-link.egrn-link--pseudo:focus, a.egrn-link--pseudo:focus {
  color: #21526b;
}

/*
 ██   ██ ███████  █████  ██████  ██ ███    ██  ██████
 ██   ██ ██      ██   ██ ██   ██ ██ ████   ██ ██
 ███████ █████   ███████ ██   ██ ██ ██ ██  ██ ██   ███
 ██   ██ ██      ██   ██ ██   ██ ██ ██  ██ ██ ██    ██
 ██   ██ ███████ ██   ██ ██████  ██ ██   ████  ██████
*/
.egrn-h1 {
  color: #54565b;
  font-family: 'bebasneue-forced';
  font-weight: 700;
  text-transform: uppercase;
  line-height: 1.14;
  font-size: 3.1rem;
}

.egrn-h2 {
  color: #54565b;
  font-family: 'bebasneue-forced';
  font-weight: 700;
  text-transform: uppercase;
  line-height: 1.14;
  font-size: 2.6rem;
}

.egrn-h3 {
  color: #54565b;
  font-family: 'bebasneue-forced';
  font-weight: 700;
  text-transform: uppercase;
  line-height: 1.14;
  font-size: 2rem;
}

.egrn-heading-smaller_line {
  display: block;
  font-size: .6452em;
  color: #787b82;
  margin-top: .19em;
}

.egrn-heading-link {
  font-size: 1.4rem;
  font-family: "PT Sans", sans-serif;
  font-weight: 400;
  text-transform: none;
  margin: 0 .5rem;
  vertical-align: middle;
}

/*
 ████████ ███████ ██   ██ ████████
    ██    ██       ██ ██     ██
    ██    █████     ███      ██
    ██    ██       ██ ██     ██
    ██    ███████ ██   ██    ██
*/
.egrn-text-paragraph, p {
  margin: 1.3em 0;
}

.egrn-text-muted {
  color: #9398a7;
  font-family: "PT Sans", sans-serif;
}

.egrn-text-highlighted {
  display: inline-block;
  background-color: #ffdc67;
  padding: 0 .25em .2em;
  margin: .1em 0;
}

/*
 ██████   ██████  ██   ██
 ██   ██ ██    ██  ██ ██
 ██████  ██    ██   ███
 ██   ██ ██    ██  ██ ██
 ██████   ██████  ██   ██
*/
.egrn-box-title {
  margin-bottom: 3.4rem;
}

.egrn-box-section {
  margin-bottom: 3.5rem;
}
.egrn-box-section:last-child {
  margin-bottom: 0;
}
.egrn-box-section .egrn-text-paragraph:last-child, .egrn-box-section p:last-child {
  margin-bottom: 0;
}

.egrn-map-overlay {
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	z-index: 1;
	/*pointer-events: none;*/
	background-color: #dbdfe3;
	opacity: 0.95;
	transition: opacity .2s linear;
}

@keyframes egrn-slidein-top {
	to {
		transform: translateY(0);
		opacity: 1;
	}
}
@keyframes egrn-slideout-top {
	to {
		transform: translateY(-3rem);
		opacity: 0;
	}
}
@keyframes egrn-welcome-arrow-shaft {
	from { opacity: 1 }
	to {
		opacity: 1;
		stroke-dasharray: 130 200;
	}
}
@keyframes egrn-welcome-arrow-head {
	from { opacity: 1 }
	to {
		opacity: 1;
		stroke-dasharray: 35 200;
	}
}
.egrn-welcome {
	position: absolute;
	top: 11rem;
	left: 21rem;
	z-index: 2;
	cursor: default;
}
.egrn-welcome-heading {
	opacity: 0;
	transform: translateY(-4rem);
	animation: .8s egrn-slidein-top forwards 1s;
}
.egrn-welcome-tip {
	opacity: 0;
	transform: translateY(-4rem);
	font-size: 1.9rem;
	font-weight: 700;
	animation: .8s egrn-slidein-top forwards 1.4s;
}
.egrn-welcome-arrow {
	position: absolute;
	top: -.6rem;
	left: -10rem;
	width: 9rem;
	height: 9rem;
	stroke: #616161;
	stroke-width: 2.8;
	stroke-linecap: round;
	stroke-linejoin: round;
}
#egrn-welcome-arrow-shaft {
	opacity: 0;
	animation: .8s egrn-welcome-arrow-shaft forwards 1.9s;
	stroke-dasharray: 0 200;
}
#egrn-welcome-arrow-head {
	opacity: 0;
	animation: .5s egrn-welcome-arrow-head forwards 2.5s;
	stroke-dasharray: 0 200;
}