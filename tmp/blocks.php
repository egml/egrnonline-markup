<?php 
	$baseUrl = '..';
	$pageTitle = 'Блоки';
?>
<?php include '../partials/header.php' ?>
		<link rel="stylesheet" href="../css/egrn-new.css?<?php include '../partials/cache-buster.txt' ?>">
		<style>
			body {
				background:#fff;
			}

			/* Это только для демонстрации, чтобы лучше смотрелось */
			.egrn-btn {
				margin: .8rem 0;
			}

			.demo-row {
				padding:20px 10px 0 10px;
				max-width:1200px;
				margin:0 auto;
				text-align:center;
			}

			.demo-title {
				font-size:28px;
				line-height:28px;
				font-family:'bebasneue-forced';
				font-weight:700;
				padding:0 5px 10px 5px;
			}

			.demo-subtitle {
				font-size:16px;
				line-height:16px;
				padding:0 5px 10px 5px;
			}

			.demo-item {
				display:inline-block;
				vertical-align:top;
				margin:0 5px 10px 5px;
				max-width:300px;
				text-align:left;
			}

			.demo-item--250 {
				max-width:none;
				width:250px;
			}

			.demo-item--300 {
				max-width:none;
				width:300px;
			}

			.demo-item--340 {
				max-width:none;
				width:340px;
			}

			.demo-item--350 {
				max-width:none;
				width:350px;
			}

			.demo-item--540 {
				max-width:none;
				width:540px;
			}
		</style>
		<div class="demo-row">
			<div class="demo-item demo-item--340">
				<div class="egrn-left-panel">
					<div class="egrn-object-info">
						<div class="egrn-object-info-head">
							<span class="egrn-object-info-head__number">
								77:01:0004036:19
							</span>
							г. Москва, ул. 2-я Звенигородская, вл.&nbsp;8
						</div>
						<ul class="egrn-object-info-links">
							<li class="egrn-object-info-links__item">
								<a class="egrn-object-info-link egrn-object-info-link--arrow-left" href="javascript:void(0);">
									<svg class="egrn-object-info-link__arrow"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
									Назад в&nbsp;список
								</a>
							</li>
							<li class="egrn-object-info-links__item">
								<a class="egrn-object-info-link" href="javascript:void(0);">
									Поиск в&nbsp;границах объекта
								</a>
							</li>
						</ul>
						<div class="egrn-object-info-data">
							<table class="egrn-object-info-data-table">
								<tbody>
									<tr>
										<td>
											Тип:
										</td>
										<td>
											Земельный участок
										</td>
									</tr>
									<tr>
										<td>
											Межевание:
										</td>
										<td>
											Погрешность определения границ
										</td>
									</tr>
									<tr>
										<td>
											Кад. квартал:
										</td>
										<td>
											<a class="egrn-link" href="javascript:void);">77:01:0004036</a>
										</td>
									</tr>
									<tr>
										<td>
											Статус:
										</td>
										<td>
											Ранее учтенный
										</td>
									</tr>
									<tr>
										<td>
											Категории земель:
										</td>
										<td>
											Земли населенных пунктов
										</td>
									</tr>
									<tr>
										<td>
											Форма собственности:
										</td>
										<td>
											публичная
										</td>
									</tr>
									<tr>
										<td>
											Права:
										</td>
										<td>
											77-77-12/033/2014-989 от 2014-07-08 (Постоянное (бессрочное) пользование)
										</td>
									</tr>
									<tr>
										<td>
											Ограничения:
										</td>
										<td>
											-
										</td>
									</tr>
									<tr>
										<td>
											Кадастровая стоимость:
										</td>
										<td>
											654 936 590 руб.
										</td>
									</tr>
									<tr>
										<td>
											Уточненная площадь:
										</td>
										<td>
											15 400 кв. м
										</td>
									</tr>
									<tr>
										<td>
											Разрешенное использование:
										</td>
										<td>
											Для размещения объектов дошкольного, начального, общего и среднего (полного) общего образования
										</td>
									</tr>
									<tr>
										<td>
											по документу:
										</td>
										<td>
											объекты размещения учреждений начального и среднего общего образования (1.2.17)
										</td>
									</tr>
									<tr>
										<td>
											Кадастровый инженер:
										</td>
										<td>
											Субботина Людмила Владимировна
										</td>
									</tr>
									<tr>
										<td>
											Дата постановки на&nbsp;учет:
										</td>
										<td>
											01.01.2007
										</td>
									</tr>
									<tr>
										<td>
											Дата изменения сведений в&nbsp;ГКН:
										</td>
										<td>
											20.12.2016
										</td>
									</tr>
									<tr>
										<td>
											Дата выгрузки сведений из&nbsp;ГКН:
										</td>
										<td>
											20.12.2016
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item demo-item--350">
				<div class="egrn-collapsable">
					<div class="egrn-collapsable-trigger" data-toggle="collapse" data-target="#collapsable1" role="button" aria-expanded="false" aria-controls="collapsable1">
						<div class="egrn-collapsable-trigger-text"></div>
						<div class="egrn-collapsable-trigger-arrow">
							<svg class="egrn-collapsable-trigger-icon"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapsable"></use></svg>
						</div>
					</div>
					<div class="egrn-collapsable-content collapse show" id="collapsable1">
						<div class="egrn-collapsable-content-inner">
							<div class="egrn-collapsable-content-title">
								обл. Московская, р-н Талдомский, СНТ "Дружба" южнее д. Пановка, уч 743
							</div>
							<div class="egrn-collapse">
								<div class="egrn-collapse-trigger egrn-collapse-trigger--light" data-toggle="collapse" data-target="#collapse1" role="button" aria-expanded="true" aria-controls="collapse1">
									<div class="egrn-collapse-trigger-text">
										<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--head egrn-collapse-trigger-text__span--dark">
											ВЫПИСКИ
										</span>
									</div>
									<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--dark"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
								</div>
								<div class="egrn-collapse-content collapse show" id="collapse1">
									<div class="egrn-collapse-content-inner">
										<div class="egrn-collapse">
											<div class="egrn-collapse-divided">
												<div class="egrn-collapse-trigger egrn-collapse-trigger--light" data-toggle="collapse" data-target="#collapse1-1" role="button" aria-expanded="true" aria-controls="collapse1-1">
													<div class="egrn-collapse-trigger-text">
														<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--dark">Электронные</span>
													</div>
													<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--dark"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
												</div>
												<div class="egrn-collapse-divided-link">
													<a class="egrn-link--pseudo" href="javascript:void(0);">
														Образцы
													</a>
												</div>
											</div>
											<div class="egrn-collapse-content collapse show" id="collapse1-1">
												<div class="egrn-collapse-content-inner">
													<ul class="egrn-checkbox-list">
														<li class="egrn-checkbox-list__item">
															<label class="egrn-checkbox">
																<input class="egrn-checkbox-input" type="checkbox" />
																<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
																<span class="egrn-checkbox-text">
																	Выписка из&nbsp;ЕГРН<br />
																	<span class="egrn-text-muted">
																		49&nbsp;₽, 5-30 мин
																	</span>
																</span>
															</label>
														</li>
														<li class="egrn-checkbox-list__item">
															<label class="egrn-checkbox">
																<input class="egrn-checkbox-input" type="checkbox" />
																<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
																<span class="egrn-checkbox-text">
																	Выписка из&nbsp;ЕГРН о&nbsp;переходе прав<br />
																	<span class="egrn-text-muted">
																		49&nbsp;₽, 5-30 мин
																	</span>
																</span>
															</label>
														</li>
														<li class="egrn-checkbox-list__item">
															<label class="egrn-checkbox">
																<input class="egrn-checkbox-input" type="checkbox" />
																<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
																<span class="egrn-checkbox-text">
																	Справка о&nbsp;кадастровой стоимости<br />
																	<span class="egrn-text-muted">
																		49&nbsp;₽, 1-3 рабочих дня
																	</span>
																</span>
															</label>
														</li>
														<li class="egrn-checkbox-list__item">
															<label class="egrn-checkbox">
																<input class="egrn-checkbox-input" type="checkbox" />
																<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
																<span class="egrn-checkbox-text">
																	<a class="egrn-link--pseudo" href="javascript:void(0);">Пакет выписок из&nbsp;ЕГРН</a> (5&nbsp;-&nbsp;1000 шт.) <span class="egrn-text-highlighted">от 10 р. за выписку</span>
																</span>
															</label>
														</li>
													</ul>
												</div>
											</div>
										</div>
										<div class="egrn-collapse">
											<div class="egrn-collapse-divided">
												<div class="egrn-collapse-trigger egrn-collapse-trigger--light" data-toggle="collapse" data-target="#collapse1-2" role="button" aria-expanded="true" aria-controls="collapse1-2">
													<div class="egrn-collapse-trigger-text">
														<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--dark">Бумажные с печатью</span>
													</div>
													<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--dark"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
												</div>
												<div class="egrn-collapse-divided-link">
													<a class="egrn-link--pseudo" href="javascript:void(0);">
														Образцы
													</a>
												</div>
											</div>
											<div class="egrn-collapse-content collapse" id="collapse1-2">
												<div class="egrn-collapse-content-inner">
													<ul class="egrn-checkbox-list">
														<li class="egrn-checkbox-list__item">
															<label class="egrn-checkbox">
																<input class="egrn-checkbox-input" type="checkbox" />
																<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
																<span class="egrn-checkbox-text">
																	Выписка из&nbsp;ЕГРН<br />
																	<span class="egrn-text-muted">
																		49&nbsp;₽, 5-30 мин
																	</span>
																</span>
															</label>
														</li>
														<li class="egrn-checkbox-list__item">
															<label class="egrn-checkbox">
																<input class="egrn-checkbox-input" type="checkbox" />
																<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
																<span class="egrn-checkbox-text">
																	Выписка из&nbsp;ЕГРН о&nbsp;переходе прав<br />
																	<span class="egrn-text-muted">
																		49&nbsp;₽, 5-30 мин
																	</span>
																</span>
															</label>
														</li>
														<li class="egrn-checkbox-list__item">
															<label class="egrn-checkbox">
																<input class="egrn-checkbox-input" type="checkbox" />
																<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
																<span class="egrn-checkbox-text">
																	Справка о&nbsp;кадастровой стоимости<br />
																	<span class="egrn-text-muted">
																		49&nbsp;₽, 1-3 рабочих дня
																	</span>
																</span>
															</label>
														</li>
														<li class="egrn-checkbox-list__item">
															<label class="egrn-checkbox">
																<input class="egrn-checkbox-input" type="checkbox" />
																<span class="egrn-checkbox-check"><span class="egrn-checkbox-check-icon"></span></span>
																<span class="egrn-checkbox-text">
																	<a class="egrn-link--pseudo" href="javascript:void(0);">Пакет выписок из&nbsp;ЕГРН</a> (5&nbsp;-&nbsp;1000 шт.) <span class="egrn-text-highlighted">от 10 р. за выписку</span>
																</span>
															</label>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="egrn-collapse">
								<div class="egrn-collapse-trigger egrn-collapse-trigger--light" data-toggle="collapse" data-target="#collapse2" role="button" aria-expanded="true" aria-controls="collapse2">
									<div class="egrn-collapse-trigger-text">
										<span class="egrn-collapse-trigger-text__span egrn-collapse-trigger-text__span--head egrn-collapse-trigger-text__span--dark">
											КАДАСТРОВЫЕ УСЛУГИ
										</span>
									</div>
									<svg class="egrn-collapse-trigger-arrow egrn-collapse-trigger-arrow--dark"><use xlink:href="#egrn-svg_sprite-icon-arrow-collapse"></use></svg>
								</div>
								<div class="egrn-collapse-content collapse" id="collapse2">
									<div class="egrn-collapse-content-inner">
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>
		<div class="demo-row">
			<div class="demo-item">
				
			</div>
		</div>



		<!-- jQuery -->
		<script src="../js/jquery-3.3.1.min.js"></script>

		<!-- Сворачиваемые блоки/области на bootstrap -->
		<script src="../js/util.js"></script>
		<script src="../js/collapse.js"></script>

		<!-- Кастомные селекты -->
		<!-- <script src="../js/customselect.js"></script> -->

		<!-- Тултипы/попапы на tether -->
		<script src="../js/tether.js"></script>
		<script src="../js/drop.js"></script>

		<!-- Скрипты -->
		<script src="../js/scripts.js?<?php include '../partials/cache-buster.txt' ?>"></script>
<?php include '../partials/footer.php' ?>