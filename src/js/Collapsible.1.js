import Name from './utils/Name';
import Clip from './Clip';
import CollapsibleTrigger from './CollapsibleTrigger';

export default function Collapsible(target, options, autoInitialize) {
	Clip.call(this, target, options, autoInitialize);
};
Object.defineProperty(Collapsible, 'name', {
	value: new Name('Collapsible', false),
});
Collapsible.prototype = Object.create(Clip.prototype);
Object.defineProperty(Collapsible, 'constructor', {
	value: Collapsible,
	enumerable: false,
	writable: true,
});

Collapsible.prototype.name = new Name('collapsible');
Collapsible.prototype.trigger = null;

Collapsible.prototype.constructionChain = function(target, options)
{
	Clip.prototype.constructionChain.call(this, target, options);
	
	this.saveInstanceOf(Collapsible);

	if (!this.hasOwnProperty('trigger')) {
		var triggerTarget = document.querySelector(target.dataset[this.name.camel + 'Trigger']);
		if (triggerTarget == null) {
			throw new Error('Элемент для создания триггера не найден (не указан data-атрибут или не найден в DOM)');
		}
		this.trigger = new CollapsibleTrigger(triggerTarget, {
			collapsible: this,
		});
	}
};

Collapsible.prototype.initializationChain = function(target)
{
	Clip.prototype.initializationChain.call(this, target);
	
	this.setDatasetIdOf(Collapsible);
};
