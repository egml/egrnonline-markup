import * as utils from './utils/utils';
// import Widget from './Widget';
import Widget from './utils/Widget';
import Name from './utils/Name';
import Collapsible from './Clip';
import CollapsibleTrigger from './CollapsibleTrigger';
import './polyfill/closest';
import './polyfill/event';

export default function DropdownSelect(target, options, autoInitialize) {
	Widget.call(this, target, options, autoInitialize);

	this.dom.select = this.dom.self.children[0];
	this.dom.options = this.dom.self.children[1];

	if (!this.dom.select || !this.dom.options) {
		throw new Error('Неверная структура DOM');
	}
	
	if (this.collapsible === null) {
		this.collapsible = new Collapsible(this.dom.options, {
			animate: true,
			transitionDuration: 400,
			// collapsedEventDetail: { dropdownSelect: this },
		});
	}
	if (this.collapsibleTrigger === null) {
		this.collapsibleTrigger = new CollapsibleTrigger(this.dom.select, {
			collapsible: this.collapsible,
			expandedEventDetail: { dropdownSelect: this },
			collapsedEventDetail: { dropdownSelect: this },
		});
	}

	this.dom.select.addEventListener(CollapsibleTrigger.prototype.expandedEventName, this.afterShowOptionsHandler);
	this.dom.select.addEventListener(CollapsibleTrigger.prototype.collapsedEventName, this.afterHideOptionsHandler);
	// this.dom.options.addEventListener(Collapsible.prototype.collapsedEventName, this.afterHideOptionsHandler);
};
DropdownSelect.prototype = Object.create(Widget.prototype);
DropdownSelect.prototype.constructor = DropdownSelect;
utils.extend(DropdownSelect.prototype, {

	name: new Name([ 'dropdown', 'select' ]),

	// перечисляем виджеты этого типа, так как придется делать их 
	// перебор в дальнейшем для скрытия попапа
	instances: [],
	collapsible: null,
	collapsibleTrigger: null,

	afterShowOptionsHandler: function(event)
	{
		event.detail.dropdownSelect.afterShowOptions();
	},

	afterShowOptions: function()
	{
		this.dom.select.classList.add(this.name.ns.css + '--active');
		document.addEventListener('click', this.clickOutsideHandler);
	},

	afterHideOptionsHandler: function(event)
	{
		event.detail.dropdownSelect.afterHideOptions();
	},

	afterHideOptions: function()
	{
		this.dom.select.classList.remove(this.name.ns.css + '--active');
		document.removeEventListener('click', this.clickOutsideHandler);
	},

	hideOptions: function()
	{
		this.collapsibleTrigger.collapse();
	},

	clickOutsideHandler: function(event)
	{
		// console.log('клик');
		var widget;
		// Перебираются все виджеты этого класса и находится тот, collapsible которого 
		// в открытом состоянии. Находится первый экземпляр, который не обязательно искомый,
		// но на практике находиться должен всегда искомый.
		for (var i = 1; i <= DropdownSelect.prototype.instances.length; i++) {
			widget = DropdownSelect.prototype.instances[i];
			if (!widget.collapsible.collapsed) {
				// console.log('экземпляр найден');
				break;
			}
			widget = null;
		}

		// В случае успешного поиска и если элемент, на который упал клик, находится 
		// вне выпадающего списка. Именно такое условие потому, что для точного нахождения
		// соответствующего виджету выпадающего списка нужен id виджета.
		// #DONE Проверка на widget.collapsible.state == 'expanded' не должна происходить,
		// точнее должна, но наблюдается неверный эффект на практике - искаженные состояния элементов.
		if (
			widget &&
			!event.target.closest('[data-' + widget.name.kebab + '-id="' + widget.id + '"] ' + widget.name.cssClassSelector('options'))
		) {
			widget.hideOptions();
			// console.log('... где-то снаружи');
		}
	},

});