import * as utils from './utils/utils';
import Widget from './utils/Widget';
import Name from './utils/Name';
import Clip from './Clip';

var name = new Name([ 'collapsible', 'trigger' ]);

export default function CollapsibleTrigger(target, options, autoInitialize) {
	Widget.call(this, target, options, autoInitialize);
};
CollapsibleTrigger.prototype = Object.create(Widget.prototype);
Object.defineProperty(CollapsibleTrigger, 'constructor', {
	value: CollapsibleTrigger,
	enumerable: false,
	writable: true,
});

CollapsibleTrigger.prototype.name = name;
CollapsibleTrigger.prototype.collapsible = null;
CollapsibleTrigger.prototype.collapsed = Clip.prototype.collapsed;
CollapsibleTrigger.prototype.multiLine = null;
CollapsibleTrigger.prototype.expandedEvent = null;
CollapsibleTrigger.prototype.expandedEventName = name.event('expanded');
CollapsibleTrigger.prototype.expandedEventDetail = null;
CollapsibleTrigger.prototype.collapsedEvent = null;
CollapsibleTrigger.prototype.collapsedEventName = name.event('collapsed');
CollapsibleTrigger.prototype.collapsedEventDetail = null;

CollapsibleTrigger.prototype.constructionChain = function(target, options)
{
	Widget.prototype.constructionChain.call(this, target, options);
	
	if (!this.collapsible) {
		throw new Error('Отсутствует привязка к Collapsible');
	}

	if (!this.hasOwnProperty('expandedEvent')) {
		this.expandedEvent = new CustomEvent(this.expandedEventName, {
			detail: utils.extend({ collapsibleTrigger: this }, this.expandedEventDetail),
		});
	}
	if (!this.hasOwnProperty('collapsedEvent')) {
		this.collapsedEvent = new CustomEvent(this.collapsedEventName, {
			detail: utils.extend({ collapsibleTrigger: this }, this.collapsedEventDetail),
		});
	}
};

CollapsibleTrigger.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);

	this.dom.extra = this.dom.self.querySelector(this.name.cssClassSelector('extra'));
	
	if (
		!this.hasOwnProperty('collapsed') && 
		this.dom.self.classList.contains(this.name.cssMod('collapsed'))
	) {
		this.collapsed = true;
	} else if (
		!this.hasOwnProperty('collapsed') && 
		this.dom.self.classList.contains(this.name.cssMod('expanded'))
	) {
		this.collapsed = false;
	} else {
		// значение по умолчанию из прототипа
		if (this.collapsed) {
			this.dom.self.classList.add(this.name.cssMod('collapsed'));
		} else {
			this.dom.self.classList.add(this.name.cssMod('expanded'));
		}
	}

	if (!this.hasOwnProperty('multiLine')) {
		this.multiLine = this.dom.self.classList.contains(this.name.cssMod('multi-line'));
	}

	if (this.collapsed) {
		this.setState('collapsed');
	} else {
		this.setState('expanded');
	}

	this.dom.self.addEventListener('click', this.toggle.bind(this));

	this.checkTitle();
};

CollapsibleTrigger.prototype.toggle = function()
{
	switch (this.state) {
		case 'collapsed':
			this.expand();
			break;
		case 'expanded':
			this.collapse();
			break;
	}
};

CollapsibleTrigger.prototype.collapse = function()
{
	this.setState('collapsed');
	if (this.collapsible) {
		this.collapsible.toggle();
	}
	this.dom.self.dispatchEvent(this.collapsedEvent);
};

CollapsibleTrigger.prototype.expand = function()
{
	this.setState('expanded');
	if (this.collapsible) {
		this.collapsible.toggle();
	}
	this.dom.self.dispatchEvent(this.expandedEvent);
};

CollapsibleTrigger.prototype.setState = function(state)
{
	// сброс
	// #CBFIX: У IE10 у classList.remove() нет возможности добавить два и более аргументов, только один
	this.dom.self.classList.remove(this.name.cssMod('collapsed'));
	this.dom.self.classList.remove(this.name.cssMod('expanded'));

	switch (state) {
		case 'collapsed':
			this.state = 'collapsed';
			this.dom.self.classList.add(this.name.cssMod('collapsed'));
			this.collapsed = true;
			break;
	
		case 'expanded':
			this.state = 'expanded';
			this.dom.self.classList.add(this.name.cssMod('expanded'));
			this.collapsed = false;
			break;
	}
};

/**
 * Проверяет ширину `[name.css]-extra`, и если она шире отведенного места,
 * выставляет title на контейнер триггера
 */
CollapsibleTrigger.prototype.checkTitle = function()
{
	if (!this.dom.extra || !this.dom.extra.hasChildNodes() || this.multiLine) return;
	var range = document.createRange();
	range.selectNode(this.dom.extra.childNodes[0]);
	var textWidth = range.getBoundingClientRect().width;
	var containerWidth = this.dom.extra.getBoundingClientRect().width;

	if (textWidth > containerWidth) {
		this.dom.self.setAttribute('title', this.dom.extra.textContent.trim());
	}
};
