import * as utils from '../utils';
import Popup from './Popup';
import PopupTrigger from './PopupTrigger';
import popupTemplate from '../../html/popup.mustache';

export var PopupManager = function() {
	this.dom = {};
	this.dom.stage = document.querySelector('.egrn-stage');
	this.popups = Popup.prototype.instances;
	this.triggers = PopupTrigger.prototype.instances;
	this.popupTemplate = popupTemplate;
};
PopupManager.prototype = {

	popups: null,
	triggers: null,
	popupTemplate: null,

	render: function(data) {
		var range = document.createRange();
		// #CBFIX Эта строка нужна в частности для Android 4.4.4 WebKit 537.36, иначе: `Uncaught NotSupportedError: Failed to execute 'createContextualFragment' on 'Range': The range's container must be an HTML element.`
		range.selectNode(document.getElementsByTagName('div').item(0));
		var domFragment = range.createContextualFragment(this.popupTemplate(data));
		this.dom.stage.appendChild(domFragment);
		return this.dom.stage.querySelector('[data-egrn-popup-id="' + data.id + '"]');
	},

	/**
	 * Запрашивает попап и назначает его этому элементу
	 */
	addTrigger: function(element, options) {
		options = utils.extend({ manager: this }, options);
		return new PopupTrigger(element, options);
	},

	deleteTrigger: function(trigger) {
		trigger.destroy();
	},

	/**
	 * Ищет незанятый попап, если такого нет, создает новый
	 */
	provide: function() {
		if (this.popups.length) {
			var ids = Object.keys(this.popups);
			for (var i = 0; i < ids.length; i++) {
				if (!this.popups[ids[i]].occupied) break;
			}
			if (i == ids.length) {
				return new Popup({ manager: this });
			} else {
				return this.popups[ids[i]];
			}
		} else {
			return new Popup({ manager: this });
		}
	},

	closeAllTriggers: function() {
		this.triggers.forEach(function(trigger) {
			if (trigger.active) {
				trigger.close();
			}
		});
	},

};

export default PopupManager;