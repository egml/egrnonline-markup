import * as utils from '../utils';
import Popup from './Popup';
import PopupTarget from './PopupTarget';

export var PopupTrigger = function(target, options, autoInitialize) {
	PopupTarget.call(this, target, options, autoInitialize);

	this.id = ++PopupTrigger.prototype.id;
	this.dom.self.dataset.egrnPopupTriggerId = this.id;
	PopupTrigger.prototype.instances[this.id] = this;

	if (this.dom.self.dataset.egrnPopupContent) {
		this.popupContent = this.dom.self.dataset.egrnPopupContent;
	}
	if (this.dom.self.dataset.egrnPopupStyle) {
		this.popupStyle = this.dom.self.dataset.egrnPopupStyle;
	}
	if (this.dom.self.dataset.egrnPopupTriggerMode) {
		this.mode = this.dom.self.dataset.egrnPopupTriggerMode;
	}
	this.setMode();
}
PopupTrigger.prototype = Object.create(PopupTarget.prototype);
utils.extend(PopupTrigger.prototype, {

	id: 0,
	manager: null,
	instances: [],
	popup: null,
	popupStyle: null,
	popupContent: null,
	mode: 'hover', // 'toggle', 'hover'
	active: false,
	hoverModeTimeout: null,
	hoverDuration: 800,

	setMode: function(mode) {
		mode = !mode ? this.mode : mode;
		switch (mode) {
			case 'toggle':
				this.dom.self.addEventListener('click', this.toggleModeHandler);
				break;
			case 'hover':
				this.dom.self.addEventListener('mouseenter', this.hoverModeTimeoutHandler);
				this.dom.self.addEventListener('click', this.hoverModeHandler);
				break;
		}
		this.mode = mode;
	},

	toggleModeHandler: function() {
		var trigger = PopupTrigger.prototype.instances[this.dataset.egrnPopupTriggerId];
		trigger.action();
	},

	// #TODO Toggle-тригер для попапа не реализован. Пока не понятно, как это сделать, потому что событие на клик вне попапа срабатывает без какой либо связи с тригером, который это событие прослушивает. Вероятно нужен какой-то глобальный реестр сработавших toggle-тригеров, который надо перебирать при срабатывании события и находить нужный тригер.
	toggleModeClickOutsideHandler: function(e) {
		if (!e.target.closest('.egrn-popup')) {
			
		}
	},
	
	hoverModeTimeoutHandler: function() {
		var trigger = PopupTrigger.prototype.instances[this.dataset.egrnPopupTriggerId];
		if (!trigger.active) {
			// console.log('hoverModeTimeoutHandler');
			trigger.dom.self.addEventListener('mouseleave', trigger.hoverModeCancelHandler);
			trigger.hoverModeTimeout = window.setTimeout(trigger.hoverModeHandler.bind(trigger.dom.self), trigger.hoverDuration);
		}
	},

	hoverModeCancelHandler: function() {
		// console.log('hoverModeCancelHandler');
		var trigger = PopupTrigger.prototype.instances[this.dataset.egrnPopupTriggerId];
		window.clearTimeout(trigger.hoverModeTimeout);
		trigger.dom.self.removeEventListener('mouseleave', trigger.hoverModeCancelHandler);
	},

	hoverModeHandler: function() {
		var trigger = PopupTrigger.prototype.instances[this.dataset.egrnPopupTriggerId];
		if (!trigger.active) {
			// console.log('hoverModeHandler');
			window.clearTimeout(trigger.hoverModeTimeout);
			trigger.dom.self.removeEventListener('mouseleave', trigger.hoverModeCancelHandler);
			trigger.action();
			trigger.dom.self.addEventListener('mouseleave', trigger.closeHandler);
			// trigger.popup.dom.self.addEventListener('mouseleave', trigger.closeHandler);
		}
	},

	closeHandler: function() {
		// var popup;
		// if (this.dataset.egrnPopupTriggerId) {
		// 	popup = PopupTrigger.prototype.instances[this.dataset.egrnPopupTriggerId].popup;
		// } else {
		// 	popup = Popup.prototype.instances[this.dataset.egrnPopupId];
		// }
		var trigger = PopupTrigger.prototype.instances[this.dataset.egrnPopupTriggerId];
		trigger.close();
	},
	
	close: function() {
		this.active = false;
		this.dom.self.removeEventListener('mouseleave', this.closeHandler);
		this.popup.hide();
		this.popup.clear();
	},

	action: function() {
		this.manager.closeAllTriggers();
		this.popup = this.manager.provide();
		this.popup.occupied = true;
		this.popup.target = this;
		this.popup.setStyle(this.popupStyle);
		this.popup.setContent(this.popupContent);
		this.popup.show();
		this.active = true;
	},

	toggle: function() {
		if (this.active) {
			this.popup.hide();
			this.active = false;
		} else {
			this.action();
		}
	},

	destroy: function() {
		switch (this.mode) {
			case 'toggle':
				this.dom.self.removeEventListener('click', this.toggleModeHandler);
				break;
			case 'hover':
				this.dom.self.removeEventListener('mouseenter', this.hoverModeTimeoutHandler);
				this.dom.self.removeEventListener('mouseleave', this.hoverModeCancelHandler);
				this.dom.self.removeEventListener('mouseleave', this.closeHandler);
				this.dom.self.removeEventListener('click', this.hoverModeHandler);
				break;
		}
		this.popup = null;
		delete this.dom.self.dataset.egrnPopupTriggerId;
		delete this.instances[this.id];
	}

});

export default PopupTrigger;