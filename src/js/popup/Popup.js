import * as utils from '../utils';
import Base from '../Base';
import popupTemplate from '../../html/popup.mustache';

export var Popup = function(options) {
	Base.call(this, null, options);

	this.id = ++Popup.prototype.id;
	Popup.prototype.instances[this.id] = this;
	this.dom.self = this.manager.render({id: this.id});

	this.dom.body = this.dom.self.querySelector('.egrn-popup-body');
	this.dom.tip = this.dom.self.querySelector('.egrn-popup-tip');

}
Popup.prototype = Object.create(Base.prototype);
utils.extend(Popup.prototype, {

	id: 0,
	state: 'hidden',
	manager: null,
	instances: [],
	occupied: false,
	target: null,
	style: 'tooltip', // 'tooltip', 'example', 'blank'

	show: function() {
		this.position();
		this.setState('visible');
	},

	hide: function() {
		this.setState('hidden');
	},

	clear: function() {
		delete this.occupied;
		delete this.style;
		delete this.target;
		this.dom.self.style.top = null;
		this.dom.self.style.left = null;
	},

	position: function() {
		var stageRect = this.manager.dom.stage.getBoundingClientRect();
		var targetRect = this.target.dom.self.getBoundingClientRect();
		var bodyRect = this.dom.body.getBoundingClientRect();
		var tipRect = this.dom.tip.getBoundingClientRect();
		var scroll = utils.pageScroll();
		var left = targetRect.left + scroll.left;
		var top = targetRect.top + scroll.top;
		var preferedLeft, preferedTop;
		var bodyLeft, bodyTop;
		// #TODO direction должен определяться исходя из положения попапа относительно границ stage
		var direction = this.target.popupDirection;
		switch (direction) {
			case 'top':
				preferedLeft = left + targetRect.width / 2;
				preferedTop = top;
				bodyLeft = - bodyRect.width / 2;
				bodyTop = - bodyRect.height - tipRect.height;
				this.dom.self.classList.add('egrn-popup--top');
				break;
			case 'left':
				preferedLeft = left;
				preferedTop = top + targetRect.height / 2;
				bodyLeft = - bodyRect.width - tipRect.height; // здесь tipRect.height потому что tip вращается с помощью transition
				bodyTop = - bodyRect.height / 2;
				this.dom.self.classList.add('egrn-popup--left');
				break;
			case 'right':
				preferedLeft = left + targetRect.width;
				preferedTop = top + targetRect.height / 2;
				bodyLeft = tipRect.height; // здесь tipRect.height потому что tip вращается с помощью transition
				bodyTop = - bodyRect.height / 2;
				this.dom.self.classList.add('egrn-popup--right');
				break;
			case 'bottom':
				preferedLeft = left + targetRect.width / 2;
				preferedTop = top + targetRect.height;
				bodyLeft = - bodyRect.width / 2;
				bodyTop = tipRect.height;
				this.dom.self.classList.add('egrn-popup--bottom');
				break;
		}

		left = preferedLeft;
		top = preferedTop;

		this.dom.body.style.left = utils.rem(bodyLeft);
		this.dom.body.style.top = utils.rem(bodyTop);
		this.dom.self.style.left = utils.rem(left);
		this.dom.self.style.top = utils.rem(top);
	},

	setStyle: function(style) {
		// this.dom.body.textContent = content;
	},

	setContent: function(content) {
		this.dom.body.textContent = content;
	},

	setState: function(state) {
		if (typeof state !== 'string') {
			state = this.state;
		}
		switch(state) {
			
			case 'hidden':
				this.dom.self.style.transition = null;
				this.dom.self.classList.remove('egrn-popup--visible');
				this.dom.self.classList.remove('egrn-popup--top');
				this.dom.self.classList.remove('egrn-popup--left');
				this.dom.self.classList.remove('egrn-popup--right');
				this.dom.self.classList.remove('egrn-popup--bottom');
				break;
			
			case 'visible':
				this.dom.self.classList.add('egrn-popup--visible');
				// #TODO При изменении положения попапа ввиду изменения положения таргета, нужно корректно добавить эффект transition
				// this.dom.self.style.transition = 'top .3s ease, left .3s ease';
				break;

		}
		this.state = state;
	},

});

export default Popup;