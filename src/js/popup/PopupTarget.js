import * as utils from '../utils';
import Base from '../Base';

export var PopupTarget = function(target, options, autoInitialize) {
	Base.call(this, target, options, autoInitialize);

	if (this.dom.self.dataset.egrnPopupDirection) {
		this.popupDirection = this.dom.self.dataset.egrnPopupDirection;
	}
}
PopupTarget.prototype = Object.create(Base.prototype);
utils.extend(PopupTarget.prototype, {

	popupDirection: 'top',

});

export default PopupTarget;