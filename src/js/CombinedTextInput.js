import * as utils from './utils/utils';
// import Widget from './Widget';
import Widget from './utils/Widget';
import Name from './utils/Name';

export default function CombinedTextInput(target, options, autoInitialize) {
	Widget.call(this, target, options, autoInitialize);
	var widget = this;
	this.dom.control = this.dom.self.querySelector(this.name.cssClassSelector('control'));
	this.dom.control.addEventListener('focus', function() {
		widget.dom.self.classList.add(this.name.ns.css + '--focus');
	});
	this.dom.control.addEventListener('blur', function() {
		widget.dom.self.classList.remove(this.name.ns.css + '--focus');
	});
};
CombinedTextInput.prototype = Object.create(Widget.prototype);
CombinedTextInput.prototype.constructor = CombinedTextInput;
utils.extend(CombinedTextInput.prototype, {

	name: new Name([ 'combined', 'text', 'input' ]),

});