import * as utils from './utils/utils';
import Name from './utils/Name';
import CollapsibleTrigger from './CollapsibleTrigger';

export default function CollapsibleAreaTrigger(target, options, autoInitialize) {
	CollapsibleTrigger.call(this, target, options, autoInitialize);
};
CollapsibleAreaTrigger.prototype = Object.create(CollapsibleTrigger.prototype);
CollapsibleAreaTrigger.prototype.constructor = CollapsibleAreaTrigger;
utils.extend(CollapsibleAreaTrigger.prototype, {

	name: new Name(['collapsible', 'area', 'trigger']),
	multiLine: null,

	initialize: function(target)
	{
		CollapsibleTrigger.prototype.initialize.call(this, target);
		
		this.dom.extra = this.dom.self.querySelector(this.name.cssClassSelector('extra'));

		if (!this.hasOwnProperty('multiLine')) {
			this.multiLine = this.dom.self.classList.contains(this.name.cssMod('multi-line'));
		}

		this.checkTitle();
	},

	/**
	 * Проверяет ширину `[name.css]-extra`, и если она шире отведенного места,
	 * выставляет title на контейнер триггера
	 */
	checkTitle: function()
	{
		if (!this.dom.extra || !this.dom.extra.hasChildNodes() || this.multiLine) return;
		var range = document.createRange();
		range.selectNode(this.dom.extra.childNodes[0]);
		var textWidth = range.getBoundingClientRect().width;
		var containerWidth = this.dom.extra.getBoundingClientRect().width;

		if (textWidth > containerWidth) {
			this.dom.self.setAttribute('title', this.dom.extra.textContent.trim());
		}
	},

});