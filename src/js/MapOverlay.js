// #NOTE: В данной реализации свойство visible опирается на класс `egrn-stage-map-overlay--visible`, при запросе этого свойства (каждый раз) проверяется наличие класса. В случае скрытия элемента, класс `egrn-stage-map-overlay--visible` сохраняется до момента полного визуального скрытия, до конца перехода (transition). Поэтому надо учитывать, что если в момент скрытия, когда элемент уже даже почти не виден, запросить свойство visible, оно выдаст true, несмотря на то, что элемента уже почти не видно.

var MapOverlay;

export default MapOverlay = {
	dom: {
		self: null,
	},
	hidingTransitionDuration: null,
	busy: null,

	initialize: function()
	{
		this.dom.self = document.querySelector('.egrn-stage-map-overlay');
		if (this.dom.self) {
			this.dom.self.addEventListener('click', this.clickHandler);
			this.busy = false;

			var cssTransitionDuration = window.getComputedStyle(this.dom.self).getPropertyValue('transition-duration');
			if (cssTransitionDuration) {
				var match = cssTransitionDuration.match('[-\+]?([0-9]+(?:\.[0-9]+)?)(s|ms)');
				if (match) {
					if (match[2] == 's') {
						this.hidingTransitionDuration = match[1] * 1000;
					} else if (match[2] == 'ms') {
						this.hidingTransitionDuration = match[1];
					}
				}
			}
		}
	},

	clickHandler: function()
	{
		MapOverlay.dom.self.removeEventListener('click', MapOverlay.clickHandler);
		MapOverlay.hide();
	},

	// /*
	//  ██   ██ ██ ██████  ███████
	//  ██   ██ ██ ██   ██ ██
	//  ███████ ██ ██   ██ █████
	//  ██   ██ ██ ██   ██ ██
	//  ██   ██ ██ ██████  ███████
	// */
	// #hide
	hide: function()
	{
		this.startHiding();
	},

	startHiding: function()
	{
		if (this.hidingTransitionDuration) {
			this.dom.self.classList.add('egrn-stage-map-overlay--hiding');
			this.busy = true;
			window.setTimeout(this.finishHidingCallback, this.hidingTransitionDuration);
		} else {
			this.finishHiding();
		}
	},

	finishHidingCallback: function()
	{
		MapOverlay.busy = false;
		MapOverlay.finishHiding();
	},

	finishHiding: function()
	{
		this.dom.self.classList.remove('egrn-stage-map-overlay--visible', 'egrn-stage-map-overlay--hiding');
	},

	// /*
	//  ███████ ██   ██  ██████  ██     ██
	//  ██      ██   ██ ██    ██ ██     ██
	//  ███████ ███████ ██    ██ ██  █  ██
	//       ██ ██   ██ ██    ██ ██ ███ ██
	//  ███████ ██   ██  ██████   ███ ███
	// */
	// #show
	show: function()
	{
		if (this.dom.self) {
			this.dom.self.addEventListener('click', this.clickHandler);
			this.dom.self.classList.add('egrn-stage-map-overlay--visible');
		}
	},
};

Object.defineProperty(MapOverlay, 'visible', {
	get: function() {
		return this.dom.self.classList.contains('egrn-stage-map-overlay--visible');
	},
});