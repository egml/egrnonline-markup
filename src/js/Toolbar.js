import * as utils from './utils/utils';
import Widget from './utils/Widget';
import Name from './utils/Name';
import SearchTool from './SearchTool';
import Tool from './Tool';
import './polyfill/event';

var name = new Name('toolbar');

export default function Toolbar(target, options, autoInitialize) {
	Widget.call(this, target, options, autoInitialize);
};
Toolbar.prototype = Object.create(Widget.prototype);

Toolbar.prototype.name = name;
Toolbar.prototype.state = 'collapsed';
Toolbar.prototype.expanded = false;
Toolbar.prototype.expandDuration = 500;
Toolbar.prototype.expandedWidth = null;
Toolbar.prototype.expandEventName = name.event('expand');
Toolbar.prototype.expandedEventName = name.event('expanded');
Toolbar.prototype.collapseEventName = name.event('collapse');
Toolbar.prototype.collapsedEventName = name.event('collapsed');
Toolbar.prototype.expandEvent = null;
Toolbar.prototype.expandedEvent = null;
Toolbar.prototype.collapseEvent = null;
Toolbar.prototype.collapsedEvent = null;
Toolbar.prototype.expandEventDetail = null;
Toolbar.prototype.expandedEventDetail = null;
Toolbar.prototype.collapseEventDetail = null;
Toolbar.prototype.collapsedEventDetail = null;

Object.defineProperty(Toolbar.prototype, 'collapsed', {
 	get: function() {
		return !this.expanded;
	},
	enumerable: true,
});

Toolbar.prototype.constructionChain = function(target, options)
{
	Widget.prototype.constructionChain.call(this, target, options);
	
	if (!this.hasOwnProperty('expandEvent')) {
		this.expandEvent = new CustomEvent(this.expandEventName, {
			detail: utils.extend({ widget: this }, this.expandEventDetail),
		});
	}
	if (!this.hasOwnProperty('expandedEvent')) {
		this.expandedEvent = new CustomEvent(this.expandedEventName, {
			detail: utils.extend({ widget: this }, this.expandedEventDetail),
		});
	}
	if (!this.hasOwnProperty('collapseEvent')) {
		this.collapseEvent = new CustomEvent(this.collapseEventName, {
			detail: utils.extend({ widget: this }, this.collapseEventDetail),
		});
	}
	if (!this.hasOwnProperty('collapsedEvent')) {
		this.collapsedEvent = new CustomEvent(this.collapsedEventName, {
			detail: utils.extend({ widget: this }, this.collapsedEventDetail),
		});
	}

	var tools = target.children;
	if (!tools.length) {
		throw new Error('У контейнера тулбара должны быть потомки');
	}

	// Создание инструментов
	this.tools = [];
	for (var i = 0, tool; i < tools.length; i++) {
		if (tools[i].classList.contains('egrn-search_tool')) {
			tool = new SearchTool(tools[i], { toolbar: this }, false);
		} else {
			tool = new Tool(tools[i], { toolbar: this }, false);
		}
		// tool = new Tool(tools[i], { toolbar: this }, false);
		this.tools[tool.id.tool] = tool;
	}
};

Toolbar.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);

	// Определение начального состояния
	if (this.dom.self.classList.contains(this.name.cssMod('expanded'))) {
		this.state = 'expanded';
	}
	if (this.dom.self.classList.contains(this.name.cssMod('collapsed'))) {
		this.state = 'collapsed';
	}
	if (this.dom.self.classList.contains(this.name.cssMod('expand'))) {
		this.dom.self.classList.remove(this.name.cssMod('expand'));
		this.state = 'expanding';
	}
	if (this.dom.self.classList.contains(this.name.cssMod('collapse'))) {
		this.dom.self.classList.remove(this.name.cssMod('collapse'));
		this.state = 'collapsing';
	}

	if (this.tools.length == 0) {
		this.dom.self.classList.add(this.name.cssMod('empty'));
	}

	this.tools.forEach(function(tool) {
		tool.initialize();
	});

	// Установка ширины раскрытия тулбара, чтобы было видно все тайтлы.
	// Это для работы транзишенов.
	this.getWidth();
	
	this.setState();
	// this.dom.self.classList.replace(this.name.cssMod('not-ready'), this.name.cssMod('ready'));
	this.ready = true;
};

Toolbar.prototype.getWidth = function(width)
{
	if (typeof width !== 'number') {
		if (this.expandedWidth == null) {
			var widths = [];
			this.tools.forEach(function(tool) {
				widths.push(tool.titleWidth());
			});
			width = Math.max.apply(null, widths);
		}
		width += this.tools[Object.keys(this.tools)[0]].iconWidth();
	}
	this.expandedWidth = width;
};

Toolbar.prototype.setState = function(state)
{
	if (typeof state !== 'string') {
		state = this.state;
	}
	var toolbar = this;
	switch (state) {

		case 'expanding':
			this.dom.self.dispatchEvent(this.expandEvent);
			this.state = 'expanding';
			this.busy = true;
			this.dom.self.classList.remove(this.name.cssMod('expanded'));
			this.dom.self.classList.add(this.name.cssMod('expanding'));
			this.dom.self.classList.remove(this.name.cssMod('collapsed'));
			this.dom.self.classList.remove(this.name.cssMod('collapsing'));
			this.dom.self.style.width = utils.rem(this.expandedWidth);
			this.dom.self.style.transition = 'width ' + this.expandDuration / 1000 + 's cubic-bezier(.5, 0, .5, 1)';
			window.setTimeout(function() {
				toolbar.dom.self.style.transition = null;
				toolbar.busy = false;
				toolbar.setState('expanded');
			}, this.expandDuration);

			break;
		
		case 'collapsing':
			this.dom.self.dispatchEvent(this.collapseEvent);
			this.state = 'collapsing';
			this.busy = true;
			this.expanded = false;
			this.dom.self.classList.remove(this.name.cssMod('expanded'));
			this.dom.self.classList.remove(this.name.cssMod('expanding'));
			this.dom.self.classList.remove(this.name.cssMod('collapsed'));
			this.dom.self.classList.add(this.name.cssMod('collapsing'));
			this.dom.self.style.width = null;
			this.dom.self.style.transition = 'width ' + this.expandDuration / 1000 + 's cubic-bezier(.5, 0, .5, 1)';
			window.setTimeout(function() {
				toolbar.dom.self.style.transition = null;
				toolbar.busy = false;
				toolbar.setState('collapsed');
			}, this.expandDuration);

			break;
		
		case 'expanded':
			this.dom.self.style.width = utils.rem(this.expandedWidth);
			this.dom.self.classList.add(this.name.cssMod('expanded'));
			this.dom.self.classList.remove(this.name.cssMod('expanding'));
			this.dom.self.classList.remove(this.name.cssMod('collapsed'));
			this.dom.self.classList.remove(this.name.cssMod('collapsing'));
			this.expanded = true;
			this.state = 'expanded';
			this.dom.self.dispatchEvent(this.expandedEvent);

			break;

		case 'collapsed':
			this.dom.self.style.width = null;
			this.dom.self.classList.remove(this.name.cssMod('expanded'));
			this.dom.self.classList.remove(this.name.cssMod('expanding'));
			this.dom.self.classList.add(this.name.cssMod('collapsed'));
			this.dom.self.classList.remove(this.name.cssMod('collapsing'));
			this.expanded = false;
			this.state = 'collapsed';
			this.dom.self.dispatchEvent(this.collapsedEvent);

			break;
	}
};

Toolbar.prototype.expand = function()
{
	if (this.collapsed && !this.busy) {
		this.setState('expanding');
	}
};
	
Toolbar.prototype.collapse = function()
{
	if (this.expanded && !this.busy) {
		this.setState('collapsing');
	}
};