// #CBFIX: WebView 33.0.0.0 (Android 4.4 KitKat) сильно не нравится, если в конце параметра функции `console.log()` оставить запятую и следующий параметр не указать, например, `console.log(jsp, jsp.getContentPositionY(),);`. Это просто напоминалка, чтобы иметь в виду.

import * as utils from './utils/utils';
// import Widget from './Widget';
import Widget from './utils/Widget';
import Name from './utils/Name';
import $ from 'jquery';
import 'jscrollpane';
import 'jscrollpane/script/jquery.mousewheel';
import 'jscrollpane/style/jquery.jscrollpane.css';

export default function Scrollable(target, options, autoInitialize) {
	Widget.call(this, target, options, autoInitialize);

	var $scroll = $(target);
	
	var scrollable = this;
	// $scroll.bind('jsp-initialised', function(event, isScrollable) {
	// 	console.log($(scrollable.dom.self).data('jsp'));
	// 	scrollable.initialize();
	// });
	// $scroll.bind('jsp-arrow-change', function(event, isAtTop, isAtBottom, isAtLeft, isAtRight) {
	// 	if (isAtTop) {
	// 		console.log('top');
	// 	}
	// 	if (isAtBottom) {
	// 		console.log('bottom');
	// 	}
	// });

	if (this.shadows === null) {
		this.shadows = this.dom.self.classList.contains('egrn-scrollable--shadows');
	}

	$scroll.bind('jsp-user-scroll-y', function(event, destTop, isAtTop, isAtBottom) {
		scrollable.checkShadows();
	});

	$scroll.jScrollPane({
		// showArrows: true,
		verticalGutter: 0,
		// horizontalGutter: 50,
	});

	// initialize():
	this.jsp = $scroll.data('jsp');
	this.dom.container = this.dom.self.querySelector('.jspContainer');
	this.dom.pane = this.dom.self.querySelector('.jspPane');
	this.dom.bar = this.dom.self.querySelector('.jspVerticalBar');
	this.checkShadows();

	// console.log(
	// 	jsp, 
	// 	jsp.getContentPositionY(),
	// 	jsp.getPercentScrolledY() 
	// );
	// window.jsp = this.jsp;

	this.dom.self.dispatchEvent(this.readyEvent);
};
Scrollable.prototype = Object.create(Widget.prototype);
Scrollable.prototype.constructor = Scrollable;
utils.extend(Scrollable.prototype, {

	name: new Name('scrollable'),

	/**
	 * Использовать ли тени для индикации, что область имеет прокрутку
	 */
	shadows: false,

	/**
	 * Объект jScrollPane, инициализированный на DOM-элементе (переменная target)
	 */
	jsp: null,

	checkShadows: function() {
		// console.log('checkShadows()');
		if (!this.shadows) return;
		var scrolled = this.jsp.getPercentScrolledY();
		// #CBFIX: В хроме почему-то значение scrolled не достигает 1, а остается наподобие 0.999995324393574, поэтому scrolled переведен в значение с большим порядком и округлением
		scrolled = Math.round(scrolled * 100);
		// console.log(scrolled);
		// if (scrolled > 0) {
		// 	this.dom.container.classList.add('egrn-scrollable-shadow-top');
		// } else {
		// 	this.dom.container.classList.remove('egrn-scrollable-shadow-top');
		// }
		if (scrolled < 100) {
			this.dom.container.classList.add('egrn-scrollable-shadow-bottom');
		} else {
			this.dom.container.classList.remove('egrn-scrollable-shadow-bottom');
		}
	}

});