import * as utils from './utils/utils';
import Widget from './utils/Widget';
import Name from './utils/Name';

var name = new Name('tool');

export default function Tool(target, options, autoInitialize) {
	Widget.call(this, target, options, autoInitialize);
};
Object.defineProperty(Tool, 'name', {
	value: new Name('Tool', false),
});
Tool.prototype = Object.create(Widget.prototype);
Object.defineProperty(Tool, 'constructor', {
	value: Tool,
	enumerable: false,
	writable: true,
});

Tool.prototype.name = name;
Tool.prototype.toolbar = null; // Ссылка на родительскую панель инструментов
Tool.prototype.active = false;

Tool.prototype.constructionChain = function(target, options)
{
	Widget.prototype.constructionChain.call(this, target, options);
	
	this.saveInstanceOf(Tool);
};

Tool.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);

	this.setDatasetIdOf(Tool);

	this.dom.icon = this.dom.self.querySelector(name.cssClassSelector('icon'));
	this.dom.ui = this.dom.self.querySelector(name.cssClassSelector('ui'));
	this.dom.title = this.dom.self.querySelector(name.cssClassSelector('title-text'));
	
	this.dom.self.addEventListener('click', this.activateListener);
	
	if (this.toolbar.collapsed) {
		this.setTooltip();
	}
	var tool = this;
	this.toolbar.dom.self.addEventListener(this.toolbar.collapsedEventName, function() {
		tool.setTooltip();
	});
	this.toolbar.dom.self.addEventListener(this.toolbar.expandedEventName, function() {
		tool.unsetTooltip();
	});
};

Tool.prototype.action = function() {
	console.log('Событие по клику');
	this.deactivate();
};

// Вызывается на DOM-элементе, поэтому this ссылается на DOM-элемент (O.O)
Tool.prototype.activateListener = function() {
	Tool.prototype.instanceFromDataset(this, Tool).activate();
};

Tool.prototype.activate = function(preactivated) {
	if (!preactivated) {
		this.dom.self.removeEventListener('click', this.activateListener);
		this.dom.self.classList.add('egrn-tool--active');
	}
	this.active = true;
	this.action();
};

Tool.prototype.deactivate = function() {
	this.dom.self.classList.remove('egrn-tool--active');
	this.dom.self.addEventListener('click', this.activateListener);
	this.active = false;
};

Tool.prototype.titleWidth = function() {
	return this.dom.title.getBoundingClientRect().width;
};

Tool.prototype.iconWidth = function() {
	return this.dom.icon.getBoundingClientRect().width;
};

Tool.prototype.setTooltip = function() {
	// var pm = new PopupManager();
	// var trigger = pm.addTrigger(this.dom.self, {
	// 	popupContent: this.dom.title.textContent,
	// 	popupDirection: 'right',
	// 	mode: 'hover',
	// });
	this.dom.self.setAttribute('title', this.dom.title.textContent);
};
	
Tool.prototype.unsetTooltip = function() {
	// console.log('unsetTooltip', this);
	// var pm = new PopupManager();
	// var trigger = PopupTrigger.prototype.instances[this.dom.self.dataset.egrnPopupTriggerId];
	// if (trigger) {
	// 	pm.deleteTrigger(trigger);
	// }
	this.dom.self.removeAttribute('title');
};