// #TODO Неадекватность анимации при последовательном сворачивании вложенных областей: сначала сворачивается вложенная, затем, не дожидаясь сворачивания вложенной, сворачивается родительская область - наблюдается неадекватность.

// #TODO Также есть неадекватность при сорачивании области, если фокус был на элементе внутри области (сворачивание по клику) - содержимое области пропадает сразу же без перехода в прозрачность.

// #CBFIX Либо баг в Gecko, либо опять Google кладет на спецификацию (хотя в IE11 тоже не наблюдается), поведение происходит при сбрасывании транзишена: если он указан для нескольких параметров и один из параметров сбрасывается через инлайновые стили, а другой нет, в таком случае событие transitionend произойдет для сбрасываемого параметра раньше времени, т.к. будет соответствовать таймингу первого, не сбрасываемого, параметра. Приведенное в файле решение также перестает действовать в Gecko, если тайминг-функции у параметров разные, а эффект срабатывает в таком случае даже быстрее.

// #CBFIX В IE/Edge встречается странное поведение содержимого контейнера, который меняет свою высоту с `overflow: hidden` с помощью транзишенов. Если параллельно менять еще и opacity содержимого, то содержимое пропадает до момента полного раскрытия контейнера. Пример эффекта здесь: http://haveagoodday.ru/var/test/collapsible-ie-problem/

import * as utils from './utils/utils';
import Widget from './utils/Widget';
import Name from './utils/Name';
import './polyfill/event';

var name = new Name('clip');

export default function Clip(target, options, autoInitialize) {
	Widget.call(this, target, options, autoInitialize);
};
Object.defineProperty(Clip, 'name', {
	value: new Name('Clip', false),
});
Clip.prototype = Object.create(Widget.prototype);
Object.defineProperty(Clip.prototype, 'constructor', {
    value: Clip,
    enumerable: false,
    writable: true,
});

Clip.prototype.name = name;
Clip.prototype.collapsed = false;
Clip.prototype.animate = true;
Clip.prototype.interruptable = true;
Clip.prototype.durationLimit = 0.6; // в секундах (см. `getDuration`)
Clip.prototype.expandedEvent = null;
Clip.prototype.expandedEventName = name.event('expanded');
Clip.prototype.expandedEventDetail = null;
Clip.prototype.collapsedEvent = null;
Clip.prototype.collapsedEventName = name.event('collapsed');
Clip.prototype.collapsedEventDetail = null;

Clip.prototype.constructionChain = function(target, options)
{
	Widget.prototype.constructionChain.call(this, target, options);

	this.saveInstanceOf(Clip);

	if (!this.hasOwnProperty('expandedEvent')) {
		this.expandedEvent = new CustomEvent(this.expandedEventName, {
			detail: utils.extend({ collapsible: this }, this.expandedEventDetail),
		});
	}
	if (!this.hasOwnProperty('collapsedEvent')) {
		this.collapsedEvent = new CustomEvent(this.collapsedEventName, {
			detail: utils.extend({ collapsible: this }, this.collapsedEventDetail),
		});
	}
};

Clip.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);

	this.setDatasetIdOf(Clip);

	this.dom.content = this.dom.self.children[0];

	// элемент для стилей
	var styleId = this.name.css + '-' + this.id + '-style';
	this.dom.style = document.getElementById(styleId);
	if (this.dom.style == null) {
		this.dom.style = document.createElement('style');
		this.dom.style.setAttribute('id', styleId);
		this.dom.self.parentNode.insertBefore(this.dom.style, this.dom.self);
	}

	if (!this.hasOwnProperty('animate')) {
		if (this.dom.self.classList.contains(this.name.cssMod('animated'))) {
			this.animate = true;
		}
	}

	if (!this.hasOwnProperty('interruptable')) {
		if (this.dom.self.classList.contains(this.name.cssMod('interruptable'))) {
			this.interruptable = true;
		}
	}

	if (
		!this.hasOwnProperty('collapsed') && 
		this.dom.self.classList.contains(this.name.cssMod('collapsed'))
	) {
		this.state = 'collapsed';
		this.collapsed = true;
	} else if (
		!this.hasOwnProperty('collapsed') && 
		this.dom.self.classList.contains(this.name.cssMod('expanded'))
	) {
		this.state = 'expanded';
		this.collapsed = false;
	} else {
		// значение по умолчанию из прототипа
		if (this.collapsed) {
			this.dom.self.classList.add(this.name.cssMod('collapsed'));
			this.state = 'collapsed';
		} else {
			this.dom.self.classList.add(this.name.cssMod('expanded'));
			this.state = 'expanded';
		}
	}
};

Clip.prototype.collapse = function()
{
	if (this.animate) {
		if (this.busy) {
			if (this.interruptable) {
				this.dom.self.classList.replace(this.name.cssMod('expanding'), this.name.cssMod('collapsing'));
				this.state = 'collapsing';
			}
		} else {
			if (!this.collapsed) {
				setStyle.call(this);
				this.dom.self.addEventListener('transitionend', transitionEndListener, false);
				this.dom.self.addEventListener('scroll', scrollListener, false);
				this.dom.self.classList.replace(this.name.cssMod('expanded'), this.name.cssMod('collapsing'));
				this.state = 'collapsing';
				this.busy = true;
			}
		}
	} else {
		this.dom.self.classList.replace(this.name.cssMod('expanded'), this.name.cssMod('collapsed'));
		this.state = 'collapsed';
		this.collapsed = true;
		this.dom.self.dispatchEvent(this.collapsedEvent);
	}
};

Clip.prototype.expand = function()
{
	if (this.animate) {
		if (this.busy) {
			if (this.interruptable) {
				this.dom.self.classList.replace(this.name.cssMod('collapsing'), this.name.cssMod('expanding'));
				this.state = 'expanding';
			}
		} else {
			if (this.collapsed) {
				setStyle.call(this);
				this.dom.self.addEventListener('transitionend', transitionEndListener, false);
				this.dom.self.addEventListener('scroll', scrollListener, false);
				this.dom.self.classList.replace(this.name.cssMod('collapsed'), this.name.cssMod('expanding'));
				this.state = 'expanding';
				this.busy = true;
			}
		}
	} else {
		this.dom.self.classList.replace(this.name.cssMod('collapsed'), this.name.cssMod('expanded'));
		this.state = 'expanded';
		this.collapsed = false;
		this.dom.self.dispatchEvent(this.expandedEvent);
	}
};

function transitionEndListener(event)
{
	event.stopPropagation(); // чтобы не вызывался на родительских областях
	if (event.propertyName == 'height') {
		var that = Clip.prototype.instanceFromDataset(this, Clip);
		if (that.dom.self.classList.contains(that.name.cssMod('collapsing'))) {
			that.dom.self.classList.replace(that.name.cssMod('collapsing'), that.name.cssMod('collapsed'));
			that.collapsed = true;
			that.state = 'collapsed';
			that.dom.self.dispatchEvent(that.collapsedEvent);
		} else {
			that.dom.self.classList.replace(that.name.cssMod('expanding'), that.name.cssMod('expanded'));
			that.collapsed = false;
			that.state = 'expanded';
			that.dom.self.dispatchEvent(that.expandedEvent);
		}
		clearStyle.call(that);
		that.dom.self.removeEventListener('transitionend', transitionEndListener);
		that.dom.self.removeEventListener('scroll', scrollListener);
		that.busy = false;
	}
};

Clip.prototype.toggle = function()
{
	switch(this.state) {
		case 'collapsed':
		case 'collapsing':
			this.expand();
			break;

		case 'expanded':
		case 'expanding':
			this.collapse();
			break;
	}
};

Clip.prototype.getHeight = function()
{
	return this.dom.content.getBoundingClientRect().height;
};

Clip.prototype.getDuration = function(length)
{
	length = length || this.getHeight();
	return Math.tanh(length / 200) * this.durationLimit; // Вычисляется по функции гиперболического тангенса (да, сам в шоке) с поправками (http://yotx.ru/#!1/3_h/sH@2cHB/tGDOF/bf9of3PrYH/rYN@IIfyv7ZP5G3vbZwe7@wf7JBp2Y@eU8Xi6xXjcurzY3d/aBwU=)
};

function setStyle()
{
	var height = this.getHeight();
	var duration = this.getDuration(height);
	this.dom.style.textContent = 
		'[data-' + this.name.kebab + '-id="' + this.id + '"].' + this.name.css + ' {\n' +
			'\theight: ' + utils.rem(height) + ';\n' + 
			'\ttransition-duration: ' + duration + 's;\n' + 
		'}\n' +
		'[data-' + this.name.kebab + '-id="' + this.id + '"] > .' + this.name.css + '-content {\n' +
			'\ttransition-duration: ' + duration + 's;\n' + 
		'}\n'
	;
	this.dom.self.getBoundingClientRect(); // reflow
};

function clearStyle()
{
	this.dom.style.textContent = null;
	this.dom.self.getBoundingClientRect(); // reflow
};

function scrollListener(event)
{
	Clip.prototype.instanceFromDataset(this, Clip).dom.self.scrollTop = 0;
};

// Clip.prototype.expand = function()
// {
// 	// эта проверка нужна на случай вызова двух последовательных expand в обход toggle
// 	if (this.state == 'expanded' || this.state == 'expanding') return;
// 	if (this.animate) {
// 		if (this.busy && !this.interruptable) return;
// 		this.setState('expanding');
// 	} else {
// 		this.setState('expanded');
// 	}
// };

// Clip.prototype.collapse = function()
// {
// 	// эта проверка нужна на случай вызова двух последовательных collapse в обход toggle
// 	if (this.state == 'collapsed' || this.state == 'collapsing') return;
// 	if (this.animate) {
// 		if (this.busy && !this.interruptable) return;
// 		this.setState('collapsing');
// 	} else {
// 		this.setState('collapsed');
// 	}
// };

// Clip.prototype.animationEnd = function()
// {
// 	var self = Clip.prototype.instances[this.dataset[Clip.prototype.name.camel + 'Id']];
// 	if (self.debug) console.log('Событие transitionend');
// 	self.busy = false;
// 	if (self.state == 'expanding') {
// 		self.setState('expanded');
// 	}
// 	if (self.state == 'collapsing') {
// 		self.setState('collapsed');
// 	}
// 	self.dom.self.removeEventListener('transitionend', self.animationEnd);
// 	self.dom.self.removeEventListener('scroll', self.preventContentScroll);
// 	self.dom.self.classList.remove(self.name.css + '--transitioning');
// 	if (this.transitionDuration !== null) {
// 		self.dom.self.style.transitionDuration = null;
// 	}
// };

// /**
//  * Предотвращает прокручивание еще не до конца показанного контента, если в момент показа
//  * происходит фокус на одном из принимающих фокус элементов внутри контента.
//  */
// Clip.prototype.preventContentScroll = function()
// {
// 	var self = Clip.prototype.instances[this.dataset[Clip.prototype.name.camel + 'Id']];
// 	if (self.debug) console.log('Событие scroll на контенте');
// 	// #CBFIX scrollTo() на элементе почему-то работает в Gecko и Blink, но не работает в IE/Edge, что впрочем указано в документации, что для прокрутки элементов стоит использовать именно scrollTop, а не scrollTo() https://developer.mozilla.org/en-US/docs/Web/API/window/scrollTo
// 	// self.dom.self.scrollTo(0, 0);
// 	self.dom.self.scrollTop = 0;
// };

// Clip.prototype.setStateCollapsing = function()
// {
// 	if (this.debug) console.log('collapsing <- ' + this.state);
// 	if (this.state == 'expanded') {
// 		this.clearState();
// 		// Следующие две строчки должны присутствовать именно здесь, чтобы наблюдалось поведение, описанное в комментарии в начале файла
// 		// #CBFIX Почему-то при применении решения для Gecko также убрался баг в iOS 8.4, при котором область некорректно сворачивалась. То есть это либо из-за применения opacity, либо из-за другого положения следующий двух строк:
// 		// this.dom.self.classList.add(this.name.cssMod('transitioning'));
// 		// this.dom.self.addEventListener('transitionend', this.animationEnd);
// 		this.busy = true;
// 		this.dom.self.style.height = utils.rem(this.dom.content.getBoundingClientRect().height);
// 		this.dom.self.style.opacity = 1; // это и все последующие явные задания opacity добавлены для исправления поведения, описанного в комментарии в начале файла
// 		this.dom.self.getBoundingClientRect(); // reflow
// 		this.dom.self.classList.add(this.name.cssMod('transitioning'));
// 		if (this.transitionDuration !== null) {
// 			this.dom.self.style.transitionDuration = this.transitionDuration + 'ms';
// 		}
// 		this.dom.self.addEventListener('transitionend', this.animationEnd);
// 		this.dom.self.addEventListener('scroll', this.preventContentScroll);
// 	}
// 	if (this.state == 'expanding') {
// 		this.dom.self.classList.remove(this.name.cssMod('expanding'));
// 	}
// 	this.state = 'collapsing';
// 	this.dom.self.classList.add(this.name.cssMod('collapsing'));
// 	this.dom.self.style.height = 0;
// 	this.dom.self.style.opacity = 0;
// };

// Clip.prototype.clearStateCollapsing = function()
// {
// 	this.dom.self.style.height = null;
// 	this.dom.self.style.opacity = null;
// 	this.dom.self.classList.remove(this.name.cssMod('collapsing'));
// };

// Clip.prototype.setStateExpanding = function()
// {
// 	if (this.debug) console.log('expanding <- ' + this.state);
// 	if (this.state == 'collapsed') {
// 		this.clearState();
// 		// this.dom.self.classList.add(this.name.cssMod('transitioning'));
// 		this.busy = true;
// 		this.dom.self.style.height = 0;
// 		this.dom.self.style.opacity = 0;
// 		// здесь почему-то reflow не требуется
// 		// this.dom.self.getBoundingClientRect(); // reflow
// 		this.dom.self.classList.add(this.name.cssMod('transitioning'));
// 		if (this.transitionDuration !== null) {
// 			this.dom.self.style.transitionDuration = this.transitionDuration + 'ms';
// 		}
// 		this.dom.self.addEventListener('transitionend', this.animationEnd);
// 		this.dom.self.addEventListener('scroll', this.preventContentScroll);
// 	}
// 	if (this.state == 'collapsing') {
// 		this.dom.self.classList.remove(this.name.cssMod('collapsing'));
// 	}
// 	this.state = 'expanding';
// 	this.dom.self.classList.add(this.name.cssMod('expanding'));
// 	this.dom.self.style.height = utils.rem(this.dom.content.getBoundingClientRect().height);
// 	this.dom.self.style.opacity = 1;
// };

// Clip.prototype.clearStateExpanding = function()
// {
// 	this.dom.self.style.height = null;
// 	this.dom.self.style.opacity = null;
// 	this.dom.self.classList.remove(this.name.cssMod('expanding'));
// };

// Clip.prototype.setStateCollapsed = function()
// {
// 	if (this.debug) console.log('collapsed <- ' + this.state);
// 	this.clearState();
// 	this.state = 'collapsed';
// 	this.dom.self.classList.add(this.name.cssMod('collapsed'));
// 	this.collapsed = true;
// 	this.dom.self.dispatchEvent(this.collapsedEvent);
// };

// Clip.prototype.clearStateCollapsed = function()
// {
// 	this.dom.self.classList.remove(this.name.cssMod('collapsed'));
// };

// Clip.prototype.setStateExpanded = function()
// {
// 	if (this.debug) console.log('expanded <- ' + this.state);
// 	this.clearState();
// 	this.state = 'expanded';
// 	this.dom.self.classList.add(this.name.cssMod('expanded'));
// 	this.collapsed = false;
// 	this.dom.self.dispatchEvent(this.expandedEvent);
// };

// Clip.prototype.clearStateExpanded = function()
// {
// 	this.dom.self.classList.remove(this.name.cssMod('expanded'));
// };
