// /*
//  ██████  ███████  ██████  ██    ██ ███████ ███████ ████████
//  ██   ██ ██      ██    ██ ██    ██ ██      ██         ██
//  ██████  █████   ██    ██ ██    ██ █████   ███████    ██
//  ██   ██ ██      ██ ▄▄ ██ ██    ██ ██           ██    ██
//  ██   ██ ███████  ██████   ██████  ███████ ███████    ██
//                      ▀▀
// */
// #request
export function request(options) {
	var xhr = new XMLHttpRequest();
	options.type = options.type || 'get';
	xhr.open(options.type, options.url);
	// Yii проверяет наличие хедера X-Requested-With в фильтре ajaxOnly
	// http://www.yiiframework.com/doc/api/1.1/CHttpRequest#getIsAjaxRequest-detail
	xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');
	xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded; charset=UTF-8');
	xhr.onload = options.success;
	xhr.send(options.query);
};

// /*
//  ██ ███    ███ ██████   ██████  ██████  ████████
//  ██ ████  ████ ██   ██ ██    ██ ██   ██    ██
//  ██ ██ ████ ██ ██████  ██    ██ ██████     ██
//  ██ ██  ██  ██ ██      ██    ██ ██   ██    ██
//  ██ ██      ██ ██       ██████  ██   ██    ██
// 
//  ███████ ██    ██  ██████
//  ██      ██    ██ ██
//  ███████ ██    ██ ██   ███
//       ██  ██  ██  ██    ██
//  ███████   ████    ██████
// */
// #import #svg
export function importSvg(url, loadEventName, target, idAttribute) {
	importSVG(url, loadEventName, target, idAttribute);
}

export function importSVG(url, loadEventName, target, idAttribute) {
	loadEventName = loadEventName || 'svgload';
	target = target || document.body;
	if (typeof DOMParser !== 'undefined') // IE6-
	request({
		url: url,
		success: function() {
			// Для IE7-9, т.к. они не поддерживают встроенный парсинг свойства responseXML
			// http://msdn.microsoft.com/en-us/library/ie/ms535874%28v=vs.85%29.aspx
			var parser = new DOMParser();
			var xml = parser.parseFromString(this.responseText, 'text/xml');
			// var xml = this.responseXML;
			var svg = document.importNode(xml.documentElement, true);
			if (idAttribute) {
				svg.setAttribute('id', idAttribute);
			}
			target.appendChild(svg);
			target.dispatchEvent(new CustomEvent(loadEventName, { bubbles: true }));
		},
	});
};

// /*
//  ███████ ██   ██ ████████ ███████ ███    ██ ██████
//  ██       ██ ██     ██    ██      ████   ██ ██   ██
//  █████     ███      ██    █████   ██ ██  ██ ██   ██
//  ██       ██ ██     ██    ██      ██  ██ ██ ██   ██
//  ███████ ██   ██    ██    ███████ ██   ████ ██████
// */
// #extend
export function extend(dest, source) {
	for (var prop in source) {
		dest[prop] = source[prop];
	}
	return dest;
}

// /*
//   ██████ ██   ██ ██ ██      ██████
//  ██      ██   ██ ██ ██      ██   ██
//  ██      ███████ ██ ██      ██   ██
//  ██      ██   ██ ██ ██      ██   ██
//   ██████ ██   ██ ██ ███████ ██████
// 
//  ██████  ██    ██
//  ██   ██  ██  ██
//  ██████    ████
//  ██   ██    ██
//  ██████     ██
// 
//   ██████ ██       █████  ███████ ███████
//  ██      ██      ██   ██ ██      ██
//  ██      ██      ███████ ███████ ███████
//  ██      ██      ██   ██      ██      ██
//   ██████ ███████ ██   ██ ███████ ███████
// */
// #child #class
export function childByClass(parent, className) {
	var children = parent.children;
	for (var i = 0; i < children.length; i++) {
		if (children[i].classList.contains(className)) {
			return children[i];
		}
	}
}

// /*
//  ██████   █████   ██████  ███████
//  ██   ██ ██   ██ ██       ██
//  ██████  ███████ ██   ███ █████
//  ██      ██   ██ ██    ██ ██
//  ██      ██   ██  ██████  ███████
// 
//  ███████  ██████ ██████   ██████  ██      ██
//  ██      ██      ██   ██ ██    ██ ██      ██
//  ███████ ██      ██████  ██    ██ ██      ██
//       ██ ██      ██   ██ ██    ██ ██      ██
//  ███████  ██████ ██   ██  ██████  ███████ ███████
// */
// #page #scroll
// Кросс-браузерное определение величины скролла
export function pageScroll() {
	if (window.pageXOffset != undefined) {
		return {
			left: pageXOffset,
			top: pageYOffset
		};
	} else {
		var html = document.documentElement;
		var body = document.body;

		var top = html.scrollTop || body && body.scrollTop || 0;
		top -= html.clientTop;

		var left = html.scrollLeft || body && body.scrollLeft || 0;
		left -= html.clientLeft;

		return {
			left: left,
			top: top
		};
	}
}

// /*
//   ██████ ██       ██████  ███████ ███████ ███████ ████████
//  ██      ██      ██    ██ ██      ██      ██         ██
//  ██      ██      ██    ██ ███████ █████   ███████    ██
//  ██      ██      ██    ██      ██ ██           ██    ██
//   ██████ ███████  ██████  ███████ ███████ ███████    ██
// 
//  ██████   █████  ██████  ███████ ███    ██ ████████
//  ██   ██ ██   ██ ██   ██ ██      ████   ██    ██
//  ██████  ███████ ██████  █████   ██ ██  ██    ██
//  ██      ██   ██ ██   ██ ██      ██  ██ ██    ██
//  ██      ██   ██ ██   ██ ███████ ██   ████    ██
// 
//  ██████  ██    ██
//  ██   ██  ██  ██
//  ██████    ████
//  ██   ██    ██
//  ██████     ██
// 
//   ██████ ██       █████  ███████ ███████
//  ██      ██      ██   ██ ██      ██
//  ██      ██      ███████ ███████ ███████
//  ██      ██      ██   ██      ██      ██
//   ██████ ███████ ██   ██ ███████ ███████
// */
// #closest #parent #class
export function closestParentByClass(element, className) {
	var parent = element.parentNode;
	while (!parent.classList.contains(className)) {
		parent = parent.parentNode;
	}
	return parent;
}

// /*
//  ██ ███████
//  ██ ██
//  ██ ███████
//  ██      ██
//  ██ ███████
// 
//   ██████  ██████       ██ ███████  ██████ ████████
//  ██    ██ ██   ██      ██ ██      ██         ██
//  ██    ██ ██████       ██ █████   ██         ██
//  ██    ██ ██   ██ ██   ██ ██      ██         ██
//   ██████  ██████   █████  ███████  ██████    ██
// */
// #is #object
// Сперто из interact.js (сперто немного, но все равно: http://interactjs.io/)
export function isObject(thing) {
	return !!thing && (typeof thing === 'object');
}

// /*
//  ██████  ███████ ███    ███
//  ██   ██ ██      ████  ████
//  ██████  █████   ██ ████ ██
//  ██   ██ ██      ██  ██  ██
//  ██   ██ ███████ ██      ██
// */
// #rem
// Подсчёт величины в rem. На входе пиксели без `px`, на выходе величина в rem с `rem`
var rootFontSize;
export function rem(pxNoUnits, recalc) {
	if (typeof rootFontSize != 'number' || recalc) {
		// #CBFIX: Edge (42.17134.1.0, EdgeHTML 17.17134) и IE определяют значение как, например, 9.93 в результате >> операция дает 9, поэтому тут дополнительно надо округлять. UPD: Зачем переводить в integer с помощью >>, если мы уже применям к строке Math.round, тем самым она автоматически кастуется в integer.
		// rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0,-2)) >> 0;
		rootFontSize = Math.round(window.getComputedStyle(document.documentElement).getPropertyValue('font-size').slice(0,-2));
	}
	return pxNoUnits/rootFontSize + 'rem';
}

// /*
//   ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████
//  ██      ██    ██ ████   ██    ██    ██      ████   ██    ██
//  ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██
//  ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██
//   ██████  ██████  ██   ████    ██    ███████ ██   ████    ██
// 
//  ██████   ██████  ██   ██
//  ██   ██ ██    ██  ██ ██
//  ██████  ██    ██   ███
//  ██   ██ ██    ██  ██ ██
//  ██████   ██████  ██   ██
// 
//  ██   ██ ███████ ██  ██████  ██   ██ ████████
//  ██   ██ ██      ██ ██       ██   ██    ██
//  ███████ █████   ██ ██   ███ ███████    ██
//  ██   ██ ██      ██ ██    ██ ██   ██    ██
//  ██   ██ ███████ ██  ██████  ██   ██    ██
// */
// #content #box #height
// Подсчёт высоты content-box с дробью (в отличие от $(...).height())
export function contentBoxHeight(element) {
	if (element instanceof jQuery) element = element.get(0);
	var style = window.getComputedStyle(element);
	return element.getBoundingClientRect().height
			- style.getPropertyValue('padding-top').slice(0,-2)
			- style.getPropertyValue('padding-bottom').slice(0,-2)
			- style.getPropertyValue('border-top-width').slice(0,-2)
			- style.getPropertyValue('border-bottom-width').slice(0,-2);
}

// /*
//  ███    ███  █████  ██████   ██████  ██ ███    ██
//  ████  ████ ██   ██ ██   ██ ██       ██ ████   ██
//  ██ ████ ██ ███████ ██████  ██   ███ ██ ██ ██  ██
//  ██  ██  ██ ██   ██ ██   ██ ██    ██ ██ ██  ██ ██
//  ██      ██ ██   ██ ██   ██  ██████  ██ ██   ████
// 
//  ██████   ██████  ██   ██
//  ██   ██ ██    ██  ██ ██
//  ██████  ██    ██   ███
//  ██   ██ ██    ██  ██ ██
//  ██████   ██████  ██   ██
// 
//  ██   ██ ███████ ██  ██████  ██   ██ ████████
//  ██   ██ ██      ██ ██       ██   ██    ██
//  ███████ █████   ██ ██   ███ ███████    ██
//  ██   ██ ██      ██ ██    ██ ██   ██    ██
//  ██   ██ ███████ ██  ██████  ██   ██    ██
// */
// #margin #box #height
export function marginBoxHeight(element, sourceWindow) {
	sourceWindow = sourceWindow || window;
	var style;
	if (element && (style = sourceWindow.getComputedStyle(element))) {
		return element.getBoundingClientRect().height
				+ (style.getPropertyValue('margin-top').slice(0,-2) >> 0)
				+ (style.getPropertyValue('margin-bottom').slice(0,-2) >> 0);
	} else {
		return 0;
	}
}

// /*
//   ██████ ██████  ███████  █████  ████████ ███████
//  ██      ██   ██ ██      ██   ██    ██    ██
//  ██      ██████  █████   ███████    ██    █████
//  ██      ██   ██ ██      ██   ██    ██    ██
//   ██████ ██   ██ ███████ ██   ██    ██    ███████
// 
//  ██     ██ ██ ██████   ██████  ███████ ████████ ███████
//  ██     ██ ██ ██   ██ ██       ██         ██    ██
//  ██  █  ██ ██ ██   ██ ██   ███ █████      ██    ███████
//  ██ ███ ██ ██ ██   ██ ██    ██ ██         ██         ██
//   ███ ███  ██ ██████   ██████  ███████    ██    ███████
// */
// #create #widgets
/**
 * Создает объекты виджета в определенном контексте.
 * 
 * @param {function|class} constructor Конструктор виджета
 * @param {DOMElement} context В контексте (внутри) какого элемента производить поиск 
 * элементов для инициализации виджета. Если не указан, используется document.
 * @param {object} options Конфигурационный объект, передаваемый создаваемому виджету
 * @param {string} selector Какой селектор использовать для поиска элементов. 
 * Если не указать, используется name.css из прототипа виджета.
 */
export function createWidgets(constructor, context, options, selector)
{
	if (typeof constructor == 'undefined') {
		throw new Error('Нужно обязательно указать класс виджета');
	}
	context = context || document;
	selector = selector || '.' + constructor.prototype.name.css;

	var elements = context.querySelectorAll(selector);
	if (elements.length) {
		for (var i = 0; i < elements.length; i++) {
			// Почему-то try...catch выдает куда меньше полезной информации, чем простая обработка ошибок в консоли браузера
			// try {
				new constructor(elements[i], options);
			// } catch(error) {
			// 	console.error(error);
			// }
		}
	} else {
		throw new Error('Элементы в DOM в выбранном контексте по такому селектору не найдены');
	}
}

// /*
//  ███████ ████████ ██████  ██ ███    ██  ██████
//  ██         ██    ██   ██ ██ ████   ██ ██
//  ███████    ██    ██████  ██ ██ ██  ██ ██   ███
//       ██    ██    ██   ██ ██ ██  ██ ██ ██    ██
//  ███████    ██    ██   ██ ██ ██   ████  ██████
// 
//   ██████  █████  ███████ ███████
//  ██      ██   ██ ██      ██
//  ██      ███████ ███████ █████
//  ██      ██   ██      ██ ██
//   ██████ ██   ██ ███████ ███████
// */
// #string #case
export function stringCase(bits, type) {
	if (!bits || !bits.length) {
		return '';
	}
	if (typeof bits == 'string') {
		bits = [ bits ];
	}
	var string =  '';
	switch (type) {
		case 'camel':
			bits.forEach(function(bit, i) {
				if (i == 0) {
					string += bit.firstLowerCase();
				} else {
					string += bit.firstUpperCase();
				}
			});
			break;

		case 'capitalCamel':
			bits.forEach(function(bit) {
				string += bit.firstUpperCase();
			});
			break;

		case 'flat':
			string = bits.join('');
			break;

		case 'snake':
			string = bits.join('_');
			break;

		case 'kebab':
			string = bits.join('-');
			break;

		case 'dot':
			string = bits.join('.');
			break;
	
		case 'asIs':
		default:
			string = bits.join('');
			break;
	}
	return string;
}

// /*
//  ███████ ██ ██████  ███████ ████████
//  ██      ██ ██   ██ ██         ██
//  █████   ██ ██████  ███████    ██
//  ██      ██ ██   ██      ██    ██
//  ██      ██ ██   ██ ███████    ██
// 
//  ██    ██ ██████  ██████  ███████ ██████      ██
//  ██    ██ ██   ██ ██   ██ ██      ██   ██    ██
//  ██    ██ ██████  ██████  █████   ██████    ██
//  ██    ██ ██      ██      ██      ██   ██  ██
//   ██████  ██      ██      ███████ ██   ██ ██
// 
//  ██       ██████  ██     ██ ███████ ██████
//  ██      ██    ██ ██     ██ ██      ██   ██
//  ██      ██    ██ ██  █  ██ █████   ██████
//  ██      ██    ██ ██ ███ ██ ██      ██   ██
//  ███████  ██████   ███ ███  ███████ ██   ██
// 
//   ██████  █████  ███████ ███████
//  ██      ██   ██ ██      ██
//  ██      ███████ ███████ █████
//  ██      ██   ██      ██ ██
//   ██████ ██   ██ ███████ ███████
// */
// #first #upper #lower #case
String.prototype.firstUpperCase = function() {
	return this[0].toUpperCase() + this.slice(1);
}
String.prototype.firstLowerCase = function() {
	return this[0].toLowerCase() + this.slice(1);
}