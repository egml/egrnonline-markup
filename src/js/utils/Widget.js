import * as utils from './utils';
import Name from './Name';
import Id from './Id';

/**
 * Базовый класс для виджетов
 * @param {HTMLElement} target Элемент DOM, на котором создается виджет
 * @param {Object} options Параметры для экземпляра класса (перезаписывает значения по умолчанию)
 * @param {Boolean} autoInitialize Проводить ли инициализацию сразу после конструирования объекта. По 
 * умолчанию - да (true)
 */
export default function Widget(target, options, autoInitialize) {
	this.construct(target, options, autoInitialize);
};

/**
 * Переопределение свойства `name` конструктора нужно при сохранении экзепляра класса в прототипе
 * (см. метод `saveInstanceOf`)
 */
Object.defineProperty(Widget, 'name', {
	value: new Name('Widget', false),
});

Widget.prototype.name = new Name('widget');
Widget.prototype.debug = false;
Widget.prototype.busy = false;
Widget.prototype.initialized = false;

/**
 * Конструирование объекта.
 * Этот метод не задуман для переопределения или расширения (для этого см. `constructionChain`).
 * @param {HTMLElement} target Элемент DOM, на котором создается виджет (иногда он нужен в конструкторе)
 * @param {Object} options Параметры для экземпляра класса (перезаписывает значения по умолчанию)
 * @param {Boolean} autoInitialize Проводить ли инициализацию сразу после конструирования объекта. По 
 * умолчанию - да (true)
 */
Widget.prototype.construct = function(target, options, autoInitialize)
{
	this.constructionChain(target, options);
	if (typeof autoInitialize == 'undefined' || autoInitialize == true) {
		this.initialize(target);
	}
};

/**
 * Конструирование объекта (только относящееся к этом объекту + вызов цепочки). Тут только тот код, 
 * который нельзя назвать инициализацией (который не должен исполняться повторно). Этот метод задуман 
 * для расширения или переопределения дочерними классами.
 * @param {HTMLElement} target Элемент DOM, на котором создается виджет (иногда он нужен в конструкторе)
 * @param {Object} options Параметры для экземпляра класса (перезаписывает значения по умолчанию)
 */
Widget.prototype.constructionChain = function(target, options)
{
	this.dom = {
		self: target,
	};
	this.id = new Id;
	this.setOptions(options);
	this.saveInstanceOf(Widget);
};

/**
 * Инициализация экзепляра класса: привязка к DOM, назначение прослушки событий и т.п.
 * Этот метод не задуман для переопределения или расширения (для этого см. `initializationChain`)
 * @param {HTMLElement|null} target Элемент DOM, на котором создается виджет
 */
Widget.prototype.initialize = function(target)
{
	if (typeof target == 'undefined') {
		target = this.dom.self; // При отложенной инициализации
	}
	this.initializationChain(target);
	this.initialized = true;
};

/**
 * Инициализация экзепляра класса (только относящаяся к этому объекту + вызов цепочки): 
 * привязка к DOM, назначение прослушки событий и т.п. Этот метод задуман для расширения 
 * или переопределения дочерними классами.
 * @param {HTMLElement} target Элемент DOM, на котором создается виджет
 */
Widget.prototype.initializationChain = function(target)
{
	this.dom.self = target;
};

Widget.prototype.setOptions = function(options)
{
	if (utils.isObject(options)) {
		utils.extend(this, options);
	}
};

// /*
//  ███████  █████  ██    ██ ███████
//  ██      ██   ██ ██    ██ ██
//  ███████ ███████ ██    ██ █████
//       ██ ██   ██  ██  ██  ██
//  ███████ ██   ██   ████   ███████
// 
//  ██ ███    ██ ███████ ████████  █████  ███    ██  ██████ ███████
//  ██ ████   ██ ██         ██    ██   ██ ████   ██ ██      ██
//  ██ ██ ██  ██ ███████    ██    ███████ ██ ██  ██ ██      █████
//  ██ ██  ██ ██      ██    ██    ██   ██ ██  ██ ██ ██      ██
//  ██ ██   ████ ███████    ██    ██   ██ ██   ████  ██████ ███████
// 
//   ██████  ███████
//  ██    ██ ██
//  ██    ██ █████
//  ██    ██ ██
//   ██████  ██
// */
// #save #instance #of
/**
 * Сохранение экземпляра класса в регистре экземпляров класса этого типа
 * @param {Function} constructor Конструктор класса, в который происходит сохранение экземпляра
 */
Widget.prototype.saveInstanceOf = function(constructor)
{
	constructor = constructor || this.constructor;
	if (!constructor.prototype.hasOwnProperty('instances')) {
		(constructor.prototype.instances = []).length++; // потому что мы хотим считать id с 1, а не с 0
		// #TODO: Переделать на что-то более корректное, ведь при таком подходе с массивом его length будет выдавать результат не единицу больший, чем в действительности есть сохраненных экзепляров. Возможно лучше использовать Set или Map.
	}
	this.id.current = constructor.prototype.instances.length;
	this.id[constructor.name.camel] = this.id >> 0; // скастовать в строку, а затем в целое число
	constructor.prototype.instances[this.id] = this;
};

// /*
//  ███████ ███████ ████████
//  ██      ██         ██
//  ███████ █████      ██
//       ██ ██         ██
//  ███████ ███████    ██
// 
//  ██████   █████  ████████  █████  ███████ ███████ ████████
//  ██   ██ ██   ██    ██    ██   ██ ██      ██         ██
//  ██   ██ ███████    ██    ███████ ███████ █████      ██
//  ██   ██ ██   ██    ██    ██   ██      ██ ██         ██
//  ██████  ██   ██    ██    ██   ██ ███████ ███████    ██
// 
//  ██ ██████   ██████  ███████
//  ██ ██   ██ ██    ██ ██
//  ██ ██   ██ ██    ██ █████
//  ██ ██   ██ ██    ██ ██
//  ██ ██████   ██████  ██
// */
// #set #dataset #id #of
Widget.prototype.setDatasetIdOf = function(constructor)
{
	constructor = constructor || this.constructor;
	var id = this.id[constructor.name.camel];
	var name = constructor.prototype.name.camel + 'Id';
	this.dom.self.dataset[name] = id;
};

// /*
//  ██ ███    ██ ███████ ████████  █████  ███    ██  ██████ ███████
//  ██ ████   ██ ██         ██    ██   ██ ████   ██ ██      ██
//  ██ ██ ██  ██ ███████    ██    ███████ ██ ██  ██ ██      █████
//  ██ ██  ██ ██      ██    ██    ██   ██ ██  ██ ██ ██      ██
//  ██ ██   ████ ███████    ██    ██   ██ ██   ████  ██████ ███████
// 
//  ███████ ██████   ██████  ███    ███
//  ██      ██   ██ ██    ██ ████  ████
//  █████   ██████  ██    ██ ██ ████ ██
//  ██      ██   ██ ██    ██ ██  ██  ██
//  ██      ██   ██  ██████  ██      ██
// 
//  ██████   █████  ████████  █████  ███████ ███████ ████████
//  ██   ██ ██   ██    ██    ██   ██ ██      ██         ██
//  ██   ██ ███████    ██    ███████ ███████ █████      ██
//  ██   ██ ██   ██    ██    ██   ██      ██ ██         ██
//  ██████  ██   ██    ██    ██   ██ ███████ ███████    ██
// */
// #instance #from #dataset
Widget.prototype.instanceFromDataset = function(element, constructor)
{
	return constructor.prototype.instances[element.dataset[constructor.prototype.name.camel + 'Id']];
};

Widget.prototype.setState = function(state)
{
	var methodName = 'setState' + state.firstUpperCase();
	if (typeof this[methodName] == 'function') {
		this[methodName]();
	}
};

Widget.prototype.clearState = function(state)
{
	if (state == 'all') {
		// обход всех
		for (var property in this) {
			if (property.match(/^clearState(?!All).+/)) {
				this[property]();
			}
		}
	} else {
		if (!state) {
			if (!this.state) return;
			state = this.state;
		}
		var methodName = 'clearState' + state.firstUpperCase();
		if (typeof this[methodName] == 'function') {
			this[methodName]();
		}
	}
};