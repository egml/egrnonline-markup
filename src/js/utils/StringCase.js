import * as utils from './utils';

export default function StringCase(bits) {
	this.bits = bits.concat([]);

	if (bits instanceof Array) {
		this.asIs = this.bits.join('');
		bits.forEach(function(bit, i) {
			bits[i] = bit.toLowerCase();
		});
	} else {
		this.asIs = this.bits;
		bits = bits.toLowerCase();
	}

	this.camel = utils.stringCase(bits, 'camel');
	this.capitalCamel = utils.stringCase(bits, 'capitalCamel');
	this.kebab = utils.stringCase(bits, 'kebab');
	this.snake = utils.stringCase(bits, 'snake');
	this.flat = utils.stringCase(bits, 'flat');
	this.dot = utils.stringCase(bits, 'dot');
}
StringCase.prototype.toString = function() {
	return this.asIs;
};