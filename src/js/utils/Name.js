import * as utils from './utils';
import StringCase from './StringCase';

export default function Name(nameBits, nsBits) {
	var bits;

	// Далее идет обработка параметра nsBits:
	// - если он не задан, пропустить все и взять ns из прототипа
	// - если он boolean и true, то также пропустить и использовать ns 
	//   из прототипа
	// - если он boolean и false, то nsBits - пустая строка
	// - и если в результате nsBits строка или массив, то создать новое 
	//   свойство ns поверх геттера прототипа со значением nsBits
	if (nsBits != null) {
		if (typeof nsBits == 'boolean' && nsBits == false) {
			nsBits = '';
		}
		if (typeof nsBits == 'string' || nsBits instanceof Array) {
			Object.defineProperty(this, 'ns', {
				value: new StringCase(nsBits),
				writable: true,
				configurable: true,
				enumerable: true,
			});
		}
	}

	var hasNs = this.ns && this.ns.toString() != '';

	this.base = new StringCase(nameBits);

	// as is
	if (hasNs) {
		this.asIs = this.ns.asIs.concat(this.base.asIs);
	} else {
		this.asIs = this.base.asIs;
	}

	// camel, capitalCamel, kebab, flat
	var types = ['camel', 'capitalCamel', 'kebab', 'flat'];
	for (var i = 0; i < types.length; i++) {
		bits = [ this.base[types[i]] ];
		if (hasNs) {
			bits.unshift(this.ns[types[i]]);
		}
		this[types[i]] = utils.stringCase(bits, types[i]);
	}

	// css
	bits = [ this.base.snake ];
	if (hasNs) {
		bits.unshift(this.ns.snake);
	}
	this.css = utils.stringCase(bits, 'kebab');

	// event
	this.event = function(eventName) {
		var bits = [ this.base.camel ];
		if (hasNs) {
			bits.unshift(this.ns.camel);
		}
		bits.push(eventName);
		return utils.stringCase(bits, 'dot');
	};

	// cssClass
	this.cssClass = function(appendage) {
		if (appendage != null) {
			return utils.stringCase([ this.css, appendage ], 'kebab');
		} else {
			return this.css;
		}
	};

	// cssModificator
	this.cssModificator = function(modificatorName) {
		if (modificatorName != null) {
			return this.css + '--' + modificatorName;
		} else {
			return this.css;
		}
	};
	this.cssMod = function(modName) {
		return this.cssModificator(modName);
	};

	// cssClassSelector
	this.cssClassSelector = function(appendage) {
		return '.' + this.cssClass(appendage);
	};
}
var ns;
Object.defineProperty(Name.prototype, 'ns', {
	set: function(value) {
		ns = new StringCase(value);
	},
	get: function() {
		return ns;
	},
});
Name.prototype.toString = function() {
	return this.asIs;
};