import * as utils from './utils/utils';
import * as Leaflet from 'leaflet/dist/leaflet-src.esm';
import 'leaflet/dist/leaflet.css'; // <style> в <head>
import Widget from './utils/Widget';
import Name from './utils/Name';
import './leaflet/RosreestrTileLayer';

var L = Leaflet; // Иначе ругается, если делать import * as L. Так почему-то работает.

export default function Map(target, options, autoInitialize) {
	Widget.call(this, target, options, autoInitialize);
}
Map.prototype = Object.create(Widget.prototype);
Object.defineProperty(Map, 'constructor', {
	value: Map,
	enumerable: false,
	writable: true,
});
	
Map.prototype.name = new Name('map');
Map.prototype.leafletMap = null;
	
Map.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);
	
	this.leafletMap = L.map(this.dom.self, {
		center: [ 55.755814, 37.61763 ],
		zoom: 10,
		zoomAnimation: false,
		zoomControl: false,
		scrollWheelZoom: false
	});
	L.control.attribution({ position: 'bottomleft' }).addTo(this.leafletMap);

	// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);
	// // oks
	// L.tileLayer.Rosreestr('http://{s}pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/Cadastre/MapServer/export?dpi=96&transparent=true&format=png32&bbox={bbox}&size=1024%2C1024&bboxSR=102100&imageSR=102100&f=image&layers=show%3A29%2C30%2C31%2C32%2C34%2C38%2C33%2C35%2C39', {
	// 	tileSize: 1024,
	// 	clickable: true,
	// 	attribution: 'Данные <a href="http://pkk5.rosreestr.ru">ПКК Росреестра</a>',
	// 	zIndex: 500,
	// 	maxZoom: 19
	// }).addTo(this.map);
	// // kadastr
	// L.tileLayer.Rosreestr('http://{s}pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/Cadastre/MapServer/export?dpi=96&transparent=true&format=png32&bbox={bbox}&size=1024%2C1024&bboxSR=102100&imageSR=102100&f=image&layers=show%3A0%2C1%2C2%2C4%2C5%2C6%2C7%2C8%2C9%2C10%2C11%2C12%2C13%2C14%2C15%2C16%2C17%2C18%2C19%2C20', {
	// 	tileSize: 1024,
	// 	clickable: true,
	// 	attribution: 'Данные <a href="http://pkk5.rosreestr.ru">ПКК Росреестра</a>',
	// 	zIndex: 500,
	// 	maxZoom: 19
	// }).addTo(this.map);

	L.tileLayer('http://haveagoodday.ru/dev/egrnonline/markup/demo/assets/map-tile.png').addTo(this.leafletMap);
	L.tileLayer('http://haveagoodday.ru/dev/egrnonline/markup/demo/assets/export.png', {
		tileSize: 1024,
		clickable: true,
		attribution: 'Данные <a href="http://pkk5.rosreestr.ru">ПКК Росреестра</a>',
		zIndex: 500,
		maxZoom: 19
	}).addTo(this.leafletMap);

	this.leafletMap.on('click', function() {
		console.log('Клик по карте');
	});
};