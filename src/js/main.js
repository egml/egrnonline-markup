import './config';
import * as utils from './utils/utils';
import './polyfill/event';

// Виджеты
import Tool from './Tool';
import Toolbar from './Toolbar';
import Scrollable from './Scrollable';
import Clip from './Clip';
import Collapsible from './Collapsible';
import CombinedTextInput from './CombinedTextInput';
import DropdownSelect from './DropdownSelect';
import Map from './Map';

// Синглтоны
import Stage from './Stage';

// -------------------------------------------
//  Виджеты
// -------------------------------------------
export { Tool };
export { Toolbar };
export { Scrollable };
export { Clip };
export { Collapsible };
export { CombinedTextInput };
export { DropdownSelect };
export { Map };

// -------------------------------------------
//  Cинглтоны
// -------------------------------------------
export { Stage as stage };

// -------------------------------------------
//  Утилиты
// -------------------------------------------
export { utils };

// -------------------------------------------
//  Для более простой интеграции в Drupal
// -------------------------------------------
export var options = {
	assetsUrl: '',
	cacheBuster: '',
};
export function importSvgSprite(noApp) {
	utils.importSvg(
		options.assetsUrl + '/img/egrn-svg_sprite.svg?' + options.cacheBuster, 
		'egrn.svg_sprite.load', 
		noApp ? null : document.querySelector('.egrn-app')
	);
}

// -------------------------------------------
//  Карта
// -------------------------------------------
// import map from './map';

// Карта инициализируется сразу, не дожидаясь события "load"
// document.addEventListener('DOMContentLoaded', function() {
// 	var mapElement = document.getElementById('egrn-map');
// 	if (mapElement != null) {
// 		map();
// 	}
// });

// -------------------------------------------
//  Событие `egrn.app.load`
// -------------------------------------------
// Будет вызвано событие `egrn.app.load` в момент, когда произойдет последнее 
// из следующих двух событий:
// - Стандартное `load` на `window`
// или
// - Событие `egrn.svg_sprite.load`, означающее загрузку и внедрение в DOM
//   SVG-спрайта
// Это делается для того, чтобы избежать отображения пустых мест там, где
// должны быть элементы спрайта, до момента, когда он подгрузился и внедрился
window.addEventListener('load', ready);
document.addEventListener('egrn.svg_sprite.load', ready);

// Эта функция вызывает событие `egrn.app.load` в случае, если два события - 
// `load` или `egrn.svg_sprite.load` - уже произошли
function ready() {
	if (
		document.readyState == 'complete' &&
		document.querySelector('#egrn-svg_sprite') != null
	) {
		document.dispatchEvent(new CustomEvent('egrn.app.load'));
	}
}

// -------------------------------------------
//  `.egrn-app--loaded`
// -------------------------------------------
document.addEventListener('egrn.app.load', function() {
	var app = document.querySelector('.egrn-app');
	if (app != null) {
		app.classList.replace('egrn-app--not-loaded', 'egrn-app--loaded');
	}
});

// // -------------------------------------------
// //  DOMLoaded
// // -------------------------------------------
// document.addEventListener('DOMContentLoaded', function() {

// 	// // -------------------------------------------
// 	// //  Карта
// 	// // -------------------------------------------
// 	// // Карта инициализируется сразу, не дожидаясь события "load"
// 	// var mapElement = document.getElementById('egrn-map');
// 	// if (mapElement != null) {
// 	// 	map();
// 	// }

// });

// // -------------------------------------------
// //  Load
// // -------------------------------------------
// // #CBFIX Старые андроиды (Android Browser 4.4.4 (Webkit 537.36) и почему-то iOS Safari 10.3 (8.4, 9.3, 11.3 - ок)), из-за подгрузки шрифтов, выдают некорректные размеры элементов, если выполнять код до загрузки всех ресурсов. Поэтому используется событие load на window.
// window.addEventListener('load', function() {

// });