import $ from 'jquery';
// import ymaps from 'ymaps';
import * as utils from './utils/utils';
import * as Leaflet from 'leaflet/dist/leaflet-src.esm';
// import 'leaflet/dist/leaflet-src';
import 'leaflet/dist/leaflet.css'; // <style> в <head>
// import '../../var/dop';
// import '../../var/L.Control.Zoomslider';
// import '../../var/L.Control.Zoomslider.css';
// import '../../var/leaflet.draw';
// import '../../var/leaflet.draw.css';
// import '../../var/MarkerCluster.Default.css';
// import '../../var/leaflet.label';
// import '../../var/leaflet.measurecontrol';
// import '../../var/leaflet-measure.min';

// -------------------------------------------
//  Разные заметки
// -------------------------------------------
// ОКС - Объект каптиального строительства
// ЕГИП - Единое Геоинформационное Пространство
// 
// 
// Пример ответа от http://pkk5.rosreestr.ru/api/features/5:
// {
// features: [
// 	{
// 		attrs: Object { 
// 			address: "Москва г, ул Краснопрудная, д 3/5, строен 6", 
// 			cn: "77:01:0003036:1070", 
// 			id: "77:1:3036:1070"
// 		}
// 		center: Object { 
// 			x: 4192027.9389663655, 
// 			y: 7514826.195839435 
// 		}
// 		extent: Object { 
// 			xmax: 4192101.3693434587,
// 			xmin: 4191954.957843829,
// 			ymax: 7514945.388240929,
// 			ymin: 7514712.254072403
// 		}
// 		sort: 7701000303601070
// 		type: 5
// 	}
// 	...
// ]
// }
//
// 
// Пример дальнейшего ответа (например, от http://pkk5.rosreestr.ru/api/features/5/77:9:5006:1043):
// feature: {…}
// 	attrs: {…}
// 		adate: "20.12.2017"
// 		address: "Москва г, ш Хорошёвское, д 82, корп 2"
// 		anno_text: "1043"
// 		area_type: "002"
// 		area_unit: "055"
// 		area_value: 3343.4
// 		cad_cost: 517989150.52
// 		cad_eng_data: Object { actual_date: "Wed, 20 Dec 2017 00:00:00 GMT", co_name: "ГУП МосгорБТИ", lastmodified: "Wed, 20 Dec 2017 20:23:07 GMT", … }
// 		cad_record_date: "20.12.2017"
// 		cad_unit: "383"
// 		cn: "77:09:0005006:1043"
// 		date_cost: "01.01.2016"
// 		date_create: "27.05.2012"
// 		elements_constuct: Array [ {…} ]
// 		floors: "5"
// 		fp: null
// 		id: "77:9:5006:1043"
// 		kvartal: "77:9:5006"
// 		kvartal_cn: "77:09:0005006"
// 		name: null
// 		okrug: "77"
// 		okrug_cn: "77"
// 		oks_type: "building"
// 		pubdate: "21.12.2017"
// 		purpose: "204002000000"
// 		rayon: "77:9"
// 		rayon_cn: "77:09"
// 		reg: 177
// 		rights_reg: 0
// 		statecd: "01"
// 		underground_floors: "1"
// 		year_built: "1959"
// 		year_used: "1959"
// 	center: {…}
// 		x: 4176727.3781396174
// 		y: 7514632.46539486
// 	extent: {…}
// 		xmax: 4176757.95027975
// 		xmin: 4176696.813179338
// 		ymax: 7514697.556923461
// 		ymin: 7514567.3768206155
// 	stat: Object { пустой }
// 	type: 5
// 
// 
// 


var L = utils.extend({}, Leaflet);

export default function() {
	// ymaps.load('https://api-maps.yandex.ru/2.1/?lang=ru_RU').then(function() { ... });

	// function AjaxLoader(type, url, object, AjaxResponseHandler) {
	// 	$.ajax({
	// 		type: type,
	// 		url: url,
	// 		data: object,
	// 		success: function (data) {
	// 			AjaxResponseHandler(data);
	// 		}
	// 	});
	// }
	// $.fn.live = function (event, callback) {
	// 	$(document).on(event, $(this).selector, callback);
	// }

	// var marker = "",mezhaGroup = "", ind = "", editableLayers = new L.FeatureGroup(), seeArea = 0, sessMezha = true;

	var map = L.map('egrn-map', { 
		center: [ 55.755814, 37.61763 ], 
		zoom: 10, 
		zoomAnimation: false, 
		zoomControl: false
	});

	// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
	L.tileLayer('http://localhost/egrnonline/markup/tmp/map-tile.png').addTo(map);

	// map.locate({ setView: true });

	// L.control.scale({
	// 			imperial: false
	// }).addTo(map);

	// map.addLayer(editableLayers);

	// var rosreestr_oks = L.tileLayer.Rosreestr('http://{s}pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/Cadastre/MapServer/export?dpi=96&transparent=true&format=png32&bbox={bbox}&size=1024%2C1024&bboxSR=102100&imageSR=102100&f=image&layers=show%3A29%2C30%2C31%2C32%2C34%2C38%2C33%2C35%2C39',
	// 	{
	// 		tileSize: 1024,
	// 		clickable: true,
	// 		attribution: 'Данные ПКК Росреестра',
	// 		zIndex: 500,
	// 		maxZoom: 19
	// 	}
	// );
	// var rosreestr_parcel = L.tileLayer.Rosreestr(
	// 	'http://{s}pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/Cadastre/MapServer/export?dpi=96&transparent=true&format=png32&bbox={bbox}&size=1024%2C1024&bboxSR=102100&imageSR=102100&f=image&layers=show%3A23%2C24',
	// 	{
	// 		tileSize: 1024,
	// 		clickable: true,
	// 		attribution: 'Данные ПКК Росреестра',
	// 		zIndex: 500,
	// 		maxZoom: 19
	// 	}
	// );

	L.rosreestrTileLayer = function(url, options) {
		var TileLayer = L.TileLayer.extend({
			options: {
				tileSize: 1024
			},

			getTileUrl: function(tilePoint) {
				var map = this._map,
					crs = map.options.crs,
					tileSize = this.options.tileSize,
					nwPoint = tilePoint.multiplyBy(tileSize),
					sePoint = nwPoint.add([tileSize, tileSize]);

				var nw = crs.project(map.unproject(nwPoint, tilePoint.z)),
					se = crs.project(map.unproject(sePoint, tilePoint.z)),
					bbox = [nw.x, se.y, se.x, nw.y].join(',');
				//console.log(bbox);
				return L.Util.template(this._url, L.extend({
					s: this._getSubdomain(tilePoint),
					bbox: bbox
				}, this.options));
			}
		});

		if (options.clickable) {
			TileLayer = TileLayer.extend({
				onAdd: function(map) {
					L.TileLayer.prototype.onAdd.call(this, map);
					if (this.options.clickable) {
						L.DomUtil.addClass(this._container, 'leaflet-clickable-raster-layer');
						if (this._needInitInteraction) {
							this._initInteraction();
							this._needInitInteraction = false;
						}
					}
				},

				_needInitInteraction: true,

				_initInteraction: function() {
					// console.log(this);
					// var div = this._container,
					// 	events = [
					// 		'dblclick',
					// 		'click',
					// 		'mousedown',
					// 		// 'mouseover',
					// 		// 'mouseout',
					// 		// 'contextmenu'
					// 	];
					// 
					// for (var i = 0; i < events.length; i++) {
					// 	L.DomEvent.on(div, events[i], this._fireMouseEvent, this);
					// }
					L.DomEvent.on(this._container, 'click', this._fireMouseEvent, this);
				},

				_fireMouseEvent: function(e) {
					var map = this._map;
					if (map.dragging && map.dragging.moved()) {
						return;
					}

					var containerPoint = map.mouseEventToContainerPoint(e),
						layerPoint = map.containerPointToLayerPoint(containerPoint),
						latlng = map.layerPointToLatLng(layerPoint);

					this.fire(e.type, {
						latlng: latlng,
						layerPoint: layerPoint,
						containerPoint: containerPoint,
						originalEvent: e
					});
				}
			});
		}
		return new TileLayer(url, options);
	}

	var rosreestr_kadastr = L.rosreestrTileLayer(
	// var rosreestr_kadastr = L.tileLayer(
		// 'http://{s}pkk5.rosreestr.ru/arcgis/rest/services/Cadastre/Cadastre/MapServer/export?dpi=96&transparent=true&format=png32&bbox={bbox}&size=1024%2C1024&bboxSR=102100&imageSR=102100&f=image&layers=show%3A0%2C1%2C2%2C4%2C5%2C6%2C7%2C8%2C9%2C10%2C11%2C12%2C13%2C14%2C15%2C16%2C17%2C18%2C19%2C20',
		'http://localhost/egrnonline/markup/tmp/export.png',
		{
			// tileSize: 1024,
			clickable: true,
			attribution: 'Данные <a href="http://pkk5.rosreestr.ru">ПКК Росреестра</a>',
			zIndex: 500,
			maxZoom: 19
		}
	);

	//Yndex 
	// var yndx = new L.Yandex();

	// map.addLayer(yndx);
	//map.addLayer(rosreestr);
	// map.addLayer(rosreestr_oks);
	// map.addLayer(rosreestr_parcel);
	// map.addLayer(rosreestr_kadastr);
	rosreestr_kadastr.addTo(map);

	map.on('click', function(e) {
		console.log('Клик по карте');
		// $.ajax({
		// 	url: 'http://pkk5.rosreestr.ru/api/features/5',
		// 	data: {
		// 		text: e.latlng.lat + ' ' + e.latlng.lng,
		// 		tolerance: 1,
		// 		limit: 11
		// 	},
		// 	success: function(data, textStatus) {
		// 		if (data.status == 200 && data.features.length) {
		// 			$.ajax({
		// 				url: 'http://pkk5.rosreestr.ru/api/features/5/' + data.features[0].attrs.id,
		// 				success: function(data, textStatus) {
		// 					console.log(data);
		// 				}
		// 			});
		// 		}
		// 	},
		// });
	});
}