import * as utils from './utils/utils';
import Name from './utils/Name';
import Widget from './utils/Widget';
import CollapsibleTrigger from './CollapsibleTrigger';
import Clip from './Clip';

export default function Collapsible(target, options, autoInitialize) {
	Widget.call(this, target, options, autoInitialize);
};
Object.defineProperty(Collapsible, 'name', {
	value: new Name('Collapsible', false),
});
Collapsible.prototype = Object.create(Widget.prototype);
Object.defineProperty(Collapsible, 'constructor', {
	value: Collapsible,
	enumerable: false,
	writable: true,
});

Collapsible.prototype.name = new Name('collapsible');
Collapsible.prototype.trigger = null;
Collapsible.prototype.clip = null;
/**
 * Оптимизировать анимацию путем моментального выставления высоты дополнительного оберточного 
 * контейнера, чтобы не анимировать весь соседний с областью контент
 */
Collapsible.prototype.optimizeAnimation = false;
Object.defineProperty(Collapsible.prototype, 'collapsed', {
	get: function() {
		return this.clip.collapsed;
	},
	set: function(value) {
		this.trigger.collapsed = value;
		this.clip.collapsed = value;
	},
});
Object.defineProperty(Collapsible.prototype, 'state', {
	get: function() {
		return this.clip.state;
	},
	set: function(value) {
		this.clip.state = value;
	},
});

Collapsible.prototype.constructionChain = function(target, options)
{
	Widget.prototype.constructionChain.call(this, target, options);
	
	this.saveInstanceOf(Collapsible);

	if (!this.hasOwnProperty('trigger')) {
		var triggerTarget = document.querySelector(target.dataset[this.name.camel + 'Trigger']);
		if (triggerTarget == null) {
			throw new Error('Элемент для создания триггера не найден (не указан data-атрибут или не найден в DOM)');
		}
		this.trigger = new CollapsibleTrigger(triggerTarget, {
			collapsible: this,
		}, false);
	}

	this.clip = new Clip(target.children[0], null, false);
};

Collapsible.prototype.initializationChain = function(target)
{
	Widget.prototype.initializationChain.call(this, target);
	
	this.setDatasetIdOf(Collapsible);

	if (
		!this.hasOwnProperty('collapsed') && 
		this.dom.self.classList.contains(this.name.cssMod('collapsed'))
	) {
		this.state = 'collapsed';
		this.collapsed = true;
	} else if (
		!this.hasOwnProperty('collapsed') && 
		this.dom.self.classList.contains(this.name.cssMod('expanded'))
	) {
		this.state = 'expanded';
		this.collapsed = false;
	} else {
		// значение по умолчанию из прототипа
		if (this.collapsed) {
			this.dom.self.classList.add(this.name.cssMod('collapsed'));
			this.state = 'collapsed';
		} else {
			this.dom.self.classList.add(this.name.cssMod('expanded'));
			this.state = 'expanded';
		}
	}

	if (!this.hasOwnProperty('optimizeAnimation')) {
		if (this.dom.self.classList.contains(this.name.cssMod('optimize-animation'))) {
			this.optimizeAnimation = true;
		}
	}

	this.trigger.initialize();
	this.clip.initialize();

	this.clip.dom.self.addEventListener(this.clip.collapsedEventName, clipListener.bind(this), false);
	this.clip.dom.self.addEventListener(this.clip.expandedEventName, clipListener.bind(this), false);
};

Collapsible.prototype.toggle = Clip.prototype.toggle;

Collapsible.prototype.collapse = function()
{
	if (this.optimizeAnimation) {
		this.dom.self.style.height = utils.rem(this.clip.getHeight());
	}
	this.clip.collapse();
};

Collapsible.prototype.expand = function()
{
	if (this.optimizeAnimation) {
		this.dom.self.style.height = utils.rem(this.clip.getHeight());
	}
	this.clip.expand();
};

function clipListener() {
	if (this.optimizeAnimation) {
		this.dom.self.style.height = null;
	}
}