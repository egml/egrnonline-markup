import * as utils from './utils/utils';
import Tool from './Tool';
import invitationTemplate from '../html/search-invitation.mustache';
import Stage from './Stage';
import MapOverlay from './MapOverlay';
import Name from './utils/Name';

var name = new Name(['search', 'tool']);

export default function SearchTool(target, options, autoInitialize) {
	Tool.call(this, target, options, autoInitialize);
};
SearchTool.prototype = Object.create(Tool.prototype);

SearchTool.prototype.name = name;
SearchTool.prototype.state = 'hidden';
SearchTool.prototype.invitationEnabled = false;
SearchTool.prototype.invitationLoaded = false;
SearchTool.prototype.invitationVisible = false;
SearchTool.prototype.placeholderText = null;

SearchTool.prototype.initializationChain = function(target)
{
	Tool.prototype.initializationChain.call(this, target);

	this.dom.input = this.dom.self.querySelector(this.name.cssClassSelector('field-input-control'));
	this.dom.submit = this.dom.self.querySelector(this.name.cssClassSelector('field-submit-control'));

	if (!this.hasOwnProperty('placeholderText')) {
		this.placeholderText = this.dom.input.getAttribute('placeholder');
	}

	if (this.dom.self.classList.contains(this.name.cssMod('invitation'))) {
		this.invitationEnabled = true;
	}
	
	if (this.active) {
		this.unsetTooltip();
	}
	var tool = this;
	this.toolbar.dom.self.addEventListener(this.toolbar.collapsedEventName, function() {
		if (tool.active) {
			tool.unsetTooltip();
		}
	});
	this.dom.input.addEventListener('input', this.mainInputListener);
};

SearchTool.prototype.mainInputListener = function() {
	var tool = Tool.prototype.instanceFromDataset(utils.closestParentByClass(this, Tool.prototype.name.css), Tool);
	if (tool.dom.input.value.length) {
		tool.dom.submit.disabled = false;
	} else {
		tool.dom.submit.disabled = true;
	}
};

SearchTool.prototype.activate = function(preactivated) {
	if (this.active) return false;
	Tool.prototype.activate.call(this, preactivated);

	if (this.toolbar.collapsed) {
		this.unsetTooltip();
		this.setState('normal');
	} else {
		this.setState('mini');
	}
	this.dom.input.focus();
};

SearchTool.prototype.action = function() {};

SearchTool.prototype.deactivate = function() {
	if (!this.active) return false;
	Tool.prototype.deactivate.call(this);

	if (this.state == 'mini') {
		this.dom.input.removeEventListener('input', this.miniToNormalListener);
		this.dom.input.removeEventListener('blur', this.blurListener);
		this.toolbar.dom.self.removeEventListener(this.toolbar.collapseEventName, this.toolbarCollapseListener);
	}

	if (this.toolbar.collapsed) {
		this.setTooltip();
	}
	this.setState('hidden');
	this.dom.input.value = '';
};

SearchTool.prototype.setState = function(state) {
	if (typeof state !== 'string') {
		state = this.state;
	}
	switch (state) {

		case 'normal':
			this.state = 'normal';
			this.dom.self.classList.remove(this.name.cssMod('mini'));
			this.dom.self.classList.add(this.name.cssMod('normal'));
			this.dom.input.setAttribute('placeholder', this.placeholderText);
			if (this.invitationVisible) {
				this.invitationHide();
			}

			break;
		
		case 'mini':
			this.state = 'mini';
			this.dom.self.classList.remove(this.name.cssMod('normal'));
			this.dom.self.classList.add(this.name.cssMod('mini'));
			this.dom.input.addEventListener('input', this.miniToNormalListener);
			this.dom.input.addEventListener('blur', this.blurListener);
			this.dom.input.removeAttribute('placeholder');
			this.toolbar.dom.self.addEventListener(this.toolbar.collapseEventName, this.toolbarCollapseListener);
			if (this.invitationEnabled) {
				this.invitationShow();
			}

			break;
		
		case 'hidden':
			this.state = 'hidden';
			this.dom.self.classList.remove(this.name.cssMod('mini'));
			this.dom.self.classList.remove(this.name.cssMod('normal'));
			if (this.invitationVisible) {
				this.invitationHide();
			}

			break;
	}
};

SearchTool.prototype.miniToNormalListener = function() {
	var tool = Tool.prototype.instanceFromDataset(utils.closestParentByClass(this, Tool.prototype.name.css), Tool);
	tool.dom.input.removeEventListener('input', tool.miniToNormalListener);
	tool.dom.input.removeEventListener('blur', tool.blurListener);
	tool.toolbar.dom.self.removeEventListener(tool.toolbar.collapseEventName, tool.toolbarCollapseListener);
	tool.setState('normal');
	tool.toolbar.collapse();
};

SearchTool.prototype.blurListener = function() {
	var tool = Tool.prototype.instanceFromDataset(utils.closestParentByClass(this, Tool.prototype.name.css), Tool);
	tool.deactivate();
};

SearchTool.prototype.toolbarCollapseListener = function() {
	var tool;
	for (var i = 1; i <= Tool.prototype.instances.length; i++) {
		tool = Tool.prototype.instances[i];
		if (tool instanceof SearchTool && tool.active) {
			break;
		}
		tool = null;
	}
	if (tool !== null) {
		tool.deactivate();
	}
};

SearchTool.prototype.invitationLoad = function() {
	if (!this.invitationLoaded) {
		// var htmlTemplate = require('mustache-loader!../modal.mustache');
		// var htmlTemplateData = {
		// 	scale: 16 / ( window.Website.params.bodyFontSize || 16 ),
		// };
		var range = document.createRange();
		// #CBFIX Эта строка нужна в частности для Android 4.4.4 WebKit 537.36, иначе: `Uncaught NotSupportedError: Failed to execute 'createContextualFragment' on 'Range': The range's container must be an HTML element.`
		range.selectNode(document.getElementsByTagName('div').item(0));
		var domFragment = range.createContextualFragment(invitationTemplate());
		this.dom.input.parentNode.appendChild(domFragment);
		this.dom.invitation = this.dom.input.parentNode.querySelector('.' + this.name.ns.snake + '-search_invitation');
		this.invitationLoaded = true;
	}
	return this;
};

SearchTool.prototype.invitationShow = function() {
	this.invitationLoad();
	this.dom.invitation.classList.remove(this.name.ns.snake + '-search_invitation--hidden');
	this.invitationVisible = true;
	if (Stage.initialized) {
		MapOverlay.show();
	}
};

SearchTool.prototype.invitationHide = function() {
	this.dom.invitation.classList.add(this.name.ns.snake + '-search_invitation--hidden');
	this.invitationVisible = false;
};

SearchTool.prototype.invitationDestroy = function() {
	this.dom.invitation.parentNode.removeChild(this.dom.invitation);
	this.invitationLoaded = false;
};