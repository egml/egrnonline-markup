import * as utils from './utils/utils';
import Map from './Map';
import MapOverlay from './MapOverlay';

export default {
	dom: {
		self: null,
	},
	map: null,
	mapOverlay: null,
	initialized: null, // Используется в SearchTool

	initialize: function()
	{
		this.dom.self = document.querySelector('.egrn-stage');
		this.map = new Map(document.querySelector('.egrn-stage-map'));
		this.mapOverlay = MapOverlay;
		this.mapOverlay.initialize();
		this.initialized = true;
	},
};