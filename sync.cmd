@ECHO OFF
SETLOCAL

php -f update-cache-buster.php
php -f update-icon-index.php
rsync build/. ihc:www/haveagoodday.ru/dev/egrnonline/markup -avh --del --chmod=F0664,D0775

ENDLOCAL