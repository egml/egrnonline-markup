const path = require('path');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
	mode: 'development',
	entry: './src/js/main',
	output: {
		path: path.resolve(__dirname, 'build/assets/js/'),
		library: 'EGRN',
		filename: 'egrn.js',
	},
	module: {
		rules: [
			{ test: /\.css$/, use: ['style-loader', 'css-loader'] },
			{ // Нужен при загрузке leaflet.css. Возможно можно как-то исправить?
				test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
				loader: 'url-loader',
				options: {
					limit: 10000
				}
			},
			{ test: /\.mustache$/, use: ['mustache-loader'] },
		],
	},
	plugins: [
		new WebpackNotifierPlugin(),
	],
	// devtool: 'cheap-source-map', // #CBFIX: Исправляет баг в старых Android браузерах, в частности WebView 33.0.0.0 (Android 4.4 KitKat) (вероятно, когда они не понимают слишком длинные `eval()`), также можно использовать 'cheap-module-source-map' https://github.com/webpack/webpack/issues/426#issuecomment-275439196
};

// module.exports = [{
// 	name: 'build',
// 	mode: 'production',
// 	entry: './src/js/main',
// 	output: {
// 		// path: path.resolve(__dirname, 'build/assets/js/'),
// 		path: path.resolve(__dirname, '../yii/www/assets/js/'),
// 		library: 'EGRN',
// 		filename: 'egrn.js',
// 	},
// 	module: {
// 		rules: [
// 			{ test: /\.css$/, use: ['style-loader', 'css-loader'] },
// 			{ test: /\.mustache$/, use: ['mustache-loader'] },
// 		],
// 	},
// 	plugins: [
// 		new WebpackNotifierPlugin({
// 			skipFirstNotification: true, // Do not notify on the first build. This allows you to receive notifications on subsequent incremental builds without being notified on the initial build.
// 		}),
// 	],
// 	devtool: 'cheap-source-map', // #CBFIX: Исправляет баг в старых Android браузерах, в частности WebView 33.0.0.0 (Android 4.4 KitKat) (вероятно, когда они не понимают слишком длинные `eval()`), также можно использовать 'cheap-module-source-map' https://github.com/webpack/webpack/issues/426#issuecomment-275439196
// }, {
// 	name: 'dev',
// 	mode: 'development',
// 	// entry: {
// 	// 	main: './src/js/main',
// 	// 	// frontpage: './src/js/frontpage',
// 	// 	// demo_scrollbar: './src/js/demo_scrollbar',
// 	// 	// demo_collapsible: './src/js/demo_collapsible',
// 	// 	// demo_collapsible_area: './src/js/demo_collapsible_area',
// 	// 	// demo_input: './src/js/demo_input',
// 	// 	// demo_toolbar: 
// 	// },
// 	entry: './src/js/main',
// 	output: {
// 		// path: path.resolve(__dirname, 'build/assets/js/'),
// 		path: path.resolve(__dirname, '../yii/www/assets/js/'),
// 		library: 'EGRN',
// 		// libraryTarget: 'assign',
// 		// filename: 'egrn-[name].js',
// 		filename: 'egrn.js',
// 		// chunkFilename: '[name].chunk.js',
// 		// jsonpFunction: 'webpackJsonpControlOverlay',
// 	},
// 	module: {
// 		rules: [
// 			{ test: /\.css$/, use: ['style-loader', 'css-loader'] },
// 			// {
// 			// 	test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
// 			// 	loader: 'url-loader',
// 			// 	options: {
// 			// 		limit: 10000
// 			// 	}
// 			// },
// 			{ test: /\.mustache$/, use: ['mustache-loader'] },
// 			// { test: /\.scss$/, use: ['style-loader', 'css-loader', 'sass-loader'] },
// 		],
// 	},
// 	plugins: [
// 		// new webpack.optimize.CommonsChunkPlugin({
// 		// 	name: 'manifest',
// 		// 	// name: 'modal',
// 		// 	// name: 'modal_component',
// 		// 	// names: ['modal_component', 'manifest'],
// 		// 	// filename: 'modal.js',
// 		// 	// chunks: ['modal'],
// 		// 	// chunks: ['modal', 'modal_lazy', 'access_point_lazy'],
// 		// 	// children: true,
// 		// 	// async: true,
// 		// 	// minChunks: Infinity,
// 		// }),
// 		new WebpackNotifierPlugin({
// 			// title: 'Webpack', // Title shown in the notification
// 			// contentImage: path.join(__dirname, 'logo.png'), // Image shown in the notification
// 			// excludeWarnings: true, // If set to true, warnings will not cause a notification
// 			// alwaysNotify: false, // Trigger a notification every time. Call it "noisy-mode".
// 			skipFirstNotification: true, // Do not notify on the first build. This allows you to receive notifications on subsequent incremental builds without being notified on the initial build.
// 		}),
// 		// new ExtractTextPlugin(
// 		// 	'styles.css'
// 		// 	// {
// 		// 	// 	filename: 'bundle.css',
// 		// 	// 	disable: false,
// 		// 	// 	allChunks: true,
// 		// 	// }
// 		// ),
// 	],
// 	//externals: {
// 	//	jquery: 'jQuery',
// 	//},
// 	// module: {
// 	//     rules: [
// 	// 		{
// 	//             test: /\.css$/,
// 	//             exclude: /node_modules/,
// 	// 			// use: ExtractTextPlugin.extract({
// 	// 			// 	use: 'css-loader',
// 	// 			// 	fallback: 'style-loader',
// 	// 			// 	// options: {
// 	// 			// 	// 	sourceMap: true,
// 	// 			// 	// },
// 	// 			// }),
// 	// 			use: [
// 	// 				{ loader: "style-loader" },
// 	// 				{ loader: "css-loader" },
// 	// 			],
// 	//         },
// 	// 	],
// 	// },

// 	// devtool: 'eval',
// 	// devtool: 'source-map',
// 	devtool: 'cheap-source-map', // #CBFIX: Исправляет баг в старых Android браузерах, в частности WebView 33.0.0.0 (Android 4.4 KitKat) (вероятно, когда они не понимают слишком длинные `eval()`), также можно использовать 'cheap-module-source-map' https://github.com/webpack/webpack/issues/426#issuecomment-275439196

// 	// stats: {
// 	//     // Configure the console output 
// 	//     colors: true,
// 	//     modules: true,
// 	//     // reasons: false,
// 	//     reasons: true,
// 	// },

// 	// // stats: false disables the stats output 
// 	// 
// 	// storeStatsTo: "xyz", // writes the status to a variable named xyz 
// 	// // you may use it later in grunt i.e. <%= xyz.hash %> 
// 	// 
// 	// progress: false, // Don't show progress 
// 	// // Defaults to true 

// 	// failOnError: false, // don't report error to grunt if webpack find errors 
// 	// // Use this if webpack errors are tolerable and grunt should continue 
// 	// 
// 	// watch: true, // use webpacks watcher 
// 	// // You need to keep the grunt process alive 

// 	// watchOptions: {
// 	//     aggregateTimeout: 500,
// 	//     poll: true
// 	// },
// 	// // Use this when you need to fallback to poll based watching (webpack 1.9.1+ only) 

// 	// keepalive: true, // don't finish the grunt task 
// 	// // defaults to true for watch and dev-server otherwise false 
// 	// 
// 	// inline: true,  // embed the webpack-dev-server runtime into the bundle 
// 	// // Defaults to false 
// 	// 
// 	// hot: true, // adds the HotModuleReplacementPlugin and switch the server to hot mode 
// 	// // Use this in combination with the inline option 
// }];
